var express = require('express');
var router = express.Router();
//require controllers
var notificationsController = require('./controllers');
var jwt = require('express-jwt');


//"/notifications/assign-notifications"
router.post('/assign-notifications', jwt({
    secret: process.env.secret
}), notificationsController.assigenNotificationToVendor)

//"/notifications/show-all-notifications"
router.get('/show-all-notifications', jwt({
    secret: process.env.secret
}), notificationsController.getAllNotifications)

//"/notifications/show-vendor-notifications"
router.get('/show-vendor-notifications', jwt({
    secret: process.env.secret
}), notificationsController.getAllNotificationsOfVendor)


module.exports = router;