const express = require('express');
const router = express.Router();
const notificationsModel = require('./model');
let middlewares = require('../middlewares')
const RequestService = require('../requests/model');

//var firebaseRef = firebase.database().ref("notifications/");

exports.assigenNotificationToVendor = function(req, res, next) {
let requestData = req.body;
let userData = req.user;

notificationsModel.findOne(
    {
      requestId: requestData.requestId
    },
    (err, foundNot) => {
      if (err || !foundNot) {
        res.json({
          success: false,
          message: err || "Error,Please contact support"
        });
      } else if (foundNot && foundNot.status === 'unassigned') {
        
        foundNot.adminId = userData._doc._id
        foundNot.adminName = userData._doc.name
        foundNot.status = "assigned"
        foundNot.save((err , data) => {
          if(err || !data) {
          	res.json({
          success: false,
          message: err || "can not save notification data"
        });
          }else{
          	res.json({
          success: true,
          message:"notification assigned success"
        });
          }
        })
      }else{
      	res.json({
          success: false,
          message:"already skipped this status"
        });
      }
  })
}


exports.getAllNotifications = function(req, res, next) {
let requestData = req.body;
let userData = req.user;

notificationsModel.find({},
    (err, foundNots) => {
      if (err || !foundNots) {
        res.json({
          success: false,
          message: err || "Error,Please contact support"
        });
      } else if (foundNots) {
        res.json({
          success: true,
          message:foundNots
        });
      }
  })

}

exports.getAllNotificationsOfVendor = function(req, res, next) {
let requestData = req.body;
let userData = req.user;

notificationsModel.find({
	 vendorId: requestData.vendorId || req.user._id
},
    (err, foundNots) => {
      if (err || !foundNots) {
        res.json({
          success: false,
          message: err || "Error,Please contact support"
        });
      } else if (foundNots) {
        res.json({
          success: true,
          message:foundNots
        });
      }
  })
}


