const mongoose = require('mongoose')
const config = require('config')

const notificationsSchema = mongoose.Schema({
    adminId: {
        type: String,
        require: false
    },
    adminName: {
        type: String,
        require: false
    },
    status: {
        type: String,
        require: false,
        default:'unassigned'
    },
    requestId: {
    	 type: String,
        require: false
    }
})

const notifications = module.exports = mongoose.model('notifications', notificationsSchema)