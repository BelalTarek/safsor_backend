var express = require('express');
var router = express.Router();
//require controllers
var requestController = require('./controllers');
var jwt = require('express-jwt');

//Routers
/**
 * @api {post} /requests/request-service request a service
 * @apiName requestService
 * @apiGroup requesting-services
 *
 * @apiParam  {lat} lat lat.
 * @apiParam  {long} long long.
 * @apiParam  {serviceId} serviceId serviceId.
 * @apiParam  {carModel} carModel carModel.
 * @apiParam  {carManufactory} carManufactory carManufactory.
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *     newRequestId: newRequestId
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
        message: 'failed to add'
 *   }
 */
router.post('/request-service',
jwt({
    secret: process.env.secret
}),
 requestController.requestService);
router.post('/add-driver', requestController.addDriver);
/**
 * @api {post} /requests/send-request-admin schedule a request
 * @apiName scheduleService
 * @apiGroup requesting-services
 *
 * @apiParam {string} serviceId id of request
 * @apiParam {Number} lat lat of user location
 * @apiParam {Number} long long of user location
 * @apiParam {Date} date date of request
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
* newrequestId:newrequestId 
* }
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
        message: 'failed to add'
 *   }
 */
router.post('/send-request-admin', jwt({
    secret: process.env.secret
}), requestController.sendRequestToAdmin);
//router.get('/call-interval', requestController.callInterval)
//router.get('/intervals', requestController.intervals)
/**
 * @api {post} /requests/cancel cancel a request
 * @apiName cancel a service
 * @apiGroup requesting-services
 *
 * @apiParam {string} serviceId id of request
 * @apiParam {Number} lat lat of user location
 * @apiParam {Number} long long of user location
 * @apiParam {Date} date date of request
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
* newrequestId:"Request canceled" 
* }
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
        message: 'failed to cancel'
 *   }
 */

router.post('/cancel', jwt({
    secret: process.env.secret
}), requestController.cancelRequest);


/**
 * @api {post} /requests/cancel cancel a request
 * @apiName cancel a service
 * @apiGroup requesting-services
 *
 * @apiParam {string} serviceId id of request
 * @apiParam {Number} lat lat of user location
 * @apiParam {Number} long long of user location
 * @apiParam {Date} date date of request
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
* newrequestId:"Request canceled" 
* }
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
        message: 'failed to cancel'
 *   }
 */

router.post('/user-cancel-request', jwt({
    secret: process.env.secret
}), requestController.userCancelRequest);


/**
 * @api {post} /requests/cancel cancel a request
 * @apiName cancel a service
 * @apiGroup requesting-services
 *
 * @apiParam {string} serviceId id of request
 * @apiParam {Number} lat lat of user location
 * @apiParam {Number} long long of user location
 * @apiParam {Date} date date of request
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
* newrequestId:"Request canceled" 
* }
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
        message: 'failed to cancel'
 *   }
 */

router.post('/user-cancel', jwt({
    secret: process.env.secret
}), requestController.UserCancelRequest);
/**
 * @api {post} /requests/feedback write feedback
 * @apiName addFeedback
 * @apiGroup requesting-services
 *
 * @apiParam {string} _id request Id.
 * @apiParam {string} feedback comment user wrote (optinal)
 * @apiParam {number} rate (required)
 *
 * @apiSuccessExample Success-Response:
 *{
 *      success: true,
 *     message: 'Your Feedback Saved'
 *}
 * @apiErrorExample Error-Response:
 * {
 *     success: false,
 *    message: 'Your Request Not Done Yet!'
 *   }
 */

router.post('/write-feedback', jwt({
    secret: process.env.secret
}), requestController.feedback);

/**
 * @api {get} /requests/user-track-his-trip list all of my trips
 * @apiName myTrips
 * @apiGroup requesting-services
 *
 * @apiParam {string} id send the token of the customer in the header.
 *
 * @apiSuccessExample Success-Response:
 *{
 *      success: true,
 *     message: 'no requests for this user'
 *}
 * @apiErrorExample Error-Response:
 * {
 *     success: false,
 *    message: requests
 *   }
 */

router.post('/user-track-his-trip',jwt({
    secret: process.env.secret
}), requestController.userTrackHisTrip)


/**
 * @api {get} /requests/driver-track-his-trip list all of my trips
 * @apiName myTrips
 * @apiGroup requesting-services
 *
 * @apiParam {string} id send the token of the driver in the header.
 *
 * @apiSuccessExample Success-Response:
 *{
 *      success: true,
 *     message: 'no requests for this user'
 *}
 * @apiErrorExample Error-Response:
 * {
 *     success: false,
 *    message: requests
 *   }
 */

router.post('/driver-track-his-trip',jwt({
    secret: process.env.secret
}), requestController.driverTrackHisTrip)


/**
 * @api {post} /requests/cancel-accepted-request cancel request after accept 
 * @apiName cancelacceptedrequest
 * @apiGroup requesting-services
 *
 * @apiParam {string} requestId id of request
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
* newrequestId:"Request canceled" 
* }
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
        message: 'failed to cancel'
 *   }
 */

router.post('/cancel-accepted-request', jwt({
    secret: process.env.secret
}), requestController.cancelAcceptedRequest);


//* @api {get} /requests/list-requests
router.get('/list-requests', requestController.runningRequests);
//* @api {get} /requests/finished-requests
router.get('/finished-requests', requestController.finishedRequests);
//* @api {get} /requests/canceled-requests
router.get('/canceled-requests', requestController.canceledRequests);


//* @api {get} /requests/list-vendor-requests
router.get('/list-vendor-requests', jwt({
    secret: process.env.secret
}), requestController.runningVendorRequests);
//* @api {get} /requests/finished-vendor-requests
router.get('/finished-vendor-requests', jwt({
    secret: process.env.secret
}), requestController.finishedVendorRequests);
//* @api {get} /requests/canceled-vendor-requests
router.get('/canceled-vendor-requests', jwt({
    secret: process.env.secret
}), requestController.canceledVendorRequests);




router.post('/write-feedback', jwt({
    secret: process.env.secret
}), requestController.feedback);
/**
 * @api {post} /requests/accept-request driver accept request
 * @apiName acceptRequest
 * @apiGroup requesting-services
 *
 * @apiParam {string} driverLat driverLat.
 * @apiParam {string} driverLong driverLong.
 * @apiParam {string} requestId requestId.
 *
 * @apiSuccessExample Success-Response:
 *{
 *      success: true,
 *}
 * @apiErrorExample Error-Response:
 * {
 *     success: false,
 *   }
 */

router.post('/accept-request', jwt({
    secret: process.env.secret
}), requestController.driverAcceptRequest);

//"/requests/is-customer-request"
 router.post('/is-customer-request', jwt({
     secret: process.env.secret
 }), requestController.inRequestCustomer);

//"/requests/customer-request"
 router.get('/customer-request', jwt({
     secret: process.env.secret
 }), requestController.RequestCustomer);

 //"/requests/assign-driver-request"
 router.post('/assign-driver-request', jwt({
     secret: process.env.secret
 }), requestController.assignDriverToRequest);

 //"/requests/is-driver-request"
  router.get('/is-driver-request', jwt({
     secret: process.env.secret
 }), requestController.inRequestDriver);


router.post('/estimate-fare', jwt({
    secret: process.env.secret
}), requestController.estimateFare);

//"/requests/cancelled-request"
router.get('/cancelled-request',  requestController.sayHello);
module.exports = router;

