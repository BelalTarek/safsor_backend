const mongoose = require('mongoose')
const config = require('config')

const RequestServiceSchema = mongoose.Schema({
	userId: {
		type: String,
	},
	userName: {
		type: String,
	},
	driverId: {
		type: String,
	},
	acceptReqLocation: {
		lat: String,
		long: String
	},
    destinationLocation: {
		lat: String,
		long: String
	},
	startTripLocation: {
		lat: String,
		long: String
	},
	endTripLocation: {
		lat: String,
		long: String
	},
	serviceId: [{
		type: String,
	}],
	serviceName: [{
		type: String,
	}],
	userLocation: {
		lat: String,
		long: String
	},
	status: {
		type: String,
		default: 'requested',
	},
	paymentType: {
		type: String,
	},
	userFeedback: {
		type: String,
	},
	userRate: {
		type: Number,
	},
	driverFeedback: {
		type: String,
	},
	driverRate: {
		type: Number,
	},
	expectedCost: {
		type: Number,
	},
	finalCost: {
		type: Number,
	},
	closingTime: {
		type: Date,
	},
	date: {
		type: Date,
	},
	canceledBy: {
		type: String
	},
	createdAt: {
		type: Date,
		default: Date.now
	},
	carModel: {
		type: String
	},
	carManufactory: {
		type: String
	},
	carNumber: {
		type: String
	},
	firstEstimate: {
		type: String,
	},
	lastEstimate: {
		type: String,
	},
	waitingTime: {
		type: Number,
	},
	cancellationReason: {
		type: String
	}
});

const requestService = module.exports = mongoose.model('requestService', RequestServiceSchema)