const express = require("express");
const router = express.Router();
const RequestService = require("./model");
const decodeJWT = require("decoding_jwt");
const moment = require("moment");
let middlewares = require("../middlewares");
const customer = require("../auth/customer/customerModel");
const vendor = require("../auth/vendor/vendorModel");
const vendorCar = require("../cars/model");
const servicesModel = require("../services/model");
const notificationsModel = require("../notifications/model");
let tripSettings = require("../trip/settingsModel");
var admin = require("firebase-admin");
var firebase = require("firebase");
var GeoFire = require("geofire");
var geodist = require('geodist')
var FCM = require("fcm-push");
var fcm = new FCM("AIzaSyCogA6raV7doIYReJiuy9HYUCyplslbCII");
require("dotenv").config();
const pushModel = require("../pushNot/model");
var db = admin.database();
var rof = db.ref("/drivers");
var request = require('request');
var serverKey = process.env.serverKey;
var Report = require('../report/model')
var Rating = require('../rating/model')
var promoCode = require('../promocodes/model')



// Initialize Firebase
var config = {
  apiKey: "AIzaSyBrBX3cSps9nzZffX-CeGblyzNaWpxjfEc",
  authDomain: "safesor-96915.firebaseapp.com",
  databaseURL: "https://safesor-96915.firebaseio.com",
  projectId: "safesor-96915",
  storageBucket: "safesor-96915.appspot.com",
  messagingSenderId: "651953087927"
};
firebase.initializeApp(config);
// Create a Firebase reference where GeoFire will store its information
var firebaseRef = firebase.database().ref("drivers/");
// Create a GeoFire index
var geoFire = new GeoFire(firebaseRef);

exports.addDriver = function(req, res, next) {
  geoFire.set("5b460ce721df2b5e827589c9", [37.79, -122.41]).then(
    function() {
      console.log("Provided key has been added to GeoFire");
    },
    function(error) {
      console.log("Error: " + error);
    }
  );
};
// var starCountRef = firebase.database().ref('schedule/');
// starCountRef.once('value', function (snapshot) {
//     snapshot.forEach(function (childSnapshot) {
//       var childKey = childSnapshot.key;
//       var childData = childSnapshot.val();
//       console.log(childData.month)
//       })

//   })
//var date = new Date();
// function createDateAsUTC(date) {
//     return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()));
// }
//console.log(date.getHours());

function intervalFunc(req, res ,next) {
   //var reqBody = req.body
   //console.log("ana barra")
   var now = new Date()
   //console.log(now)
  var starCountRef = firebase.database().ref('schedule/');
starCountRef.once('value', function (snapshot) {
    snapshot.forEach(function (childSnapshot) {
      var childKey = childSnapshot.key;
      var childData = childSnapshot.val();
  if(childData.month == now.getMonth() && childData.day == now.getDate() && childData.hour == now.getHours() && childData.min == now.getMinutes()){
    
    console.log("ana henaaa")
    request.post({url:'https://enqaz2.herokuapp.com/requests/request-service',
      headers: {
    'Authorization': childData.token
  },
     form:
      {lat:childData.customerLat,
     long:childData.customerLng,
     carModel:childData.carModel,
     carManufactory:childData.carManufactory,
     serviceId:childData.serviceId,
     serviceName:childData.serviceName,
     userId:childData.userId,
     paymentType:childData.paymentType,
     destinationLat:childData.destLat,
     destinationLong:childData.destLng
   }
 }, function(err,httpResponse,body){ 
if(err){
  console.log(err)
}else{
  console.log(body)
  
  pushModel.findOne(
    {
      userId: childData.userId
    },
    (err, userFcmToken) => {
      if (err || !userFcmToken) {
       res.json({
          success: false,
          message: err || "no admin found"
        });
      } else if (userFcmToken) {
        
        var message = {
          to: userFcmToken.fcmToken, // required fill with device token or topics
          collapse_key:
            "your_collapse_key",
          
          notification: {
            title: "your Request",
            body:"your schedule request is running now"
          }

        };
        //promise style
        fcm
          .send(message)
          .then(function(response) {
            // res.json({
            //   success: true,
            //   message: response
            // });
           

            console.log(
              "Successfully sent with response: ",
              response
            );
            starCountRef.child(childKey).remove();
          })
          .catch(function(err) {
            console.log(
              "Something has gone wrong!",
              err
            );
          });
      }
    }

  );

}

 })
   
  }

});

})
}

// function intervalF(){
//   request.get('https://enqaz2.herokuapp.com/requests/call-interval', function(er, response, body){
//     if(er){
//       console.log(er)
//     }else{
//       console.log(body)
//     }
//   })
// }

setInterval(intervalFunc, 180000);
//setInterval(intervalF, 180000);

// exports.intervals = function(req, res, next){
//   intervalF();
// }

// exports.callInterval = function(req, res, next){
//    //console.log("ana barra")
//   var now = new Date()
//   console.log(now.getHours())
//   var starCountRef = firebase.database().ref('schedule/');
//   starCountRef.once('value', function (snapshot) {
//     snapshot.forEach(function (childSnapshot) {
//       var childKey = childSnapshot.key;
//       var childData = childSnapshot.val();
//       console.log('ss '+childData.hour)
//   if(childData.month == now.getMonth() && childData.day == now.getDate() 
//   && childData.hour == now.getHours() && childData.min == now.getMinutes()){
    
//     console.log("ana henaaa")
//     request.post({url:'https://enqaz2.herokuapp.com/requests/request-service',
//       headers: {
//     'Authorization': childData.token
//   },
//      form:
//       {lat:childData.customerLat,
//      long:childData.customerLng,
//      carModel:childData.carModel,
//      carManufactory:childData.carManufactory,
//      serviceId:childData.serviceId,
//      serviceName:childData.serviceName,
//      userId:childData.userId,
//      paymentType:childData.paymentType
//    }
//  }, function(err,httpResponse,body){ 
//       if(err){
//         console.log(err)
//       }else{
//         console.log(body)
//         starCountRef.child(childKey).remove();
//       }

//     })
   
//   }

// });

// res.json({success: true})

// })
// }
exports.sendRequestToAdmin = function(req, res, next) {
  let reqBody = req.body
  let newNotification = new notificationsModel({
  requestId: reqBody.requestId
  })
  newNotification.save((err,data) => {
    if (err || !data){
      res.json({
          success: false,
          message: err || "can not save notification data"
        });
    }else{

      RequestService.findOne({
        _id: reqBody.requestId
      } , (err , request) => {

      if(err || !request) {
        res.json({
          success: false,
          message: err || "no request found"
        });
      }else{

      var notificationsRef = firebase.database().ref('notifications/').push({
        
            customerData: {
              name: reqBody.name,
              phoneNumber: reqBody.phoneNumber,
            },
            requestData: {
              requestId: reqBody.requestId,
              lat: reqBody.lat,
              long: reqBody.long, 
              service: reqBody.serviceName
            }
          
      });
         res.json({
          success: true,
          message: "your request go to admin now"
        });
   }
  })
    }
  })
};
//------------------------------------------------------------------------------------------------------------------------------------------------//

//Request a Service
exports.requestService = function(req, res, next) {
  var drivers ;
  let reqBody = req.body;
  console.log(reqBody)
  RequestService.findOne(
    {
      userId: reqBody.userId || req.user._doc._id,
      status: {
        $in: ["requested", "running", "accepted"]
      }
    },
    (err, foundService) => {
      if (err || foundService) {
        res.json({
          success: false,
          message: err || "already requested"
        });
      } else {
        if (
          reqBody.lat &&
          reqBody.long &&
          reqBody.serviceId &&
          reqBody.carModel &&
          reqBody.carManufactory
        ) {
          tripSettings.find({}, (err, data) => {
            if (err || !data) {
              // console.log("keeeey")
              res.json({
                success: false,
                message: err || "we need to add settings"
              });
            } else if (data.length > 0) {
              customer.findOne(
                {
                  _id: reqBody.userId || req.user._doc._id
                },
                function(err, customerData) {
                  if (err || !customerData) {
                    res.json({
                      success: false,
                      message: err || "smth wrong"
                    });
                  } else if (customerData) {
                    servicesModel.find(
                      {
                        _id:{
                          $in:reqBody.serviceId
                        }
                      },
                      (err, serviceDetails) => {
                        if (serviceDetails) {
                          console.log("laaaat", serviceDetails[0].name)
                          // console.log("looong", reqBody.long)
                          var geoQuery = geoFire.query({
                            center: [Number(reqBody.lat), Number(reqBody.long)],
                            radius: data[0].firstRadius || 10
                          });
                          console.log(reqBody.serviceName)
                          let newrequest = new RequestService({
                            userName: req.user._doc.name,
                            userId: req.user._doc._id,
                            serviceId: reqBody.serviceId,
                            serviceName: reqBody.serviceName,
                            paymentType: reqBody.paymentType,
                            userLocation: {
                              lat: reqBody.lat,
                              long: reqBody.long
                            },
                            destinationLocation: {
                              lat: reqBody.destinationLat,
                              long: reqBody.destinationLong
                            },
                            carModel: reqBody.carModel,
                            carManufactory: reqBody.carManufactory
                          });
                          newrequest.save((err, newrequest) => {
                            if (err) {
                              res.json({
                                success: false,
                                message: "failed to add"
                              });
                            } else {
                              if(req.body.promoCode){
                                                    promoCode.findOne({
                                                    userIds: reqBody.userId ? reqBody.userId : req.user._doc._id
                                                  }, function(er, code){
                                                    if(er){
                                                      return res.json({
                                                        success: false,
                                                        message: er
                                                      })
                                                    }
                                                    
                                                    else if(code && new Date(Date.now()) <= code.expiresAt){
                                                      res.json({
                                                        success: true,
                                                        message: "you already have a valid promo code, your request will be processed with the old one",
                                                        newRequestId: newrequest._id
                                                      });
                                                    }
                                                    else{
                                                      promoCode.findOneAndUpdate({code: req.body.promoCode},
                                                      {
                                                        $push: {userIds: reqBody.userId ? reqBody.userId : req.user._doc._id}
                                                      },{new: true}, (errr, foundCode) => {
                                                        if(errr || !foundCode){
                                                          return res.json({
                                                            success: false,
                                                            message: errr || "failed to use promo code, your request will be processed without promo code",
                                                            newRequestId: newrequest._id
                                                          })
                                                        }
                                                        else{
                                                          res.json({
                                                            success: true,
                                                            newRequestId: newrequest._id
                                                          })
                                                        }
                                                      })
                                                      // res.json({
                                                      //   success: true,
                                                      // });
                                                    }
                                                  })
                                                  }
                                                  if(!req.body.promoCode){
                                                            res.json({
                                                              success: true,
                                                              newRequestId: newrequest._id
                                                            });
                                                          }
                           
                              var onKeyEnteredRegistration = geoQuery.on(
                                "key_entered",
                                function(key, location, distance) {
                                  console.log("keeeey", key)
                                  console.log("distance", distance)
                                  vendor.findOne({
                                    _id: key
                                  }, (err , vendor) => {
                                    if(err || !vendor) {
                                      res.json({
                                        success: false,
                                        message: err || "no drivers"
                                      })
                                    }//console.log(vendor)
                                    if(newrequest.paymentType != "Cash"){
                                      console.log("mo7n")
                                      drivers = key
                                    }
                                    else if(vendor.cashRequest === true && newrequest.paymentType === "Cash" && vendor.isActive === true){
                                      console.log(vendor._id)
                                     drivers = key
                                    }
                                  
                                  
                                            Report.update({
                                              driverId: {
                                                $in: key
                                              },
                                              'connectionHistory.endTime': null
                                            }, {
                                              $inc: {'connectionHistory.numOfReqRecieved': 1}
                                            },{ multi: true },function(errorr, reports){
                                              if(errorr || !reports){
                                                return res.json({
                                                  success: false,
                                                  message: errorr || 'error updating'
                                                })
                                              }
                                              /*else{
                                                for (var i = 0; i < reports.length; i++) {
                                                  var reqRecieved = reports[i].connectionHistory.numOfReqRecieved;
                                                  reports[i].connectionHistory.numOfReqRecieved = reqRecieved + 1;
                                                }
                                                reports.save((er, resultt) =>{
                                                  if(er || !resultt || resultt.length <= 0){
                                                    return res.json({
                                                      success: false,
                                                      message: er || "error updating"
                                                    })
                                                  }
                                                })
                                              }*/
                                            })
                                            console.log(drivers)
                                            pushModel.findOne(
                                              {
                                                userId: drivers
                                              },
                                              (err, userFcmToken) => {
                                                if (err || !userFcmToken) {
                                                 console.log(err || "sorry");
                                                } else if (userFcmToken) {
                                                   console.log('cancel')
                                                      geoQuery.cancel();
                                                  // console.log("userFcmToken", userFcmToken)
                                                  Rating.findOne({
                                                    userId: reqBody.userId ? reqBody.userId : req.user._doc._id
                                                  }, function(e, rate){
                                                    if(e){
                                                      return res.json({
                                                        success: false,
                                                        message: e
                                                      })
                                                    }
                                                    else{
                                                      var message = {
                                                        to: userFcmToken.fcmToken, // required fill with device token or topics
                                                        collapse_key:
                                                          "your_collapse_key",
                                                        data: {
                                                          customerData: {
                                                            lat: reqBody.lat,
                                                            long: reqBody.long,
                                                            id: userFcmToken.userId,
                                                            name: customerData.name,
                                                            phoneNumber:
                                                              customerData.phoneNumber,
                                                            rating: rate ? Number.parseFloat(rate.rating).toFixed(2) : 5.0  
                                                          },
                                                          carData: {
                                                            carModel:
                                                              reqBody.carModel,
                                                            carManufactory:
                                                              reqBody.carManufactory
                                                          },
                                                          requestData: {
                                                            destinationLocation: 
                                                              newrequest.destinationLocation,
                                                            requestId:
                                                              newrequest._id,
                                                            paymentType:
                                                              newrequest.paymentType,
                                                            distance: distance,
                                                            serviceType:
                                                              serviceDetails[0].type,
                                                            serviceName:
                                                             newrequest.serviceName,
                                                          },
                                                          notificationType:
                                                            "request"
                                                        },
                                                        notification: {
                                                          title: "New Request",
                                                          body:
                                                            "Apart by " +
                                                            distance +
                                                            " km",
                                                            sound: "alarm.mp3",
                                                            badge: "1"
                                                        }
    
                                                      };
                                                      //promise style
                                                      fcm
                                                        .send(message)
                                                        .then(function(response) {
                                                          
                                                          console.log(
                                                            "Successfully sent with response: ",
                                                            response
                                                          );
                                                        })
                                                        .catch(function(err) {
                                                          console.log(
                                                            "Something has gone wrong!",
                                                            err
                                                          );
                                                        });
                                                    }
                                                  })

                                                  
                                                }
                                              }
                                            );
})
                                         // }
                                          // console.log("The read failed: " + errorObject.code);
                                        //});
                                      //} 
                                      // else {
                                      //   // console.log(
                                      //   //   "the request is already canceled"
                                      //   // );
                                      //   geoQuery.cancel();
                                      // }
                                    },
                                    function(errorObject) {
                                     // console.log(errorObject);
                                    }
                                  );
                                 
                                
                           
                           
                            //   );
                             }
                          });
                        } else {
                          res.json({
                            success: false,
                            message: "service not found"
                          });
                        }
                      }
                    );
                  } else if (err) {
                    res.json({
                      success: false,
                      message: err
                    });
                  }
                }
              );
            }
          });
        } else {
          res.json(middlewares.checkAllFieldsExist);
        }
      }
    }
  );
};

//schedule the request
// exports.scheduleRequestService = function(req, res, next) {
//   let requestData = req.body;
//   if (
//     requestData.serviceId &&
//     requestData.lat &&
//     requestData.long &&
//     requestData.date
//   ) {
//     let newrequest = new RequestService({
//       userId: req.user._id,
//       serviceId: req.body.serviceId,
//       userLocation: {
//         lat: req.body.lat,
//         long: req.body.long
//       },
//       date: Date.parse(req.body.date)
//     });
//     newrequest.save((err, newrequest) => {
//       console.log("schedule", err, newrequest);
//       if (err) {
//         console.log("errrrrr", err);
//         res.json({
//           success: false,
//           message: "failed to add"
//         });
//       } else {
//         res.json({
//           success: true,
//           newRequestId: newrequest._id
//         });
//       }
//     });
//   } else {
//     res.json(middlewares.checkAllFieldsExist);
//   }
// };
// driver cancel the request while still in broadcasting
exports.cancelRequest = function(req, res, next) {
  let requestData = req.body;
  let userData = req.user;
  if (requestData.requestId) {
    console.log("requestData.requestId", requestData.requestId);
    RequestService.findOneAndUpdate(
      {
        _id: requestData.requestId
      },
      {
        $set: {
          canceledBy: req.user._doc._id
        }
      },
      {
        new: true
      },
      function(err, doc) {
        console.log("err,doc", err, doc);
        if (err || !doc) {
          res.json({
            success: false,
            message: err || "failed to find"
          });
        } else {
          // db.ref(
          //   "/drivers/" + req.user._doc._id + "/" + requestData.requestId
          // ).update({
          //   visited: true
          // });
          // console.log("dooooocc", doc);
          // pushModel.findOne({
          //   userId: doc.userId
          // }, (err, userFcmToken) => {

          //   if (userFcmToken) {
          //     // console.log("userFcmToken", userFcmToken)
          //     var message = {
          //       to: userFcmToken.fcmToken, // required fill with device token or topics
          //       collapse_key: 'your_collapse_key',
          //       data: {
          //         notificationType: "canceledReq"
          //       },
          //       notification: {
          //         title: "Request canceld",
          //         body: "sorry your request has been canceld",
          //       }
          //     };
          //     //promise style
          //     fcm.send(message)
          //       .then(function (response) {
          //         res.json({
          //           success: true,
          //           message: response
          //         });
          //         // console.log("Successfully sent with response: ", response);
          //       })
          //       .catch(function (err) {
          //         res.json({
          //           success: false,
          //           message: err
          //         });
          //       });
          //   } else if (err) {
          //     res.json({
          //       success: false,
          //       message: 'error'
          //     });
          //   }
          // });
          res.json({
            success: true,
            message: "request cancelled"
          });
        }
      }
    );
  } else {
    res.json(middlewares.checkAllFieldsExist);
  }
};
// user cancel after driver accepted the requested
exports.userCancelRequest = function(req, res, next) {
  let requestData = req.body;
  let userData = req.user;
  
  if (requestData.requestId) {
    // db.ref("/requests_data/" + requestData.requestId + "/").update({
    //   status: "canceled"
    // });
    //console.log("requestData.requestId", requestData.requestId)
    RequestService.findOneAndUpdate(
      {
        _id: requestData.requestId
      },
      {
        $set: {
          status: "canceled",
          canceledBy: req.user._doc._id,
          cancellationReason: requestData.cancellationReason
        }
      },
      {
        new: true
      },
      function(err, doc) {
        //console.log("err,doc", err, doc);
        if (err || !doc) {
          res.json({
            success: false,
            message: err || "failed to find"
          });
        } else {
          //console.log("dooooocc", doc);
          pushModel.findOne(
            {
              userId: doc.driverId
            },
            (err, userFcmToken) => {
              if (userFcmToken) {
                // console.log("userFcmToken", userFcmToken)
                var message = {
                  to: userFcmToken.fcmToken, // required fill with device token or topics
                  collapse_key: "your_collapse_key",
                  data: {
                    notificationType: "canceledReq"
                  },
                  notification: {
                    title: "Request cancelled",
                    body: "sorry your request has been cancelled",
                    sound: "alarm"
                  }
                };
                //promise style
                fcm
                  .send(message)
                  .then(function(response) {
                    res.json({
                      success: true,
                      message: response
                    });
                    // console.log("Successfully sent with response: ", response);
                  })
                  .catch(function(err) {
                    res.json({
                      success: false,
                      message: err
                    });
                  });
              } else if (err) {
                res.json({
                  success: false,
                  message: "error"
                });
              }
            }
          );
        }
      }
    );
  } else {
    res.json(middlewares.checkAllFieldsExist);
  }
};

// user cancel before any driver accepted
exports.UserCancelRequest = function(req, res, next) {
  let requestData = req.body;
  let userData = req.user;
  if (requestData.requestId) {
    // db.ref("/requests_data/" + requestData.requestId + "/").update({
    //   status: "canceled"
    // });
    RequestService.findOneAndUpdate(
      {
        _id: requestData.requestId
      },
      {
        $set: {
          status: "canceled",
          canceledBy: req.user._doc._id
        }
      },
      {
        new: true
      },
      function(err, doc) {
        //console.log("err,doc", err, doc);
        if (err || !doc) {
          res.json({
            success: false,
            message: err || "failed to find"
          });
        } else {
          //console.log("dooooocc", doc);
          // pushModel.findOne({
          //   userId: doc.driverId
          // }, (err, userFcmToken) => {

          //   if (userFcmToken) {
          //     // console.log("userFcmToken", userFcmToken)
          //     var message = {
          //       to: userFcmToken.fcmToken, // required fill with device token or topics
          //       collapse_key: 'your_collapse_key',
          //       data: {
          //         notificationType: "canceledReq"
          //       },
          //       notification: {
          //         title: "Request canceld",
          //         body: "sorry your request has been canceld",
          //       }
          //     };
          //     //promise style
          //     fcm.send(message)
          //       .then(function (response) {
          //         res.json({
          //           success: true,
          //           message: response
          //         });
          //         // console.log("Successfully sent with response: ", response);
          //       })
          //       .catch(function (err) {
          //         res.json({
          //           success: false,
          //           message: err
          //         });
          //       });
          //   } else if (err) {
          //     res.json({
          //       success: false,
          //       message: 'error'
          //     });
          //   }
          // });
          res.json({
            success: true,
            message: "request cancelled"
          });
        }
      }
    );
  } else {
    res.json(middlewares.checkAllFieldsExist);
  }
};

//write feedback
exports.feedback = function(req, res, next) {
  let feedbackDetails = req.body;
  let userData = req.user;
  // query to save whether the rate and feedback for user or driver
  let query = (userData, feedbackDetails) => {
    // if user
    if (userData._doc.role === 5) {
      return (query = {
        userRate: feedbackDetails.rate,
        userFeedback: feedbackDetails.feedback
      });
    } // if admin
    else if (userData._doc.role === 4) {
      return (query = {
        driverRate: feedbackDetails.rate,
        driverFeedback: feedbackDetails.feedback
      });
    }
  };
  // check first of all if the user sent the needed parameters
  if (feedbackDetails.requestId && feedbackDetails.rate) {
    // if parameters exists start get the request using requestId
    RequestService.findOne(
      {
        _id: feedbackDetails.requestId
      },
      function(err, foundDoc) {
        // check if status is done
        if (foundDoc.status != "finished") {
          res.json({
            success: false,
            message: "request hasn't finished yet"
          });
        } // if the status is done , user can add feedback and rate
        else {
          //check if user and driver already added their rating
          console.log(userData._doc.role)
          if ((userData._doc.role === 5 && !foundDoc.userRate) || (userData._doc.role === 4 && !foundDoc.driverRate)) {
            // check the rate user sent , it should be between 1 and 5
            if (feedbackDetails.rate < 1 || feedbackDetails.rate > 5) {
              res.json({
                success: false,
                message: "rate should be from 1-5"
              });
            } else {
              
              // if everything is good , update feedback and rate
              RequestService.findOneAndUpdate(
                {
                  _id: feedbackDetails.requestId
                },
                {
                  $set: query(userData, feedbackDetails)
                },
                {
                  new: true
                },
                function(err, doc) {
                  if (err || !doc) {
                    res.json({
                      success: false,
                      message: err
                    });
                  } else {
                    Rating.findOne({
                      userId: userData._doc.role == 5 ? foundDoc.driverId : foundDoc.userId
                    }, function(er, rate){
                      if(er){
                        return res.json({
                          success: false,
                          message: er
                        })
                      }
                      else if(!rate){
                        var newRate = new Rating({
                          userId: userData._doc.role == 5 ? foundDoc.driverId : foundDoc.userId,
                          rating: feedbackDetails.rate
                        })
                        newRate.save(function(errr, rslt){
                          if(errr || !rslt){
                            return res.json({
                              success: false,
                              message: errr || "failed to add to avg. rating"
                            })
                          }
                          else{
                            if(userData._doc.role == 5){
                              Report.findOne({
                                driverId: foundDoc.driverId,
                                'connectionHistory.endTime': null
                              }, function(er, report){
                                if(er || !report){
                                  return res.json({
                                    success: false,
                                    message: er || "report not found"
                                  })
                                }
                                else{
                                  var oldRate = report.connectionHistory.rating;
                                  report.connectionHistory.rating = (oldRate + Number(feedbackDetails.rate)) / 2;
                                  report.save(function(e, rprt){
                                    if(e || !rprt){
                                      return res.json({
                                        success: false,
                                        message: e || "failed to save rate to connection history"
                                      })
                                    }
                                    else{
                                      res.json({
                                        success: true,
                                        message: "Your Feedback Saved"
                                      });
                                    }
                                  })
                                }
                              }
                            )
                            }
                            else{
                              res.json({
                                success: true,
                                message: "Your Feedback Saved"
                              });
                            }
                          }
                        })
                      }
                      else{
                        rate.rating = (rate.rating + Number(feedbackDetails.rate)) / 2; 
                        rate.save(function(errr, rslt){
                          if(errr || !rslt){
                            return res.json({
                              success: false,
                              message: errr || "failed to add to avg. rating"
                            })
                          }
                          else{
                            if(userData._doc.role == 5){
                              Report.findOne({
                                driverId: foundDoc.driverId,
                                'connectionHistory.endTime': null
                              }, function(er, report){
                                if(er || !report){
                                  return res.json({
                                    success: false,
                                    message: er || "report not found"
                                  })
                                }
                                else{
                                  var oldRate = report.connectionHistory.rating;
                                  report.connectionHistory.rating = (oldRate + Number(feedbackDetails.rate)) / 2;
                                  report.save(function(e, rprt){
                                    if(e || !rprt){
                                      return res.json({
                                        success: false,
                                        message: e || "failed to save rate to connection history"
                                      })
                                    }
                                    else{
                                      res.json({
                                        success: true,
                                        message: "Your Feedback Saved"
                                      });
                                    }
                                  })
                                }
                              }
                            )
                            }
                            else{
                              res.json({
                                success: true,
                                message: "Your Feedback Saved"
                              });
                            }
                          }
                        })
                      }
                    })
                    
                    // res.json({
                    //   success: true,
                    //   message: "Your Feedback Saved"
                    // });
                  }
                }
              );
            }
          } else {
            res.json({
              success: false,
              message: "feedback already added"
            });
          }
        }
      }
    );
  } else {
    res.json(middlewares.checkAllFieldsExist);
  }
};

//admin list all running Requests
exports.runningRequests = function(req, res, next) {
  RequestService.find(
    {
      status: "running" || "accepted"
    },
    function(err, docs) {
      if (!docs) {
        res.json({
          success: false,
          message: "No requests running"
        });
      } else {
        res.json({
          success: true,
          message: docs
        });
      }
    }
  );
};
//admin list all Done Requests
exports.finishedRequests = function(req, res, next) {
  RequestService.find(
    {
      status: "finished"
    },
    function(err, docs) {
      if (!docs) {
        res.json({
          success: false,
          message: "No requests finished"
        });
      } else {
        res.json({
          success: true,
          message: docs
        });
      }
    }
  );
};

//admin list all canceled Requests
exports.canceledRequests = function(req, res, next) {

  RequestService.find(
    {
      status: "canceled"
    },
    function(err, docs) {
      console.log("nooo" ,  docs.length)
      if (!docs) {
        res.json({
          success: false,
          message: "No requests cancel"
        });
      } else {
        res.json({
          success: true,
          message: docs
        });
      }
    }
  );
};

//vendor list all canceled Requests
exports.canceledVendorRequests = function(req, res, next) {
  vendor.findOne(
    {
      _id: req.user._doc._id
    },
    (err, foundVendor) => {
      if (err) {
        res.json({
          success: false,
          message: err
        });
      } else {
        RequestService.find(
          {
            driverId: {
              $in: foundVendor.drivers
            },
            status: "cancel"
          },
          function(err, docs) {
            if (!docs) {
              res.json({
                success: false,
                message: "No requests canceled"
              });
            } else {
              res.json({
                success: true,
                message: docs
              });
            }
          }
        );
      }
    }
  );
};

//vendor list all running Requests
exports.runningVendorRequests = function(req, res, next) {
  vendor.findOne(
    {
      _id: req.user._doc._id
    },
    (err, foundVendor) => {
      if (err) {
        res.json({
          success: false,
          message: err
        });
      } else {
        RequestService.find(
          {
            driverId: {
              $in: foundVendor.drivers
            },
            status: "running"
          },
          function(err, docs) {
            if (!docs) {
              res.json({
                success: false,
                message: "No requests running"
              });
            } else {
              res.json({
                success: true,
                message: docs
              });
            }
          }
        );
      }
    }
  );
};

//vendor list all finished Requests
exports.finishedVendorRequests = function(req, res, next) {
  vendor.findOne(
    {
      _id: req.user._doc._id
    },
    (err, foundVendor) => {
      if (err) {
        res.json({
          success: false,
          message: err
        });
      } else {
        RequestService.find(
          {
            driverId: {
              $in: foundVendor.drivers
            },
            status: "finished"
          },
          function(err, docs) {
            if (!docs) {
              res.json({
                success: false,
                message: "No requests finished"
              });
            } else {
              res.json({
                success: true,
                message: docs
              });
            }
          }
        );
      }
    }
  );
};

exports.driverAcceptRequest = function(req, res, next) {
  let requestData = req.body;
  let userData = req.user;
  
  RequestService.findOne(
    {
      _id: requestData.requestId
    },
    (err, foundReq) => {
      if (err || !foundReq) {
        res.json({
          success: false,
          message: err || "Error,Please contact support"
        });
      } else if (foundReq) {
    
        if (foundReq.status === "requested") {
          if (requestData.driverLat && requestData.driverLong && requestData.requestId) {
            foundReq.driverId = userData._doc._id;
            foundReq.status = "accepted";
            foundReq.acceptReqLocation.lat = requestData.driverLat;
            foundReq.acceptReqLocation.long = requestData.driverLong;
            foundReq.save((err, doc) => {
              if (err) {
                res.json({
                  success: false,
                  message: "failed"
                });
              } else {
          
                pushModel.findOne(
                  {
                    userId: foundReq.userId
                  },
                  (err, userFcmToken) => {
                    if (err || !userFcmToken) {
                      res.json({
                        success: false,
                        message: err || "failed to find FCM for User"
                      });
                    }
                    if (userFcmToken) {
                      vendor.findOne(
                        {
                          _id: userData._doc._id
                        },
                        (err, foundDriver) => {
                          // console.log("err,foundDriver", err, foundDriver)
                          if (err || !foundDriver) {
                            res.json({
                              success: false,
                              message: err || "no driver"
                            });
                          } else if (foundDriver) {
                            vendorCar.findOne(
                              {
                                driverId: foundDriver._id
                              },
                              (err, foundDriverCar) => {
                                console.log(
                                  "err,foundDriverCar",
                                  err,
                                  foundDriverCar
                                );
                                if (err || !foundDriverCar) {
                                  res.json({
                                    success: false,
                                    message: err || "no car attached"
                                  });
                                } else if (foundDriverCar) {
                                  Rating.findOne({
                                    userId: userData._doc._id
                                  }, function(e, rate){
                                    if(e){
                                      return res.json({
                                        success: false,
                                        message: e
                                      })
                                    }
                                    else{
                                      var message = {
                                        to: userFcmToken.fcmToken, // required fill with device token or topics
                                        collapse_key: "your_collapse_key",
                                        data: {
                                          notificationType: "driverAcceptedReq",
                                          driverData: {
                                            name: foundDriver.name,
                                            photo: foundDriver.imageURL,
                                            phoneNumber: foundDriver.phoneNumber,
                                            rating: rate ? Number.parseFloat(rate.rating).toFixed(2) : 5.0
                                          },
                                          carData: {
                                            carNumber: foundDriverCar.carNumber,
                                            carColor: foundDriverCar.carColor
                                          },
                                          tripData: {
                                            requestId: foundReq._id,
                                            location: {
                                              lat: requestData.driverLat,
                                              long: requestData.driverLong
                                            },
                                            destLat:foundReq.destinationLocation.lat,
                                            destLong:foundReq.destinationLocation.long
    
                                          }
                                        },
    
                                        notification: {
                                          title: "Request approved",
                                          body: "Your request got approved"
                                        }
                                      };
                                      //promise style
                                      fcm
                                        .send(message)
                                        .then(function(response) {
                                          res.json({
                                            success: true,
                                            driverData: {
                                              name: foundDriver.name,
                                              photo: foundDriver.imageURL,
                                              phoneNumber: foundDriver.phoneNumber,
                                              rating: rate ? Number.parseFloat(rate.rating).toFixed(2) : 5.0
                                            },
                                            carData: {
                                              carNumber: foundDriverCar.carNumber,
                                              carColor: foundDriverCar.carColor
                                            }
                                          });
                                        })
                                        .catch(function(err) {
                                          res.json({
                                            success: false,
                                            message: err
                                          });
                                          console.log(
                                            "Something has gone wrong!",
                                            err
                                          );
                                        });
                                    }
                                  })
                                  
                                }
                              }
                            );
                          }
                        }
                      );
                    }
                  }
                );
              }
            });
          } else {
            res.json(middlewares.checkAllFieldsExist);
          }
        } else if (foundReq.status) {
          res.json({
            success: false,
            message: "already skipped this status"
          });
        }
      }
    }
  );
};

exports.userTrackHisTrip = function(req, res, next) {
  let reqBody = req.body;
  let userData = req.user;
  //var i = 0;
  if(reqBody.userId){
    RequestService.find({userId: reqBody.userId},function(err, requests){
      if(err || requests.length <= 0){
        return res.json({
          success: false,
          message: err || "no requests found"
        })
      }
      else{
        res.json({
          success: true,
          requests: requests
        })
      }
    })
  }
  else{
    RequestService.find(
    {
      userId: userData._doc._id,
    },
    (err, requests) => {
      if (err || requests.length <= 0) {
        return res.json({
          success: false,
          message: err || "no requests"
        });
      } else {
        res.json({
          success: true,
          message: requests
        });
      }
    });
  }
  
};

exports.driverTrackHisTrip = function(req, res, next) {
  let reqBody = req.body;
  let userData = req.user;
  //var i = 0;
  if(reqBody.driverId){
    //console.log("henaaaa --- ")
    RequestService.find({driverId: reqBody.driverId}, function(err, requests){
      if(err || requests.length <= 0){
        return res.json({
          success: false,
          message: err || "no requests found"
        })
      }
      else{
        res.json({
          success: true,
          requests: requests
        })
      }
    })
  }
  else{
    RequestService.find(
    {
      driverId: userData._doc._id,
    },
    (err, requests) => {
      if (err || requests.length <= 0) {
        return res.json({
          success: false,
          message: err || "no requests"
        });
      } else {
        res.json({
          success: true,
          message: requests
        });
      }
    });
  }
  
};
// driver cancel after already accepted it
exports.cancelAcceptedRequest = function(req, res, next) {
  let requestData = req.body;
  let userData = req.user;

  RequestService.findOneAndUpdate(
    {
      _id: requestData.requestId
    },
    {
      $set: {
        status: "canceled",
        canceledBy: userData._doc._id
      }
    },
    {
      new: true
    },
    function(err, doc) {
      if (!doc) {
        res.json({
          success: false,
          message: "falied To cancel"
        });
      } else {
    //     db.ref("/requests_data/" + requestData.requestId + "/").update({
    //   status: "canceled"
    // });
        // let reqIdToUse = requestData.requestId;
        // db.ref("/drivers/" + req.user._doc._id + "/" + reqIdToUse).set({
        //   visited: true
        // });
        pushModel.findOne(
          {
            userId: doc.userId
          },
          (err, userFcmToken) => {
            if (err) {
              res.json({
                success: false,
                message: "error"
              });
            } else if (userFcmToken) {
              // console.log("userFcmToken", userFcmToken)
              var message = {
                to: userFcmToken.fcmToken, // required fill with device token or topics
                collapse_key: "your_collapse_key",
                data: {
                  notificationType: "canceledReq"
                },
                notification: {
                  title: "Request canceld",
                  body: "sorry your request has been canceld"
                }
              };
              //promise style
              fcm
                .send(message)
                .then(function(response) {
                  res.json({
                    success: true,
                    message: response
                  });
                  // console.log("Successfully sent with response: ", response);
                })
                .catch(function(err) {
                  console.log("Something has gone wrong!", err);
                });
            }
          }
        );
      }
    }
  );
};

exports.estimateFare = function(req, res, next) {
  let reqBody = req.body;
  let userData = req.user;
  var firstEstimate;
  var lastEstimate;
  var enqazP ;
  var baseFare;

  servicesModel.find(
    {
      _id:{
            $in:reqBody.serviceId
          }
    },
    (err, service) => {
      if (err || !service) {
        res.json({
          success: false,
          message: err || "no service"
        });
      } else {
        tripSettings.find({}, (err , tripData) => {

        if (err || !tripData) {
              
              res.json({
                success: false,
                message: err || "we need to add settings"
              });
            }else {
        console.log(service)
        if(service[0].name === 'Winch'){
        var dist = geodist({
                lat: reqBody.lat,
                lon: reqBody.long
              }, {
                lat: reqBody.destinationLat,
                lon: reqBody.destinationLong
              });
        console.log(dist + "    ha5ha5ha5")
        if(dist < 40) {
          //console.log(service.baseFare)
          enqazP = (service[0].baseFare + (dist * tripData[0].secondKmPrice)) * 0.20 ;
          if(enqazP < tripData[0].lowLimit) {
          firstEstimate = service[0].baseFare + (dist * tripData[0].secondKmPrice) + tripData[0].lowLimit;
          lastEstimate = service[0].baseFare + (dist * tripData[0].secondKmPrice) + tripData[0].highLimit;
          baseFare = service[0].baseFare
          }else{
          firstEstimate = service[0].baseFare + (dist * tripData[0].secondKmPrice) + tripData[0].lowLimit;
          lastEstimate = service[0].baseFare + (dist * tripData[0].secondKmPrice) + enqazP;
          baseFare = service[0].baseFare
          }

        }else{
          firstEstimate = service[0].baseFare + (dist * tripData[0].secondKmPrice) + tripData[0].lowLimit;
          lastEstimate = service[0].baseFare + (dist * tripData[0].firstKmPrice) + (service[0].baseFare + (dist * tripData[0].firstKmPrice)) * 0.20;
          baseFare = service[0].baseFare
        }
        
        }else if(service.length == 1) {
        firstEstimate = service[0].baseFare + 50
        lastEstimate = service[0].baseFare + tripData[0].lowLimit + 20;
        baseFare = service[0].baseFare
        }else {
        firstEstimate = service[0].baseFare + tripData[0].lowLimit + service[1].baseFare 
        lastEstimate = service[0].baseFare + tripData[0].lowLimit + service[1].baseFare  + 20;
        baseFare = service[0].baseFare + service[1].baseFare
        }
        promoCode.findOne({
          userIds: req.user._doc._id,
          expiresAt: {
            $gt : new Date(Date.now())
          }
        }, function(er, code){
          if(er){
            return res.json({
              success: false,
              message: er
            })
          }
          
          else if(code && new Date(Date.now()) <= code.expiresAt){
            res.json({
              success: true,
              firstEstimate: firstEstimate,
              lastEstimate: lastEstimate,
              distance: dist,
              startingFare: baseFare,
              waitingTimeCost: tripData[0].waitingTimePrice,
              kilometersCost: (dist * tripData[0].secondKmPrice) || 2.5,
              promoCodeFormat: code.code,
              promoCodeDiscountRate: code.discountRate
            });
          }
          else{
            res.json({
              success: true,
              firstEstimate: firstEstimate,
              lastEstimate: lastEstimate,
              distance: dist,
              startingFare: baseFare,
              waitingTimeCost: tripData[0].waitingTimePrice,
              kilometersCost: (dist * tripData[0].secondKmPrice) || 2.5
            });
          }
        })
        // res.json({
        //   success: true,
        //   firstEstimate: firstEstimate,
        //   lastEstimate: lastEstimate,
        //   distance: dist,
        //   startingFare: baseFare,
        //   waitingTimeCost: tripData[0].waitingTimePrice,
        //   kilometersCost: (dist * tripData[0].secondKmPrice) || 2.5
        // });
      }
        })
      }
    }
  );
};

exports.inRequestDriver = function(req, res, next) {
  let userData = req.user;
  RequestService.findOne(
    {
      driverId: userData._doc._id,
      status:{
       $in: ["accepted", "running"]
     }
    },
    (err, request) => {
      if (err || !request) {
        res.json({
          success: false,
          message: err || 'no request'
        });
      } else {
        var dist = geodist({
                lat: request.acceptReqLocation.lat,
                lon: request.acceptReqLocation.long
              }, {
                lat: request.userLocation.lat,
                lon: request.userLocation.long
              });
        customer.findOne({
        _id: request.userId
        },
        (err, customer) => {
          if (err || !customer) {
        res.json({
          success: false,
          message: err || "no customer"
        });
      }else{
        Rating.findOne({userId: customer._id}, (e, rate) => {
          if(e){
            return res.json({
              success: false,
              message: e
            })
          }
          else{
            res.json({
              success: true,
              message: request,
              //serviceName: service.name,
              distance: dist,
              userPhoneNumber: customer.phoneNumber,
              rating: rate ? Number.parseFloat(rate.rating).toFixed(2) : 5.0
            });
          }
        })
      //   servicesModel.findOne({
      //   _id: request.serviceId
      //   },
      //   (err, service) => {
      //     if (err || !service) {
      //   res.json({
      //     success: false,
      //     message: err || "no service"
      //   });
      // }else{
        
  //       res.json({
  //         success: true,
  //         message: request,
  //         //serviceName: service.name,
  //         distance: dist,
  //         userPhoneNumber: customer.phoneNumber
  //  });
     // }
     // })
    }

      });
       
}
})
}
exports.inRequestCustomer = function(req, res, next) {
let userData = req.user;
var request ;
  RequestService.findOne(
    {
      _id:req.body.requestId,
      userId: userData._doc._id,
      status:{
       $in: ["accepted","running","finished","requested"]
     }
    },
    (err, request) => {
      console.log(request)
      if (err || !request) {
        res.json({
          success: false,
          message: err || "no request"
        });
      }
      
      else {
        var dist = geodist({
                lat: request.acceptReqLocation.lat,
                lon: request.acceptReqLocation.long
              }, {
                lat: request.userLocation.lat,
                lon: request.userLocation.long
              });
        vendor.findOne({
        _id: request.driverId
        },
        (err, vendor) => {
          if (err || !vendor) {
        res.json({
          success: false,
          message: err || "no vendor"
        });
      }//else{
      //   servicesModel.findOne({
      //   _id: request.serviceId
      //   },
      //   (err, service) => {
      //     if (err || !service) {
      //   res.json({
      //     success: false,
      //     message: err || "no service"
      //   });
      // }
      else{
        vendorCar.findOne({
        driverId: vendor._id
        },
        (err, car) => {
          if (err || !car) {
        res.json({
          success: false,
          message: err || "no car"
        });
      }else{
        Rating.findOne({userId: vendor._id}, (e, rate) => {
          if(e){
            return res.json({
              success: false,
              message: e
            })
          }
          else{
            res.json({
              success: true,
              message: request,
              //serviceName: service.name,
              distance: dist,
              driverPhoneNumber: vendor.phoneNumber,
              driverName: vendor.name,
              carNumber: car.carNumber,
              rating: rate ? Number.parseFloat(rate.rating).toFixed(2) : 5.0
            });
          }
        })
        // res.json({
        //   success: true,
        //   message: request,
        //   //serviceName: service.name,
        //   distance: dist,
        //   driverPhoneNumber: vendor.phoneNumber,
        //   driverName: vendor.name,
        //   carNumber: car.carNumber
        // });
      }
      })
    }
      // });
      //   }
        });
      }
    })
}

exports.RequestCustomer = function(req, res, next) {
let userData = req.user;
var request ;
  RequestService.findOne(
    {
      userId: userData._doc._id,
      status:{
       $in: ["accepted", "running" , "requested"]
     }
    },
    (err, request) => {
      console.log(request)
      if (err || !request) {
        res.json({
          success: false,
          message: err || 'no request'
        });
      } else if(request.status === "requested") {
        res.json({
          success: true,
          message: request
        });
      }
      else{
        var dist = geodist({
                lat: request.acceptReqLocation.lat,
                lon: request.acceptReqLocation.long
              }, {
                lat: request.userLocation.lat,
                lon: request.userLocation.long
              });
        vendor.findOne({
        _id: request.driverId
        },
        (err, vendor) => {
          if (err || !vendor) {
        res.json({
          success: false,
          message: err || "no vendor"
        });
       }else{
      //   servicesModel.findOne({
      //   _id: request.serviceId
      //   },
      //   (err, service) => {
      //     if (err || !service) {
      //   res.json({
      //     success: false,
      //     message: err || "no service"
      //   });
      // }else{
        vendorCar.findOne({
        driverId: vendor._id
        },
        (err, car) => {
          if (err || !car) {
        res.json({
          success: false,
          message: err || "no car"
        });
      }else{
        Rating.findOne({userId: vendor._id}, (e, rate) => {
          if(e){
            return res.json({
              success: false,
              message: e
            })
          }
          else{
            res.json({
              success: true,
              message: request,
              //serviceName: service.name,
              distance: dist,
              driverPhoneNumber: vendor.phoneNumber,
              driverName: vendor.name,
              carNumber: car.carNumber,
              rating: rate ? Number.parseFloat(rate.rating).toFixed(2) : 5.0
            });
          }
        })
        // res.json({
        //   success: true,
        //   message: request,
        //  // serviceName: service.name,
        //   distance: dist,
        //   driverPhoneNumber: vendor.phoneNumber,
        //   driverName: vendor.name,
        //   carNumber: car.carNumber
        // });
      }
      })
    }
      // });
      //   }
        });
      }
    })
}

exports.assignDriverToRequest = function(req, res, next) {
let reqBody = req.body;

 RequestService.findOne(
    {
      _id: reqBody.requestId,
      status: {
        $in: ["requested"]
      }
    },
    (err, foundRequest) => {
    if (err || !foundRequest) {
        res.json({
          success: false,
          message: err || "already skipped this status"
        });
      }else {
        customer.findOne(
                {
                  _id: foundRequest.userId
                },
                function(err, customerData) {
                  if (err || !customerData) {
                    res.json({
                      success: false,
                      message: err || "customer not found"
                    });
                  } else {
                    servicesModel.find(
                      {
                        _id:{
                          $in:foundRequest.serviceId
                        }
                      },
                      (err, serviceDetails) => {
                        if (err || !serviceDetails) {
                          res.json({
                      success: false,
                      message: err || "service not found"
                    });
                        }else {
          pushModel.findOne(
            {
              userId: reqBody.driverId
            },
            (err, userFcmToken) => {
              if (err || !userFcmToken) {
               res.json({
                 success: false,
                 message: err || "no driver found"
               });
              } else {
                // console.log("userFcmToken", userFcmToken)
                var message = {
                  to: userFcmToken.fcmToken, // required fill with device token or topics
                  collapse_key:
                    "your_collapse_key",
                  data: {
                    customerData: {
                      lat: foundRequest.userLocation.lat,
                      long: foundRequest.userLocation.long,
                      id: userFcmToken.userId,
                      name: customerData.name,
                      phoneNumber:
                        customerData.phoneNumber
                    },
                    carData: {
                      carModel:
                        foundRequest.carModel,
                      carManufactory:
                        foundRequest.carManufactory
                    },
                    requestData: {
                      destinationLocation: 
                        foundRequest.destinationLocation,
                      requestId:
                        foundRequest._id,
                      paymentType:
                        foundRequest.paymentType,
                      distance: reqBody.distance,
                      serviceType:
                        serviceDetails[0].type,
                      serviceName:
                        foundRequest.serviceName
                    },
                    notificationType:
                      "request"
                  },
                  notification: {
                    title: "New Request",
                    body:
                      "Apart by " +
                      reqBody.distance +
                      " km",
                      sound: "alarm.mp3",
                      badge: "1"
                  }

                };
                //promise style
                fcm
                  .send(message)
                  .then(function(response) {
                    res.json({
                      success: true,
                      message: response
                    });
                    console.log(
                      "Successfully sent with response: ",
                      response
                    );
                  })
                  .catch(function(err) {
                    console.log(
                      "Something has gone wrong!",
                      err
                    );
                  });
              }
            }
          );
        }
      })
        }
      })
      }
    })

}
exports.sayHello = function(req, res) {
RequestService.find({status: "canceled"}, function (err, docs) {
  if (err) {
    res.json({
      success: false,
      message: err 
    });
  } else {
    res.json({
      success: true,
      message: docs
    });
  }
}).limit(5)
}