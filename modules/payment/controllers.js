var valid = require('card-validator');
const paymentCard = require('./model');
let customer = require('../auth/customer/customerModel');
const decodeJWT = require('decoding_jwt');
let middlewares = require('../middlewares');
var unirest = require("unirest");
var rn = require('random-number');
let requestsModel = require('../requests/model');
//using random string to generate qr code for invoicing
var randomstring = require("randomstring");

module.exports.addCard = function (req, res, next) {
    let cardData = req.body;
    let userData = req.user;
    if (cardData.token || req.params.hmac) {
        let findQuery = {
            userId: userData._doc._id,
            token: cardData.token
        };
        paymentCard.findOne(findQuery, (err, card) => {
            if (card) {
                res.json({
                    success: false,
                    message: "card already added"
                })
            } else {

                    let newCard = new paymentCard({
                        userId: userData._doc._id,
                        token: cardData.token,
                        maskedPan: cardData.maskedPan,
                        hmac: req.params.hmac
                    });
                    newCard.save((err, done) => {
                        if (!done) {
                            res.json({
                                success: false,
                                message: err
                            })
                        } else {
                            res.json({
                                success: true,
                                message: "card saved successfully" 
                            });
                        }
                    });
                
            }
        });

    } else {
        res.json(middlewares.checkAllFieldsExist)
    }
};
// cash or change primary credit card to use
module.exports.changePaymentMethod = function (req, res, next) {
    let paymentMethod = req.body;
    let userData = req.user;
    if (paymentMethod.cardId || paymentMethod.method) {
        if (paymentMethod.cardId) {
            let findQuery = {
                userId: userData._doc._id
            };
            paymentCard.findOneAndUpdate(findQuery, {
                $set: {
                    primaryMethod: paymentMethod.method,
                    primaryCardId: paymentMethod.cardId
                }
            }, {
                new: true
            }, function (err, card) {
                if (card) {
                    res.json({
                        success: true,
                    })
                } else {
                    res.json({
                        success: false,
                    })
                }
            });
        } else {
            let findQuery = {
                userId: userData._doc._id
            };
            paymentCard.findOneAndUpdate(findQuery, {
                $set: {
                    primaryMethod: paymentMethod.method
                }
            }, {
                new: true
            }, function (err, card) {
                if (card) {
                    res.json({
                        success: true,
                    })
                } else {
                    res.json({
                        success: false,
                    })
                }
            });
        }
    } else {
        res.json(middlewares.checkAllFieldsExist)
    }
}

module.exports.getUserCards = function (req, res, next) {

    // console.log("dataaaaa2" , req.params);
    // res.json({
    //             success: true,
    //             data2: req.params
    //         })
    let userData = req.user;
    paymentCard.find({
        userId: userData._doc._id
    }, (err, cards) => {
        if (!cards) {
            res.json({
                success: false,
                message: "user has no cards"
            })
        } else {
            res.json({
                success: true,
                cards: cards
            })
        }
    });
}

exports.paymentIndex = function (req, res) {
var gen = rn.generator({
          min: -900000,
          max: 900000,
          integer: true
        });
generatedNum = gen(100000);
    customer.findById(req.user._doc._id, (err , user) => {
      
      if(err){
        res.json({
            success: false,
            message: "no user"
        })
      }else {
        var req = unirest("POST", "https://accept.paymobsolutions.com/api/auth/tokens");
        req.headers({
            "content-type": "application/json"
        });
        req.type("json");
        req.send({
            "username": process.env.paymobUserName,
            "password": process.env.paymobPass,
        });

        req.end(function (resOne) {
           console.log("resOne", resOne.body);
            if (resOne.error) {
                console.log("resOne errrrrr" , resOne.error);
            } 
            else {
                var req = unirest("POST", "https://accept.paymobsolutions.com/api/ecommerce/orders");

                req.query({
                    "token": resOne.body.token
                });

                req.headers({
                    "content-type": "application/json"
                });

                req.type("json");
                req.send({
  "delivery_needed": "false",
  "merchant_id": "550",      // merchant_id obtained from step 1
  "amount_cents": "100",
  "currency": "EGP",
  "merchant_order_id":generatedNum,  // unique alpha-numerice value, example: E6RR3
  "items": [],
  "shipping_data": {      // Mandatory if the delivery is needed
    "apartment": "803", 
    "email": user.email, 
    "floor": "42", 
    "first_name": user.name, 
    "street": "Ethan Land", 
    "building": "8028", 
    "phone_number": user.phoneNumber, 
    "postal_code": "01898", 
    "city": "cairo", 
    "country": "EGYPT", 
    "last_name": user.name, 
    "state": "Utah"
  }
});

                req.end(function (resTwo) {
                    if (res.error) {
                        throw new Error(resTwo.error);
                    } else {
                        //console.log(resTwo.body)
                        console.log("twoooooooooooooooooo", resTwo.body)
                        var unirest = require("unirest");

                        var req = unirest("POST", "https://accept.paymobsolutions.com/api/acceptance/payment_keys");

                        req.query({
                            "token": resOne.body.token
                        });

                        req.headers({
                            "content-type": "application/json"
                        });

                        req.type("json");
                        req.send({
  "amount_cents": "100", 
  "expiration": 36000, 
  "order_id": resTwo.body.shipping_data.order,    // id obtained in step 2
  "billing_data": {
    "apartment": "803", 
    "email": user.email, 
    "floor": "42", 
    "first_name": user.name, 
    "street": "Ethan Land", 
    "building": "8028", 
    "phone_number": user.phoneNumber, 
    "postal_code": "01898", 
    "city": "cairo", 
    "country": "EGYPT", 
    "last_name": user.name, 
    "state": "Utah"
  }, 
  "currency": "EGP", 
  "integration_id":706 // card integration_id will be provided upon signing up
});

                        req.end(function (resThree) {
                            if (res.error) {
                                console.log("Threeeeeeeeeeeeeee" , resThree.error);
                                res.json({
                                        success: false,
                                        message: "NO Order",
                                    })
                                
                            } else {
                                //console.log("Threeeeeeeeeeeeeee" , resThree.body);
                                console.log("RESTHREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEe", resThree.body);
                                      res.json({
                                        success: true,
                                        token: resThree.body.token,
                                    })
                              

                            }
                        });
                    }
                });
            }

        });
}
    });
};

exports.payTrip = function (req, res) {
    var amount
var gen = rn.generator({
          min: -900000,
          max: 900000,
          integer: true
        });
generatedNum = gen(100000);
    requestsModel.findOne({_id:req.body.requestId}, (err , request) => {
      
      if(err){
        res.json({
            success: false,
            message: "no request"
        })
      }else {
        amount = request.finalCost*100;
        paymentCard.findOne({userId:request.userId}, (err , card) => {
      
      if(err){
        res.json({
            success: false,
            message: "no card"
        })
      }else {
        customer.findOne({_id:request.userId}, (err , user) => {
      
      if(err){
        res.json({
            success: false,
            message: "no user"
        })
      }else {
        var req = unirest("POST", "https://accept.paymobsolutions.com/api/auth/tokens");
        req.headers({
            "content-type": "application/json"
        });
        req.type("json");
        req.send({
            "username": "Mohamed_Nassim",
            "password": "Enq@zd0tnet"
        });

        req.end(function (resOne) {
           console.log("resOne", resOne.body);
            if (resOne.error) {
                console.log("resOne errrrrr" , resOne.error);
            } 
            else {
                var req = unirest("POST", "https://accept.paymobsolutions.com/api/ecommerce/orders");

                req.query({
                    "token": resOne.body.token
                });

                req.headers({
                    "content-type": "application/json"
                });

                req.type("json");
                req.send({
  "delivery_needed": "false",
  "merchant_id": "550",      // merchant_id obtained from step 1
  "amount_cents":amount.toString(),
  "currency": "EGP",
  "merchant_order_id":generatedNum,  // unique alpha-numerice value, example: E6RR3
  "items": [],
  "shipping_data": {      // Mandatory if the delivery is needed
    "apartment": "803", 
    "email": user.email, 
    "floor": "42", 
    "first_name": user.name, 
    "street": "Ethan Land", 
    "building": "8028", 
    "phone_number": user.phoneNumber, 
    "postal_code": "01898", 
    "city": "cairo", 
    "country": "EGYPT", 
    "last_name": user.name, 
    "state": "Utah"
  }
});

                req.end(function (resTwo) {
                    if (res.error) {
                        throw new Error(resTwo.error);
                    } else {
                        //console.log(resTwo.body)
                        console.log("twoooooooooooooooooo", resTwo.body)
                        var unirest = require("unirest");

                        var req = unirest("POST", "https://accept.paymobsolutions.com/api/acceptance/payment_keys");

                        req.query({
                            "token": resOne.body.token
                        });

                        req.headers({
                            "content-type": "application/json"
                        });

                        req.type("json");
                        req.send({
  "amount_cents": amount.toString(), 
  "expiration": 36000, 
  "order_id": resTwo.body.shipping_data.order,    // id obtained in step 2
  "billing_data": {
    "apartment": "803", 
    "email": user.email, 
    "floor": "42", 
    "first_name": user.name, 
    "street": "Ethan Land", 
    "building": "8028", 
    "phone_number": user.phoneNumber, 
    "postal_code": "01898", 
    "city": "cairo", 
    "country": "EGYPT", 
    "last_name": user.name, 
    "state": "Utah"
  }, 
  "currency": "EGP", 
  "integration_id":2811 // card integration_id will be provided upon signing up
});

                        req.end(function (resThree) {
                            if (res.error) {
                                console.log("Threeeeeeeeeeeeeee" , resThree.error);
                                res.json({
                                        success: false,
                                        message: "NO Order",
                                    })
                                
                            } else {
                                //console.log("Threeeeeeeeeeeeeee" , resThree.body);
                                // console.log("RESTHREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEe", resThree.body);
                                //       res.json({
                                //         success: true,
                                //         token: resThree.body.token,
                                //     })
                                      var unirest = require("unirest");

                                      var req = unirest("POST", "https://accept.paymobsolutions.com/api/acceptance/payments/pay");
                                      req.query({
                            "token": resOne.body.token
                        });

                        req.headers({
                            "content-type": "application/json"
                        });

                        req.type("json");
                        req.send({
"source": {
"identifier": card.token, // will be the token from card

"subtype": "TOKEN",
},
"billing": {
    "apartment": "803", 
    "email": user.email, 
    "floor": "42", 
    "first_name": user.name, 
    "street": "Ethan Land", 
    "building": "8028", 
    "phone_number": user.phoneNumber, 
    "postal_code": "01898", 
    "city": "cairo", 
    "country": "EGYPT", 
    "last_name": user.name, 
    "state": "Utah"
  },
"payment_token": resThree.body.token // token obtained in step 3
});
                        req.end(function (resFour) {
                            if (res.error) {
                                console.log("Foooooooooour" , resFour.error);
                                res.json({
                                        success: false,
                                        message: "Order Not Done",
                                    })
                                
                            } else {
                                
                                console.log("Foooooooooour" , resFour.body);
                                res.json({
                                        success: true,
                                        message: "Order Done",
                                    })
                            }
                            });
                            }
                        });
                    }
                });
            }

        });
 }

        });
}

        });
}

    });
};