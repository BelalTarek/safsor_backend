const mongoose = require('mongoose')
const config = require('config')

const paymentCardSchema = mongoose.Schema({
    userId: {
        type: String,
        //required: true
    },
    token: {
        type: String,
        //required: true
    },
    hmac: {
        type: String,
    },
    primaryMethod: {
        type: String
    },
    primaryCardId: {
        type: String
    },
    maskedPan: {
        type: String
    }
});

module.exports = mongoose.model('paymentCard', paymentCardSchema)