var express = require('express');
var router = express.Router();
//require controllers
var paymentController = require('./controllers');
var middlewares = require('../middlewares');
var jwt = require('express-jwt');

//Routers
/**

 * @api {post} /payment/add-card request a service
 * @api {post} /payment/add-card add payment card

 * @apiName addCard
 * @apiGroup paymentModule
 * @apiParam {string} number payment card number
 * @apiParam  {boolean} csv csv of payment card
 * @apiParam  {boolean} expMonth expiration month
 * @apiParam  {boolean} expYear expiration year
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *     cardDetails: {}
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *   }
 */
router.post('/add-card', jwt({
    secret: process.env.secret
}), paymentController.addCard);
/**
 * @api {post} /requests/change-payment-method change user's primary payment method
 * @apiName changePaymentMethods
 * @apiGroup paymentModule
 *
 * @apiParam {string} method primary payment method.
 * @apiParam  {string} [cardId] id of card if the user choose not to pay cash.
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *   }
 */

router.post('/change-payment-method', jwt({
    secret: process.env.secret
}), paymentController.changePaymentMethod);
/**
 * @api {post} /payment/get-my-cards get all user cards
 * @apiName getUserCardsList
 * @apiGroup paymentModule
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *     cards: cardsList
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *   }
 */
router.get('/get-my-cards', jwt({
    secret: process.env.secret
}), paymentController.getUserCards);
/**
 * @api {post} /payment/payment-index payment-index
 * @apiName paymentIndex
 * @apiGroup paymentModule
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *     cards: cardsList
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *   }
 */
router.get('/payment-index', jwt({
    secret: process.env.secret
}), paymentController.paymentIndex);

router.post('/payTrip', jwt({
    secret: process.env.secret
}), paymentController.payTrip);



module.exports = router;