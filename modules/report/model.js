const mongoose = require('mongoose')
const bcryptjs = require('bcryptjs')

const reportSchema = mongoose.Schema({
	driverId: {
		type: String
	},
	connectionHistory: {
		numOfReqRecieved: {
		 type: Number,
		  default: 0
		},
		numOfReqAccepted: {
		 type: Number,
		  default: 0
		},
		numOfTrips: {
			type: Number,
			 default: 0
			},
		totalProfit: {
			type: Number,
			 default: 0.0
			},
		startTime: {
			type: Date,
			 default: null
			},
		endTime: {
			type: Date,
			 default: null
			},
		numOfConnectingHours: {
			days: {
				type: Number
			},
			hours: {
				type: Number
			},
			minutes: {
				type: Number
			}
		},
		rating: {
			type: Number,
			 default: 5.0
			},
		enqazPercentage: {
			type: Number,
			 default: 0.0
			},
		kilometers: {
			type: Number,
			 default: 0.0
			},
		trips: {
			type: [String]
		}
	}
})

const Report = module.exports = mongoose.model('Report', reportSchema);