var middlewares = require('../middlewares')
var Report = require('./model')
var Rating = require('../rating/model')
var Request = require('../requests/model')
var Vendor = require('../auth/vendor/vendorModel')
const serviceModel = require('../services/model');
let invoiceModel = require('../invoice/model');
var Notifications = require('../notifications/model')

exports.getCurrentConnection = function(req, res){
	var reqBody = req.body;
	if(req.user){
		if(reqBody.connectionId){
			Report.findOne({
				driverId: req.user._doc._id, 
				_id: reqBody.connectionId
				}, function(err, connection){
					if(err || !connection){
						return res.json({
							success: false,
							message: err || 'Connection data not found'
						})
					}
					else{
						var acceptanceRate = (connection.connectionHistory.numOfReqAccepted / connection.connectionHistory.numOfReqRecieved) * 100;
						var now = new Date(Date.now());
						var numOfConnectingHours = new Date(now - connection.connectionHistory.startTime)
						console.log('now-- '+new Date(Date.now())+' start-- '+connection.connectionHistory.startTime)
						console.log(numOfConnectingHours)
						var days;
						if((now.getDate() === connection.connectionHistory.startTime.getDate()) 
							&& (now.getDay() === connection.connectionHistory.startTime.getDay())){
							days = 0;
						}
						else if(now.getUTCHours() < connection.connectionHistory.startTime.getUTCHours() || 
						   now.getMinutes() < connection.connectionHistory.startTime.getMinutes()){
							days = numOfConnectingHours.getDate()-2;
						}
						else{
							days = numOfConnectingHours.getDate()-1;
						}
						res.json({
							success: true,
							connectionData: {
								connectionId : connection._id,
								acceptanceRate: Number.parseFloat(acceptanceRate).toFixed(2),
								numOfTrips: connection.connectionHistory.numOfTrips,
								totalProfit: connection.connectionHistory.totalProfit,
								connectedSince: {
									//hh: numOfConnectingHours,
									days: days, 
									hours: numOfConnectingHours.getUTCHours(), 
									minutes: numOfConnectingHours.getMinutes()
								} 
							},
							//connection: connection
						})
					}
				})
		}
		else{
			return res.json(middlewares.checkAllFieldsExist)
		}
		
	}
	else{
		return res.json({
			success: false,
			message: "Authorization Header is missing or invalid"
		})
	}
	
}

exports.getLastConnection = function(req, res){
	var reqBody = req.body;
	if(req.user){
		if(reqBody.connectionId){
			Report.findOne({
				_id: reqBody.connectionId,
				driverId: req.user._doc._id
			}, function(err, connection){
				if(err || !connection){
					return res.json({
						success: false,
						message: err || 'Connection data not found'
					})
				}
				else{
					var numOfConnectingHours = new Date(connection.connectionHistory.endTime - connection.connectionHistory.startTime);
					console.log(numOfConnectingHours+' '+numOfConnectingHours.getDate())
					console.log('start-- '+connection.connectionHistory.startTime+' end-- '+connection.connectionHistory.endTime)
					var days;
					if((connection.connectionHistory.endTime.getDate() === connection.connectionHistory.startTime.getDate()) 
						&& (connection.connectionHistory.endTime.getDay() === connection.connectionHistory.startTime.getDay())){
						days = 0;
					}
					else if(connection.connectionHistory.endTime.getUTCHours() < connection.connectionHistory.startTime.getUTCHours() || 
					   connection.connectionHistory.endTime.getMinutes() < connection.connectionHistory.startTime.getMinutes()){
						days = numOfConnectingHours.getDate()-2;
					}
					else{
						days = numOfConnectingHours.getDate()-1;
					}
					connection.connectionHistory.numOfConnectingHours = { 
								days: days,
								hours: numOfConnectingHours.getUTCHours(), 
								minutes: numOfConnectingHours.getMinutes()
					}
					connection.save(function(errr, result){
						if(errr || !result){
							return res.json({
								success: false,
								message: errr || "error updating connection"
							})
						}
						else{
							res.json({
								success: true,
								connectionData: {
									connectionId : connection._id,
									numOfConnectingHours: result.connectionHistory.numOfConnectingHours /*{ 
										//hh: numOfConnectingHours, 
										hours: numOfConnectingHours.getUTCHours(), 
										minutes: numOfConnectingHours.getMinutes()
									}*/,
									numOfTrips: connection.connectionHistory.numOfTrips,
									totalProfit: connection.connectionHistory.totalProfit
								}
							})
						}
					})
					
				}
			})
		}
		else{
			return res.json(middlewares.checkAllFieldsExist)
		}
		
	}
	else{
		return res.json({
			success: false,
			message: "Authorization Header is missing or invalid"
		})
	}
}

exports.getWeeklyreport = function(req, res){
	if(req.user){
		var now = new Date(Date.now());
		var lastSunday = now.getDate() - now.getDay();

		var lastSaturday;
		var month;
		if(lastSunday > 0){
			lastSaturday = lastSunday - 1;
			month = now.getMonth();

		}
		else{
			var lastDayOfMonth = new Date(now.getFullYear(), now.getMonth(), 0);
			lastSaturday = lastDayOfMonth.getDate() + lastSunday - 1;
			month = now.getMonth() - 1;
		}
		//console.log(new Date(now.getFullYear(), now.getMonth(), 1))
		var dateOfLastSaturday = new Date(now.getFullYear(), month, lastSaturday);
		console.log(lastSaturday+' -- '+lastDayOfMonth+' -- '+dateOfLastSaturday)
		Report.find({
			$and: [
				{
					'connectionHistory.startTime': {
						$gte: dateOfLastSaturday,
						$lte: Date.now()
					}
				},
				{
					$or: [
						{
							'connectionHistory.endTime': {
								$gte: dateOfLastSaturday,
								$lte: Date.now()
							}
						},
						{
							'connectionHistory.endTime': null
						}
					]
				}
			],
			driverId: req.user._doc._id,
			
		}, function(err, reports){
			if(err || reports.length <= 0){
				return res.json({
					success: false,
					message: err || "connections data is not found"
				})
			}
			else{
				var totalWorkingHours = {
					hours: 0,
					minutes: 0,
					days: 0
				};
				var totalProfitWeek = 0;
				var totalRecievedReq = 0;
				var totalAcceptedReq = 0;
				var totalNumOfTrips = 0;
				var totalRating = 0;
				var totalEnqazPercentage = 0;
				var tripsPerWeek = [];

				for (var i = 0; i < reports.length; i++) {
					//console.log('rr -- '+reports['0'])
					if(reports[i].connectionHistory.numOfConnectingHours.hours && reports[i].connectionHistory.numOfConnectingHours.minutes){
						totalWorkingHours.hours += reports[i].connectionHistory.numOfConnectingHours.hours;
						totalWorkingHours.minutes += reports[i].connectionHistory.numOfConnectingHours.minutes;
						totalWorkingHours.days += reports[i].connectionHistory.numOfConnectingHours.days
						console.log(totalWorkingHours)
					}

					//console.log('profit-- '+reports[i].connectionHistory.totalProfit+'  acceptedReq-- '+reports[i].connectionHistory.numOfReqAccepted+'  recievedReq-- '+reports[i].connectionHistory.numOfReqRecieved)
					totalProfitWeek  += reports[i].connectionHistory.totalProfit;
					totalRecievedReq += reports[i].connectionHistory.numOfReqRecieved;
					totalAcceptedReq += reports[i].connectionHistory.numOfReqAccepted;
					totalNumOfTrips += reports[i].connectionHistory.numOfTrips;
					totalRating += reports[i].connectionHistory.rating;
					totalEnqazPercentage += reports[i].connectionHistory.enqazPercentage;
					for (let j = 0; j < reports[i].connectionHistory.trips.length; j++) {
						const element = reports[i].connectionHistory.trips[j];
						tripsPerWeek.push(element)
						
					}

					//reports[i]
				}
				if(totalWorkingHours.minutes >= 60){
					totalWorkingHours.hours += 1;
					totalWorkingHours.minutes = totalWorkingHours.minutes - 60;
				}
				if(totalWorkingHours.hours >= 24){
					totalWorkingHours.days += 1;
					totalWorkingHours.hours -= 24;
				}
				Vendor.findById(req.user._doc._id, function(e, driver){
					if(e || !driver){
						return res.json({
							success: false,
							message: e || "no driver"
						})
					}
					else{
						Request.find(
							{
								_id: {
									$in: tripsPerWeek
								}
							}, function(er, requests){
								if(er || !requests){
									return res.json({
										success: false,
										message: er || 'trips not found'
									})
								}
								else{
									var cash = 0;
									var credit = 0;
									requests.forEach(element => {
										if(element.paymentType === "Cash"){
											cash += element.finalCost
										}
										else{
											credit += element.finalCost
										}
									});
									var setOf = credit - totalEnqazPercentage
									var nextSaturday = now.getDate() + (6 - now.getDay());
									var dateOfNextSaturday = new Date(now.getFullYear(), now.getMonth(), nextSaturday+1)
									console.log("nex sat -- ",nextSaturday, dateOfNextSaturday)
									res.json({
										success: true,
										//message: reports,
										totalWorkingHours: totalWorkingHours,
										acceptanceRate: Number.parseFloat((totalAcceptedReq / totalRecievedReq) * 100).toFixed(2),
										totalProfitWeek: totalProfitWeek,
										totalNumOfTrips: totalNumOfTrips,
										rating: (Number.parseFloat(totalRating/reports.length).toFixed(2)).toString(),
										enqazPercentage: totalEnqazPercentage,
										tripsPerWeek: requests,
										cash: cash,
										setOf: setOf < 0 ? setOf : 0.0,
										setOfDate: dateOfNextSaturday,
										netProfit: totalProfitWeek - totalEnqazPercentage
									})
								}
							})
					}
				})
				

				
				
			}
		})
		// res.json({
		// 	lastSunday: new Date(now.getFullYear(), month, lastSaturday)
		// })
	}
	else{
		return res.json({
			success: false,
			message: "Authorization Header is missing or invalid"
		})
	}
}

exports.summaryFromBeginning = function(req, res){
	if(req.user){
		Report.find({driverId: req.user._doc._id}, function(err, reports){
			if(err || reports.length <= 0){
				return res.json({
					success: false,
					message: err || "no reoprts found"
				})
			}
			else{
				Rating.findOne({
					userId: req.user._doc._id
				}, function(er, rate){
					if(er){
						return res.json({
							success: false,
							message: er
						})
					}
					else{
						var totalWorkingHours = {
							hours: 0,
							minutes: 0,
							days: 0
						};
						var totalKilometers = 0.0;
	
						reports.forEach(element => {
							if(element.connectionHistory.numOfConnectingHours.hours && element.connectionHistory.numOfConnectingHours.minutes){
								totalWorkingHours.hours += element.connectionHistory.numOfConnectingHours.hours;
								totalWorkingHours.minutes += element.connectionHistory.numOfConnectingHours.minutes;
								totalWorkingHours.days += element.connectionHistory.numOfConnectingHours.days
								totalKilometers += element.connectionHistory.kilometers;
								console.log(totalWorkingHours)
							}
						});
						//console.log( Number.parseFloat(rate.rating).toFixed(2))

						res.json({
							success: true,
							rating: rate ? Number.parseFloat(rate.rating).toFixed(2) : "0.00" ,
							totalWorkingHours: totalWorkingHours,
							kilometers: totalKilometers
						})
					}
					
				})
			}
		})
	}
	else{
		return res.json({
			success: false,
			message: "Authorization Header is missing or invalid"
		})
	}
} 

exports.requestsToday = (req, res) => {
	var now = new Date();
	var today = new Date(now.getFullYear(), now.getMonth(), now.getDate())
	//var tommorow = new Date(now.getFullYear(), now.getMonth(), now.getDate()+1)

	console.log(today, now)

	// *

	Request.count({
		createdAt: { $gte: today , $lte: now}
	}, (err, count) => {
		console.log(count)
		if(err){
			res.json({
				success: false,
				message: err 
			})
		}
		else{
			res.json({
				success: true,
				count: count
			})
		}
	})
}

exports.requestsThisWeek = (req, res) => {
	var now = new Date();
	var lastSunday = now.getDate() - now.getDay();

	var lastSaturday;
	var month;
	if(lastSunday > 0){
		lastSaturday = lastSunday - 1;
		month = now.getMonth();

	}
	else{
		var lastDayOfMonth = new Date(now.getFullYear(), now.getMonth(), 0);
		lastSaturday = lastDayOfMonth.getDate() + lastSunday - 1;
		month = now.getMonth() - 1;
	}
		//console.log(new Date(now.getFullYear(), now.getMonth(), 1))
	var dateOfLastSaturday = new Date(now.getFullYear(), month, lastSaturday);
	console.log(lastSaturday+' -- '+lastDayOfMonth+' -- '+dateOfLastSaturday)

	// Request.find({
	// 	createdAt: { $gte: dateOfLastSaturday, $lte: now}
	// }, (err, count) => {
	// 	console.log(count)
	// 	// if(err){
	// 	// 	res.json({
	// 	// 		success: false,
	// 	// 		message: err 
	// 	// 	})
	// 	// }
	// 	// else{
	// 	// 	res.json({
	// 	// 		success: true,
	// 	// 		count: count
	// 	// 	})
	// 	// }
	// })

	Request.count({
		createdAt: { $gte: dateOfLastSaturday , $lte: now}
	}, (err, count) => {
		console.log(count)
		if(err){
			res.json({
				success: false,
				message: err 
			})
		}
		else{
			res.json({
				success: true,
				count: count
			})
		}
	})
}

exports.requestsThisMonth = (req, res) => {
	var now = new Date();
	//var today = new Date(now.getFullYear(), now.getMonth(), now.getDate())

	var firstDayOfMonth = new Date(now.getFullYear(),now.getMonth(), 1)

	// console.log(now, firstDayOfMonth,now.getDate())

	// Request.find({
	// 	createdAt: { $gte: firstDayOfMonth , $lte: now}
	// }, (err, count) => {
	// 	console.log(count)
	// 	if(err){
	// 		res.json({
	// 			success: false,
	// 			message: err 
	// 		})
	// 	}
	// 	else{
	// 		res.json({
	// 			success: true,
	// 			count: count
	// 		})
	// 	}
	// })
	Request.count({
		createdAt: { $gte: firstDayOfMonth , $lte: now},
		status: "finished"
	}, (err, countPerMonth)=> {
		console.log(countPerMonth)
		if(err){
			console.log(err)
			res.json({
				success: false,
				message: err 
			})
		}
		else{
			var countPerDay = [];
			var DayOfMonth = [];
			var indxs = [];
			for(var i=1; i<=now.getDate(); i++){
				indxs.push(i)
			}
			console.log(indxs)
			indxs.forEach(async function(i){
				var day = new Date(now.getFullYear(),now.getMonth(), i)
				var endOfDay = new Date(now.getFullYear(),now.getMonth(), i+1)
				var dayString = day.toDateString()

				// console.log(dayString.substr(4, 3)+i)
				DayOfMonth.push(dayString.substr(4, 3)+i)

				await Request.count({
				createdAt: { $gte: day,$lte: endOfDay},
				status: "finished"
				}, (err, count) => {
					console.log(count)
					if(err){
						console.log(err)

						res.json({
							success: false,
							message: err 
						})
					}
					else{
						countPerDay[i-1] = count
						// countPerDay.push(count)
						
						// res.json({
						// 	success: true,
						// 	count: count
						// })
						console.log( now.getDate(), day.getDate(),dayString, countPerDay.length, countPerDay.includes(undefined), countPerDay)
						if( countPerDay.length == now.getDate() && !countPerDay.includes(undefined)){
							console.log(countPerDay, DayOfMonth)
							res.json({
								success: true,
								countPerMonth: countPerMonth,
								countPerDay: countPerDay,
								DayOfMonth: DayOfMonth
							})
						}
						
						
					}
				})

			})

			
		}
	})

	

	
}
exports.percentagePerService = (req, res) => {
	var now = new Date();
	//var today = new Date(now.getFullYear(), now.getMonth(), now.getDate())

	var firstDayOfMonth = new Date(now.getFullYear(),now.getMonth(), 1)

	// Request.find({
	// 	createdAt: { $gte: firstDayOfMonth , $lte: now},
	// 	status: "finished"
	// }, (err, reqs) => {
	// 	res.json({
	// 		reqs : reqs
	// 	})
	// })

	Request.count({
		createdAt: { $gte: firstDayOfMonth , $lte: now},
		status: "finished"
	}, (err, countPerMonth)=> {
		console.log(countPerMonth)
		if(err){
			res.json({
				success: false,
				message: err 
			})
		}
		else{
			var services = ["Winch", "Gasoline","Tires", "Battery"];
			var percentage = [];

			services.forEach(async function(service){
			// for(var i=0; i<services.length; i++){
				console.log(service)
				await Request.count({
					createdAt: { $gte: firstDayOfMonth , $lte: now},
					status: "finished",
					serviceName: service
				}, (err, countPerService) => {
					console.log(countPerService)
					if(err){
						res.json({
							success: false,
							message: err 
						})
					}
					else{
						var percent = countPerMonth == 0 ? 0 : (countPerService/countPerMonth)*100
						percentage[services.indexOf(service)] = Number(Number.parseFloat(percent).toFixed(2))
						// percentage.push(Number(Number.parseFloat(percent).toFixed(2)))
						
						if(percentage.length == services.length && !percentage.includes(undefined)){
							console.log(percentage, services)
							res.json({
								success: true,
								countPerMonth: countPerMonth,
								services: services,
								percentagePerService: percentage
							})
						}
					}
				})
			})
		}
	})
}

var incomePerService = 0;

exports.incomePerService = (req, res) =>{
	var now = new Date();
	// var now = new Date(2018, 11, 2)
	//var today = new Date(now.getFullYear(), now.getMonth(), now.getDate())

	var firstDayOfMonth = new Date(now.getFullYear(),now.getMonth(), 1)
	// var firstDayOfMonth = new Date(now.getFullYear()  , 10, 2)

	
	console.log(firstDayOfMonth, now)
	invoiceModel.aggregate([{
		$match: { 
			$and: [
				{date: { $gte: firstDayOfMonth , $lte: now}}
				// {createdAt: { $gte: firstDayOfMonth , $lte: now}},
				// {status: "finished"}
			]
		}
	}, {
		$group: {
			 _id : null,
	        totalIncome: {
	            $sum : "$finalCost"
	        }
	        
		}

	}], (err, result) => {
		console.log(result)
		if(err){
			res.json({
				success: false,
				message: err 
			})
		}
		else{
			var services = ["Winch", "Gasoline","Tires", "Battery"];
			var income = [];
			var service;
			var index = 0;

			services.forEach(async function(service){

			
			// for (var i = 0; i < services.length; i++) {
				
				console.log(service)

				await invoiceModel.find({
					date: { $gte: firstDayOfMonth , $lte: now},
					// status: "finished",
					serviceName: service
				}, (er, requests) => {
					if(er){
						res.json({
							success: false,
							message: er
						})
					}
					else{
						incomePerService = 0;
						var inc = [];
						
						if(requests.length == 0){
							income[services.indexOf(service)] = 0
							// income[services.indexOf(service)] = 0
							index++
							console.log("rq", requests)
							if(Object.keys(income).length == services.length){
								console.log(income, "ii", income.length, income[services.indexOf(service)])
								res.json({
									success: true,
									incomePerMonth: result.length == 0 ? 0 : result[0].totalIncome,
									services: services,
									incomePerService: income
								})
							}
						}
						for (var j = 0; j < requests.length; j++){
							console.log('rq', requests[j].serviceName, service)
							calculateIncome(incomePerService, requests[j], service)
							.then( incom => {
								incomePerService = incom;
								console.log(incomePerService, "in loop")
								inc.push(incomePerService)
								// console.log(j, "j")

								if(inc.length == requests.length){
									// console.log("here", inc)
									var sum = 0;
									for (var l = 0; l < inc.length; l++) {
										sum += inc[l]
									}
									income[services.indexOf(service)] = sum
									// income[services.indexOf(service)] = sum
									index++
									console.log(sum, "ips", index-1, income, income.length, income[services.indexOf(service)])
									if(income.length == services.length && !income.includes(undefined)){
										console.log(income, "ii")
										res.json({
											success: true,
											incomePerMonth: result.length == 0 ? 0 : result[0].totalIncome,
											services: services,
											incomePerService: income
										})
									}
									
								}
							})
							
							

						}
						
						
						// income.push(incomePerService)
						
					}

				})
					// invoiceModel.aggregate([{
					// 	$match: { 
					// 		$and: [
					// 			// {createdAt: { $gte: firstDayOfMonth , $lte: now}},
					// 			// {status: "finished"},
					// 			{date: { $gte: firstDayOfMonth , $lte: now}},
					// 			{serviceName: services[i]}
					// 		]
					// 	}
					// }, {
					// 	$group: {
					// 		 _id : null,
					//         totalIncome: {
					//             $sum : "$finalCost"
					//         }
					        
					// 	}

					// }], (err, rslt) =>{
					// 	if(err){
					// 		res.json({
					// 			success: false,
					// 			message: err 
					// 		})
					// 	}
					// 	else{
					// 		console.log(rslt)
					// 		if(rslt.length == 0) income.push(0)
					// 		else income.push(rslt[0].totalIncome)

					// 		if(income.length == services.length){
					// 			console.log(income)
					// 			res.json({
					// 				success: true,
					// 				incomePerMonth: result.length == 0 ? 0 : result[0].totalIncome,
					// 				services: services,
					// 				incomePerService: income
					// 			})
					// 		}
					// 	}
					// })
			}) 
		}
	})

	// Request.aggregate({
	// 	createdAt: { $gte: firstDayOfMonth , $lte: now},
	// 	status: "finished"
	// }, {
	// 	$group: {
	// 		 _id : null,
	//         totalIncome: {
	//             $sum : "$finalCost"
	//         }
	        
	// 	}
	// }, (err, result) => {
	// 	console.log(result)
	// })
}

exports.runningRequests = (req, res) => {

	// Request.find({
	// 	status: {
	// 		$in: ["running", "accepted"]
	// 	}
	// }, (e, reqs) => {
	// 	console.log(reqs)
	// })

	Request.count({
		// createdAt: { $gte: firstDayOfMonth , $lte: now},
		status: {
			$in: ["running", "accepted"]
		}
	}, (err, count)=> {
		if(err){
			res.json({
				success: false,
				message: err
			})
		}
		else{
			console.log(count)
			res.json({
				success: true,
				runningRequests: count
			})
		}
	})
}

exports.issues = (req, res) => {
	// Request.find({status: "requested"}, (e, nots) => {console.log(nots)})
	var count = 0;
	var requests = [];
	var admins = [];
	Notifications.find({status: "assigned"}, (err, notifications) => {
		if(err || !notifications){
			res.json({
				success: false,
				message: err || "no issues"
			})
		}
		else{
			console.log("nots.. ", notifications)
			var index = 0;
			notifications.forEach( async function(notification){
				// console.log("id", notification.requestId)
				
				var request = await Request.findById(notification.requestId, (er, request) => {
					if(er || !request){
						res.json({
							success: false,
							message: "request not found"
						})
					}
					else{
						// console.log(request)
						
					}
				})
				if(request.status == "requested"){
					var obj = {
						name: notification.adminName,
						id: notification.adminId,
						numOfReqs: 1
					}
					// if(admins.length == 0){
					// 	admins.push(obj)
					// 	console.log(admins)
					// }
					// else{
						var pos = admins.map(function(e) { return e.id; }).indexOf(notification.adminId);
						console.log(pos, "pos")
						if(pos >= 0){
							admins[pos].numOfReqs = admins[pos].numOfReqs + 1
							console.log(admins)
							count ++;
						}
						else{
							admins.push(obj)
							console.log(admins)
							count ++;
						}
					// }
					// console.log("req.. ", request)
					
					// requests.push(request)
					// console.log(requests.indexOf(request))
					// admins[requests.indexOf(request)] = notification.adminName
				}
				// else{
				// 	return
				// }
				index++;
				console.log("indx", index)
				if(index == notifications.length){
					admins.forEach(function(admin){
						delete admin.id
					})
					console.log(admins)
					res.json({
						success: true,
						numOfReqs: count,
						// requests: requests,
						admins: admins
					})
				}
				
			})
		}
	})
}
exports.budgetSummary = (req, res) =>{
	var now = new Date();
	// var now = new Date(2018, 11, 2)
	//var today = new Date(now.getFullYear(), now.getMonth(), now.getDate())

	var firstDayOfMonth = new Date(now.getFullYear(),now.getMonth(), 1)
	// var firstDayOfMonth = new Date(now.getFullYear()  , 10, 2)

	
	console.log(firstDayOfMonth, now)
	invoiceModel.aggregate([{
		$match: { 
			$and: [
				{date: { $gte: firstDayOfMonth , $lte: now}}
				// {createdAt: { $gte: firstDayOfMonth , $lte: now}},
				// {status: "finished"}
			]
		}
	}, {
		$group: {
			 _id : null,
	        totalIncome: {
	            $sum : "$finalCost"
	        }
	        
		}

	}], (err, profitPerMonth) => {
		console.log(profitPerMonth)
		if(err){
			res.json({
				success: false,
				message: err 
			})
		}
		else{
			var lastSunday = now.getDate() - now.getDay();

			var lastSaturday;
			var month;
			if(lastSunday > 0){
				lastSaturday = lastSunday - 1;
				month = now.getMonth();

			}
			else{
				var lastDayOfMonth = new Date(now.getFullYear(), now.getMonth(), 0);
				lastSaturday = lastDayOfMonth.getDate() + lastSunday - 1;
				month = now.getMonth() - 1;
			}
				//console.log(new Date(now.getFullYear(), now.getMonth(), 1))
			var dateOfLastSaturday = new Date(now.getFullYear(), month, lastSaturday);
			console.log(lastSaturday+' -- '+lastDayOfMonth+' -- '+dateOfLastSaturday)
			invoiceModel.aggregate([{
				$match: { 
					$and: [
						{date: { $gte: dateOfLastSaturday , $lte: now}}
						// {createdAt: { $gte: firstDayOfMonth , $lte: now}},
						// {status: "finished"}
					]
				}
			}, {
				$group: {
					 _id : null,
					totalIncome: {
						$sum : "$finalCost"
					}
					
				}
		
			}], (err, profitPerWeek) => {
				console.log(profitPerWeek)
				if(err){
					res.json({
						success: false,
						message: err 
					})
				}
				else{
					var firstDayOfYear = new Date(now.getFullYear(), 0, 1)
					console.log(firstDayOfYear, "fd year")
					invoiceModel.find({ date: { $gte: firstDayOfYear , $lte: now}}, (e, inv) => {
						console.log(inv)
					})
					invoiceModel.aggregate([{
						$match: { 
							$and: [
								{date: { $gte: firstDayOfYear , $lte: now}}
								// {createdAt: { $gte: firstDayOfMonth , $lte: now}},
								// {status: "finished"}
							]
						}
					}, {
						$group: {
							 _id : null,
							Cash: {
								$sum : {
									$cond: [{"paymentType": "Cash"}, "$finalCost", 0]
								}
							},
							Credit: {
								$sum: {
									$cond: [{"paymentType": "Cash"}, 0, "$finalCost"]
								}
							}
							
						}
				
					}], (err, profitPerYear) => {
						console.log(profitPerYear)
						if(err){
							res.json({
								success: false,
								message: err 
							})
						}
						else{
							res.json({
								success: true,
								profitPerYear: profitPerYear.length > 0 ? [
									{name: "Cash", value: profitPerYear[0].Cash},
									{name: "Credit", value: profitPerYear[0].Credit}
								] : 0,
								profitPerMonth: profitPerMonth.length > 0 ? profitPerMonth[0].totalIncome : 0,
								profitPerWeek: profitPerWeek.length > 0 ? profitPerWeek[0].totalIncome : 0
							})
						}
					})
					
				}
			})
		}
	})
}

exports.incomePerServicePerYear = (req, res) =>{
	var now = new Date();
	// var now = new Date(2018, 11, 2)
	//var today = new Date(now.getFullYear(), now.getMonth(), now.getDate())

	var firstDayOfYear = new Date(now.getFullYear() , 0, 1)
	// var firstDayOfMonth = new Date(now.getFullYear(),now.getMonth(), 1)
	// var firstDayOfMonth = new Date(now.getFullYear()  , 10, 2)

	
	console.log(firstDayOfYear, now)
	invoiceModel.aggregate([{
		$match: { 
			$and: [
				{date: { $gte: firstDayOfYear , $lte: now}}
				// {createdAt: { $gte: firstDayOfYear , $lte: now}},
				// {status: "finished"}
			]
		}
	}, {
		$group: {
			 _id : null,
	        totalIncome: {
	            $sum : "$finalCost"
	        }
	        
		}

	}], (err, result) => {
		console.log(result)
		if(err){
			res.json({
				success: false,
				message: err 
			})
		}
		else{
			var services = ["Winch", "Gasoline","Tires", "Battery"];
			var income = [];
			var service;
			var index = 0;

			services.forEach(async function(service){

			
			// for (var i = 0; i < services.length; i++) {
				
				console.log(service)

				await invoiceModel.find({
					date: { $gte: firstDayOfYear , $lte: now},
					// status: "finished",
					serviceName: service
				}, (er, requests) => {
					if(er){
						res.json({
							success: false,
							message: er
						})
					}
					else{
						incomePerService = 0;
						var inc = [];
						
						if(requests.length == 0){
							income[services.indexOf(service)] = 0
							// income[services.indexOf(service)] = 0
							index++
							console.log("rq", requests)
							if(Object.keys(income).length == services.length){
								console.log(income, "ii", income.length, income[services.indexOf(service)], index-1)
								res.json({
									success: true,
									incomePerYear: result.length == 0 ? 0 : result[0].totalIncome,
									services: services,
									incomePerService: income
								})
							}
						}
						for (var j = 0; j < requests.length; j++){
							console.log('rq', requests[j].serviceName, service)
							calculateIncome(incomePerService, requests[j], service)
							.then( incom => {
								incomePerService = incom;
								// console.log(incomePerService, "in loop")
								inc.push(incomePerService)
								// console.log(j, "j")

								if(inc.length == requests.length){
									// console.log("here", inc)
									var sum = 0;
									for (var l = 0; l < inc.length; l++) {
										sum += inc[l]
									}
									income[services.indexOf(service)] = sum
									// income[services.indexOf(service)] = sum
									index++
									console.log(sum, "ips", index-1, income, income.length, income[services.indexOf(service)])
									if(income.length == services.length && !income.includes(undefined)){
										console.log(income, "ii")
										res.json({
											success: true,
											incomePerYear: result.length == 0 ? 0 : result[0].totalIncome,
											services: services,
											incomePerService: income
										})
									}
									
								}
							})
							
							

						}
						
						
						// income.push(incomePerService)
						
					}

				})
					// invoiceModel.aggregate([{
					// 	$match: { 
					// 		$and: [
					// 			// {createdAt: { $gte: firstDayOfYear , $lte: now}},
					// 			// {status: "finished"},
					// 			{date: { $gte: firstDayOfMonth , $lte: now}},
					// 			{serviceName: services[i]}
					// 		]
					// 	}
					// }, {
					// 	$group: {
					// 		 _id : null,
					//         totalIncome: {
					//             $sum : "$finalCost"
					//         }
					        
					// 	}

					// }], (err, rslt) =>{
					// 	if(err){
					// 		res.json({
					// 			success: false,
					// 			message: err 
					// 		})
					// 	}
					// 	else{
					// 		console.log(rslt)
					// 		if(rslt.length == 0) income.push(0)
					// 		else income.push(rslt[0].totalIncome)

					// 		if(income.length == services.length){
					// 			console.log(income)
					// 			res.json({
					// 				success: true,
					// 				incomePerMonth: result.length == 0 ? 0 : result[0].totalIncome,
					// 				services: services,
					// 				incomePerService: income
					// 			})
					// 		}
					// 	}
					// })
			}) 
		}
	})

	// Request.aggregate({
	// 	createdAt: { $gte: firstDayOfMonth , $lte: now},
	// 	status: "finished"
	// }, {
	// 	$group: {
	// 		 _id : null,
	//         totalIncome: {
	//             $sum : "$finalCost"
	//         }
	        
	// 	}
	// }, (err, result) => {
	// 	console.log(result)
	// })
}

exports.team = (req, res) =>{
	Vendor.find({
		$or: [
			{
				$and: [
					{parentId: { $exists: false }},
					{role: 4}
				]
			},
			{
				role: {
					$in: [3, 6]
				}
			}
			
		]
	}, (err, vendors) => {

		console.log("l", vendors.length)
		if(err){
			res.json({
				success: false,
				message: err
			})
		}
		else{
			var individualDrivers = vendors.filter(vendor => vendor.role == 4 && !vendor.parentId)
			var businessAccounts = vendors.filter(vendor => vendor.role == 3)
			var insuranceCompanies = vendors.filter(vendor => vendor.role == 6)
			res.json({
				success: true,
				individualDrivers: individualDrivers.length,
				businessAccounts: businessAccounts.length,
				insuranceCompanies: insuranceCompanies.length
			})
		}

	})
	// Vendor.count({
	// 	parentId: { $exists: false },
	// 	role: 4
	// }, (err, individualDrivers) => {
	// 	if(err){
	// 		res.json({
	// 			success: false,
	// 			message: err
	// 		})
	// 	}
	// 	else{
	// 		console.log("i",individualDrivers)
	// 		Vendor.count({role: 3}, (er, businessAccounts) => {
	// 			if(er){
	// 				res.json({
	// 					success: false,
	// 					message: er
	// 				})
	// 			}
	// 			else{
	// 				console.log("ba", businessAccounts)
	// 				Vendor.count({role: 6}, (e, insuranceCompanies) => {
	// 					if(e){
	// 						res.json({
	// 							success: false,
	// 							message: e
	// 						})
	// 					}
	// 					else{
	// 						console.log("ic", insuranceCompanies)
	// 						res.json({
	// 							success: true,
	// 							individualDrivers: individualDrivers,
	// 							businessAccounts: businessAccounts,
	// 							insuranceCompanies: insuranceCompanies
	// 						})
	// 					}
	// 				})
	// 			}
	// 		})
	// 	}
	// })
}

// async function handleRequests(requests, temp, incomePerService, service){
// 	let income = {
// 		incomePerService: 0
// 	}
// 	await requests.forEach( async function(request){
// 		console.log(this.incomePerService, "before loop")
// 		// console.log(request, "rr")
// 		this.incomePerService =  await calculateIncome(this.incomePerService, request, service)
// 		console.log(this.incomePerService, "inloop")
// 		// temp = incomePerService
// 		// console.log(temp, "temp")
// 	}, income)
	
// 	console.log(income.incomePerService, "outloop")
// 	return income.incomePerService
// }

async function calculateIncome(incomePerService, request, service){
	// var incomePerService = 0;
	// console.log(incomePerService, "before")
	if(request.serviceName.length == 1){
		incomePerService += request.finalCost

		console.log(incomePerService, "in func")

		return incomePerService;
	}
							
	else{
		// console.log(requests[j].finalCost, "total")
		// console.log(requests[j].serviceName, "  sn")
		// service = services[index]
		console.log(service, "ss")
		// console.log(request, "rr")
		var ser =  await serviceModel.findOne({
			name: service
		}, (errr, servicee) => {
									
			if(errr){
				res.json({
					success: false,
					message: errr
				})
			}
			else{
				
				// console.log(servicee, "ee")
				// console.log(request, "rr")
				
				// return incomePerService;
				// console.log("ips", incomePerService)
			}
		})
		console.log(request.serviceName.length, "ll")
		incomePerService += ( ser.baseFare + ((request.finalCost - request.serviceBaseFare) / (request.serviceName.length)))
		console.log(incomePerService, "in els")
		return incomePerService
	}
	
	
}