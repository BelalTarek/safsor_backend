var express = require('express');
var router = express.Router();
var controller = require('./controllers')
var jwt = require('express-jwt');

router.post('/show-current-connection',jwt({
    secret: process.env.secret
}), controller.getCurrentConnection)

router.post('/show-last-connection', jwt({
    secret: process.env.secret
}), controller.getLastConnection)

router.get('/show-weekly-report', jwt({
    secret: process.env.secret
}), controller.getWeeklyreport)

router.get('/summary-from-beginning', jwt({
    secret: process.env.secret
}), controller.summaryFromBeginning)

router.get('/requests-today', controller.requestsToday)

router.get('/requests-this-month', controller.requestsThisMonth)

router.get('/requests-this-week', controller.requestsThisWeek)

router.get('/percentage-per-service', controller.percentagePerService)

router.get('/income-per-service', controller.incomePerService)

router.get('/running-requests', controller.runningRequests)

router.get('/issues', controller.issues)

router.get('/budget-summary', controller.budgetSummary)

router.get('/income-per-service-per-year', controller.incomePerServicePerYear)

router.get('/team', controller.team)

module.exports = router;