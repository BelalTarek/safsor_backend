var CarSelection = require('./model')
var middlewares = require('../middlewares')
//var Set = require("Set");

exports.showCarManufactory = function(req, res){
	CarSelection.find()
	.select({"carModel": 0, "year": 0, "_id": 0})
	.exec(function(err, carManufactories){
		if(err || carManufactories.length <= 0){
			return res.json({
				success: false,
				message: err || "no car manufactories found"
			})
		}
		else{
			// var manufactoriesJson = carManufactories.map(function(manufactory) {
   //          return manufactory.toJSON();
   //          });
   //          console.log(manufactoriesJson)
			var manufactories = [];
			var copy = {};
			//manufactories = carManufactories.filter(manufactory => !manufactories.includes(manufactory)) //new Set(carManufactories);
			for(var i=0; i<carManufactories.length; i++){

				copy[carManufactories[i]["carManufactory"]] = carManufactories[i];

				// if(!manufactories.includes(carManufactories[i].toJSON())){
				// 	console.log(carManufactories[i].toJSON())
				// 	manufactories.push(carManufactories[i].toJSON())
				// }
				/*if(manufactories.length === 0){
					manufactories.push(carManufactories[i].toJSON());
					//continue;
				}
				var found = false;
				for (var j = 0; j < manufactories.length; j++) {
					if(manufactories[j].carManufactory === carManufactories[i].carManufactory){
						//manufactories.push(carManufactories[i].toJSON());
						found = true;
					}
				}
				if(found == false){
					manufactories.push(carManufactories[i].toJSON());
				}*/
				//console.log(i+' -- '+manufactories.length)
			}
			//console.log(copy)
			for (i in copy) {
				console.log(i)
				manufactories.push(copy[i]) 
			}

			//console.log(manufactories)
			res.json({
				success: true,
				message: manufactories
			})
		}
	})
	
}

exports.showCarModelForManufactory = function(req, res){
	let carData = req.body;
	if(carData.carManufactory){
		CarSelection.find({carManufactory: carData.carManufactory})
		.select({"carManufactory": 0, "year": 0, "_id": 0})
		.exec(function(err, carModels){
			if(err || carModels.length <= 0){
				return res.json({
				success: false,
				message: err || "no cars found for that manufactory"
			    })
			}
			else{

				var models = [];
				var lookUp = {};

				for (var i = 0; i < carModels.length; i++) {
					lookUp[carModels[i]["carModel"]] = carModels[i]
				}
				for(i in lookUp){
					models.push(lookUp[i])
				}
				res.json({
					success: true,
					cars: models
				})
			}
		})
	}
	else{
		return res.json(middlewares.checkAllFieldsExist)
	}
}

exports.showAllCars = function(req, res){
	CarSelection.find(function(err, cars){
		if(err || cars.length <=0 ){
			return res.json({
				success: false,
				message: err || "no cars found"
			})
		}
		else{
			res.json({
				success: true,
				cars: cars
			})
		}
	})
}

exports.showYearsForCarModel = function(req, res){

	let carData = req.body;
	if(carData.carModel){

		CarSelection.find({carModel: carData.carModel})
		.select({"carModel": 0, "carManufactory": 0, "_id": 0})
		.exec(function(err, years){
			if(err || years.length <= 0){
				return res.json({
					success: false,
					message: err
				})
			}
			else{
				res.json({
					success: true,
					message: years
				})
			}
		})

	}
	else{
		return res.json(middlewares.checkAllFieldsExist)
	}
}