const mongoose = require('mongoose')
const bcryptjs = require('bcryptjs')
const carSelectionSchema = mongoose.Schema({
	carModel: {
		type: String
	},
	carManufactory: {
		type: String
	},
	year: {
		type: Number
	}
})
var CarSelection = module.exports = mongoose.model('carSelection', carSelectionSchema)