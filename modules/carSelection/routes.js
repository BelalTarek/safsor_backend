var express = require('express');
var router = express.Router();
var controller = require('./controllers')

/**
* @api {get} /car-selection/show-car-manufactories show all car manufactories
* @apiName ShowCarManufactories
* @apiGroup CarSelection
* @apiSuccessExample Success-Response:
* {
*	"success": true,
* 	"message": manufactories
* }
* @apiErrorExample Error-Response:
* {
*	"success": false,
* 	"message": "no car manufactories found"
* }
*/
router.get('/show-car-manufactories', controller.showCarManufactory)

/**
* @api {post} /car-selection/show-car-model-for-manufactory show all car models for specific manufactory
* @apiName ShowCarModelForManufactory
* @apiGroup CarSelection
* @apiParam {String} carManufactory car manufactory
* @apiSuccessExample Success-Response:
* {
*	"success": true,
* 	"cars": models
* }
* @apiErrorExample Error-Response:
* {
*	"success": false,
* 	"message": "no car models found for that manufactory"
* }
*/
router.post('/show-car-model-for-manufactory', controller.showCarModelForManufactory)

/**
* @api {get} /car-selection/show-all-cars show all cars
* @apiName ShowAllCars
* @apiGroup CarSelection
* @apiSuccessExample Success-Response:
* {
*	"success": true,
* 	"cars": cars
* }
* @apiErrorExample Error-Response:
* {
*	"success": false,
* 	"message": "no cars found"
* }
*/
router.get('/show-all-cars', controller.showAllCars)

/**
* @api {post} /car-selection/show-years-for-car-model show all years for specific model
* @apiName ShowYearsForCarModel
* @apiGroup CarSelection
* @apiParam {String} carModel car model
* @apiSuccessExample Success-Response:
* {
*	"success": true,
* 	"message": years
* }
* @apiErrorExample Error-Response:
* {
*	"success": false,
* 	"message": err
* }
*/
router.post('/show-years-for-car-model', controller.showYearsForCarModel)

module.exports = router;