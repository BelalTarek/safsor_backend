const express = require('express');
const router = express.Router();
const Service = require('./model');
let middlewares = require('../middlewares')
var tripModel = require('../trip/settingsModel')

//add a service
exports.addService = function (req, res, next) {
  let serviceData = req.body;
  if (serviceData.name) {
    let newService = new Service({
      name: req.body.name,
      type: req.body.type,
      description: req.body.description,
      baseFare: req.body.baseFare
    })
    newService.save((err, newService) => {
      if (err) {
        res.json({
          success: false,
          message: 'failed to add'
        })
      } else {
        res.json({
          success: true,
          newService: newService
        })
      }
    })
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
}

//list services
exports.listServices = function (req, res, next) {
  Service.find({}, function (err, result) {
    console.log(result)
    if (result[0] == null) {
      res.json({
        success: false,
        message: 'No Service For You'
      })
    } else {
      res.json({
        success: true,
        services: result
      })
    }
  });
}


exports.deactivateService = function (req, res, next) {
  let serviceData = req.body;
  if(serviceData){
    Service.findOneAndUpdate({
            _id: serviceData.serviceId,
          }, {
            $set: {
              active: false,
            }
          }, {
            new: true
          }, function (err, doc) {
            if (!doc) {
              res.json({
                success: false,
                message: err
              })
            }else {
      res.json({
        success: true,
        services: doc
      })
    }
        })
  }else {
    res.json(middlewares.checkAllFieldsExist)
  }
}

exports.activateService = function (req, res, next) {
  let serviceData = req.body;
  if(serviceData){
    Service.findOneAndUpdate({
            _id: serviceData.serviceId,
          }, {
            $set: {
              active: true,
            }
          }, {
            new: true
          }, function (err, doc) {
            if (!doc) {
              res.json({
                success: false,
                message: err
              })
            }else {
      res.json({
        success: true,
        services: doc
      })
    }
        })
  }else {
    res.json(middlewares.checkAllFieldsExist)
  }
}

exports.addBaseFare = function (req, res, next) {
  let serviceData = req.body;
  if(serviceData.serviceId && serviceData.baseFare){
    Service.findOneAndUpdate({
            _id: serviceData.serviceId,
          }, {
            $set: {
              baseFare: serviceData.baseFare,
            }
          }, {
            new: true
          }, function (err, doc) {
            if (!doc) {
              res.json({
                success: false,
                message: err
              })
            }else {
      res.json({
        success: true,
        servicesBaseFare: doc.baseFare
      })
    }
        })
  }else {
    res.json(middlewares.checkAllFieldsExist)
  }
}
exports.getAppRates = function(req, res){
  // var reqBody = req.body;
  // if(reqBody.serviceId){
    Service.find({active: true}, function(err, services){
      if(err || services.length <= 0){
        return res.json({
          success: false,
          message: err || "service not found"
        })
      }
      else{
        tripModel.find(function(err, settings){
          if(err || !settings){
            return res.json({
              success: false,
              message: "could found trip settings",
              rates: services
            })
          }
          else{
            res.json({
              success: true,
              rates:{
                services: services,
                waitingTimePrice: settings[0].waitingTimePrice,
                kilometerPrice: ''
              }
            })
          }
        })
      }
    })
  // }
  // else{
  //   return res.json(middlewares.checkAllFieldsExist)
  // }
}