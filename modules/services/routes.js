var express = require('express');
var router = express.Router();
//require controllers
var serviceController = require('./controllers');
var middlewares = require('../middlewares');


/**
 * @api {post} /services/add-service add a new service
 * @apiName add a services
 * @apiGroup services
 *
 * @apiParam {string} name of the new service
 * @apiParam {string} type of the new service
 * @apiParam {string} description of the new service
 * @apiParam {string} baseFare of the new service
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *      newServiceName: 'newServiceName'
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *        message: 'failed to add'
 *   }
 */

router.post('/add-service', serviceController.addService);
/**
 * @api {get} /services/list-all list services
 * @apiName list services
 * @apiGroup services
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
        message: 'No Service For You'
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
        services: result
 *   }
 */

router.get('/list-all', serviceController.listServices);

/**
 * @api {post} /services/deactivate-service deactivate services
 * @apiName deactivate services
 * @apiGroup services
 * @apiParam  {string} serviceId service Id that you want to deactivat .
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
        message: 'deactivate successfully'
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
        services: 'err'
 *   }
 */
router.post('/deactivate-service', serviceController.deactivateService);

/**
 * @api {post} /services/activate-service activate services
 * @apiName activate services
 * @apiGroup services
 * @apiParam  {string} serviceId service Id that you want to deactivat .
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
        message: 'activate successfully'
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
        services: 'err'
 *   }
 */
router.post('/activate-service', serviceController.activateService);

/**
 * @api {post} /services/add-base-fare add base fare for services
 * @apiName add base fare 
 * @apiGroup services
 * @apiParam  {string} serviceId service Id that you want to deactivat .
 * @apiParam  {number} baseFare baseFare for service .
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
        message: 'activate successfully'
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
        services: 'err'
 *   }
 */
router.post('/add-base-fare', serviceController.addBaseFare);

/**
 * @api {post} /services/show-app-rate add show app rate for specific service
 * @apiName showAppRate
 * @apiGroup services
 * @apiParam  {string} serviceId service Id that you want to show its app rates.
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *      rates:{
 *               //serviceId: service._id,
 *               serviceName: service.name,
 *              //serviceType: service.type,
 *               baseFare: service.baseFare,
 *               waitingTimePrice: settings[0].waitingTimePrice,
 *               kilometerPrice: ''
 *             }
 *           })
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 		 message: "Service not found"
 *   }
 */
router.get('/show-app-rate', serviceController.getAppRates);
module.exports = router;