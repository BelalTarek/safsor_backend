const mongoose = require('mongoose')
const config = require('config')

const ServiceSchema = mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    type: {
        type: String,
        require: false
    },
    description: {
        type: String,
        require: true
    },
    active: {
        type: Boolean,
        require: false,
        default: true
    },
    baseFare: {
        type: Number,
        require: true
    }
})

const Service = module.exports = mongoose.model('Service', ServiceSchema)