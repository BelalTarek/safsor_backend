var express = require('express');
var router = express.Router();
//require controllers
var middlewares = require('../middlewares');
var carController = require('./controllers');
var jwt = require('express-jwt');


//"/cars/assign-driver"

router.post('/assign-driver', jwt({
    secret: process.env.secret
}), carController.assignDriverToCar);

//"/cars/add-cars"

router.post('/add-cars', jwt({
    secret: process.env.secret
}), carController.addBulkCars);

router.post('/add-user-cars', jwt({
    secret: process.env.secret
}), carController.addUserCars);
//"/cars/list-cars"

router.get('/list-cars', jwt({
    secret: process.env.secret
}), carController.getCars);

//"/cars/get-my-car"
router.get('/get-my-car', jwt({
    secret: process.env.secret
}), carController.getMyCar);
//"/cars/delete-my-car"
router.get('/delete-my-car', jwt({
    secret: process.env.secret
}), carController.deleteMyCar);

router.post('/add-car-for-ic', jwt({
    secret: process.env.secret
}), carController.addCarForIC);

router.get('/get-cars-for-ic', jwt({
    secret: process.env.secret
}), carController.getCarsForIC);

router.post('/edit-car-for-ic', jwt({
    secret: process.env.secret
}), carController.editCarForIC)

router.post('/delete-car-for-ic', jwt({
    secret: process.env.secret
}), carController.deleteCarForIC)

module.exports = router;