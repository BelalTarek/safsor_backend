const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
var request = require('request');
var rn = require('random-number');
const car = require('./model');
const bcryptjs = require('bcryptjs');
const vendor = require('../auth/vendor/vendorModel')
var Customer = require('../auth/customer/customerModel')
var PromoCode = require('../promocodes/model')
var middlewares = require('../middlewares')

exports.addUserCars = function (req, res, next) {
  let userData = req.body;
  let userInfo = req.user;
  var array = [];
  // query users collection to check if phonenumbers already registered
  let findExistCars = (carsId) => {
    car.find({
      carNumber: {
        $in: carsId
      }
    }, (err, foundCars) => {
      if (err) {
        // console.log(err);
        res.json({
          success: false,
          message: err
        })
      } else {
        // console.log(cars)
        saveBulk(foundCars, carsId);
      }
    });
  }
  // return phone numbers if found any registered or save bulk of users 
  let saveBulk = (carsFound, carsId) => {
    for (var i = 0; i < userData.length; i++) {
      // if there is no users found already registered, hash passwords
      if (carsFound.length < 1) {
        if (i === (userData.length)) {
          break;
        } else {
          
            userData[i].driverId = userInfo._doc._id
        }
      }
    }
    if (carsFound.length < 1) {
      car.insertMany(userData, function (err, cars) {
        if (err) {

          res.json({
            success: false,
            message: err
          })
        } else {
         res.json({
            success: true,
            message: "car added successfully"
          })
        }
      });
    } else {
      res.json({
        success: false,
        message: "following carNumbers is already exist",
      });
    }
  }
  // array has objects or empty?
  if (userData.length > 0 || !userData) {
    var carsId = [];
    // loop on the bulk of users to be added
    for (var i = 0; i < userData.length; i++) {
      // check all attr. are there 
      if (userData[i].carNumber && userData[i].carColor && userData[i].carModel && userData[i].carManufactory) {
        carsId.push(userData[i].carNumber)
        // console.log(carsId)
        if (i === (userData.length)) {
          break;
        }
findExistCars(carsId);
      } else {
        res.json({
          success: false,
          message: 'all attributes are needed'
        })
      };

    }
    
  } else {
    res.json({
      success: true,
      message: 'it must be an array with at least one object'
    })
  }
};

exports.addBulkCars = function (req, res, next) {
  let userData = req.body;
  let userInfo = req.user;
  var array = [];
  // query users collection to check if phonenumbers already registered
  let findExistCars = (carsId) => {
    car.find({
      carNumber: {
        $in: carsId
      }
    }, (err, foundCars) => {
      if (err) {
        // console.log(err);
        res.json({
          success: false,
          message: err
        })
      } else {
        // console.log(cars)
        saveBulk(foundCars, carsId);
      }
    });
  }
  // return phone numbers if found any registered or save bulk of users 
  let saveBulk = (carsFound, carsId) => {
    for (var i = 0; i < userData.length; i++) {
      // if there is no users found already registered, hash passwords
      if (carsFound.length < 1) {
        if (i === (userData.length)) {
          break;
        } else {
          if(userData[i].role === 3 || userData[i].role === 5){
            userData[i].parentId = userData[i].vendorId;
          }else {
            userData[i].driverId = userData[i].vendorId
          }
          
         
        }
      }
    }
    if (carsFound.length < 1) {
      car.insertMany(userData, function (err, cars) {
        if (err) {

          res.json({
            success: false,
            message: err
          })
        } else {
          vendor.findOne({
              _id: userData[0].vendorId
            },
            (err, user) => {
              if (err || !user) {
                res.json({
                  success: false,
                  message: err || "no parent for this car"
                })
              } else {
                for (var i = 0; i < cars.length; i++) {
                  array = cars[i]._id.toString();
                }
                //user.cars.push(array)
                vendor.findByIdAndUpdate(userInfo._doc._id, {
                    $push: {
                      cars: array
                    }
                  }, {
                    safe: true,
                    upsert: true
                  },
                  function (err, doc) {
                    if (err) {
                      console.log(err);
                    } else {
                      res.json({
                        success: true,
                        users: cars
                      })
                    }
                  });
                // user.save((err, user2) => {
                //   if (err || !user2) {
                //     res.json({
                //       success: false,
                //       msg: err || "issue"
                //     })
                //   } else {
                //     res.json({
                //       success: true,
                //       cars: cars
                //     })
                //   }
                // })
              }
            })
        }
      });
    } else {
      res.json({
        success: false,
        message: "following carNumbers is already exist ",
      });
    }
  }
  // array has objects or empty?
  if (userData.length > 0 || !userData) {
    var carsId = [];
    // loop on the bulk of users to be added
    for (var i = 0; i < userData.length; i++) {
      // check all attr. are there 
      if (userData[i].carNumber && userData[i].carColor && userData[i].carModel && userData[i].carManufactory && userData[i].vendorId ) {
        carsId.push(userData[i].carNumber)
        // console.log(carsId)
        if (i === (userData.length)) {
          break;
        }
findExistCars(carsId);
      } else {
        res.json({
          success: false,
          message: 'all attributes are needed'
        })
      };

    }
    
  } else {
    res.json({
      success: true,
      message: 'it must be an array with at least one object'
    })
  }
};


exports.assignDriverToCar = function (req, res, next) {
  let userData = req.body;
  let userInfo = req.user;
  if (userData.driverId && userData.carId) {
    car.findOne({
      _id: userData.carId,
      //parentId: userInfo._doc._id
    }, (err, car) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        if (car.isAvailable == false) {
          res.json({
            success: false,
            message: "this car in not available now"
          })
        } else {
          vendor.findOne({
            _id: userData.driverId,
            //parentId: userInfo._doc._id
          }, (err, driver) => {
            if (err) {
              res.json({
                success: false,
                message: err
              })
            } else {
              if (driver.isAvailable == false) {
                res.json({
                  success: false,
                  message: "this car in not available now"
                })
              } else {
                car.driverId = userData.driverId;
                car.isAvailable == false;
                car.save((err, car2) => {
                  if (err) {
                    res.json({
                      success: false,
                      message: err
                    })
                  } else {
                    res.json({
                      success: true,
                      message: 'assigned successfuly'
                    })
                  }
                })
              }
            }
          })
        }
      }
    })
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
}

exports.getCars = function (req, res, next) {
  car.find({
   parentId: req.user._doc._id,
   isAvailable: true
  }, (err, cars) => {
    if (err || !cars) {
      res.json({
        success: false,
        message: err
      })
    } else {
      // console.log(driver)
      res.json({
        success: true,
        cars: cars
      })

    }
  });
}

exports.getMyCar = function (req, res, next) {
  car.findOne({
   driverId: req.user._doc._id,
   //isAvailable: true
  }, (err, car) => {
    if (err || !car) {
      res.json({
        success: false,
        message: err
      })
    } else {
      // console.log(driver)
      res.json({
        success: true,
        cars: car
      })

    }
  });
}

exports.deleteMyCar = function (req, res, next) {
  car.deleteOne({
   driverId: req.user._doc._id,
   //isAvailable: true
  }, (err, car) => {
    if (err || !car) {
      res.json({
        success: false,
        message: err || "no cars found"
      })
    } else {
      // console.log(driver)
      res.json({
        success: true,
        cars: car
      })

    }
  });
}

exports.addCarForIC = function(req, res , next){
  var carData = req.body;

  vendor.findById(req.user._doc._id, (ee, insuranceComp) => {
    if(ee || !insuranceComp){
      return res.json({
                success: false,
                message: ee || "insurance company not found"
      })
    }
    else{
      if(insuranceComp.numOfUsers == 0){
        res.json({
          success: false,
          message: "You're not allowed to add more users"
        })
      }
      else{
        if(carData.carNumber && carData.carModel && carData.carManufactory && carData.carColor 
          && carData.username && carData.name && carData.email && carData.phoneNumber && carData.password && carData.promoCode){
          Customer.findOne({
            $or: [
              {username: carData.username},
              {phoneNumber: carData.phoneNumber},
              {password: carData.password}
            ]
          }, (err, user) => {
            if(err){
              return res.json({
                success: false,
                message: err
              })
            }
            else if(!user){
              var customer = new Customer({
                name: carData.name,
                email: carData.email,
                phoneNumber: carData.phoneNumber,
                username: carData.username,
                password: carData.password,
                insuranceCompanyId: req.user._doc._id

              })
              Customer.addUser(customer, (er, result) => {
                if(er || !result){
                  return res.json({
                    success: false,
                    message: er || "Failed to add customer"
                  })
                }
                else{
                  PromoCode.findOneAndUpdate({code: carData.promoCode},
                  {
                    $push: {userIds: result._id}
                  },{new: true}, (errr, code) => {
                    if(errr || !code){
                      return res.json({
                        success: false,
                        message: errr || "Failed to update promo code"
                      })
                    }
                    else{
                      car.findOne({carNumber: carData.carNumber}, (e, carFound) => {
                        if(e || carFound){
                          return res.json({
                            success: false,
                            message: e || "this carNumber already exists"
                          })
                        }
                        else{
                          var newCar = new car({
                            carNumber: carData.carNumber,
                            carColor: carData.carColor,
                            driverId: result._id,
                            parentId: req.user._doc._id,
                            carModel: carData.carModel,
                            carManufactory: carData.carManufactory
                          })

                          newCar.save( (eror, rslt) => {
                            if(eror || !rslt){
                              return res.json({
                                success: false,
                                message: eror || "Failed to add car"
                              })
                            }
                            else{
                              insuranceComp.numOfUsers = insuranceComp.numOfUsers - 1
                              insuranceComp.cars = insuranceComp.cars.concat([rslt._id])
                                insuranceComp.save( (eee, rs) => {
                                  if(eee || !rs){
                                    return res.json({
                                      success: false,
                                      message: eee || "Failed to update insurance company"
                                    })
                                  }
                                  else{
                                    res.json({
                                      success: true,
                                      message: "car and customer are added successfully"
                                    })
                                  }
                                })
                              // vendor.findById(req.user._doc._id, (ee, insuranceComp) => {
                              //   if(ee || !insuranceComp){
                              //     return res.json({
                              //       success: false,
                              //       message: ee || "insurance company not found"
                              //     })
                              //   }
                              //   else{
                                  
                              //   }
                              // })
                              
                            }
                          })
                        }
                      })
                    }
                  })
                  
                }
              })
            }
            else{
              if (user.username === carData.username) {
                res.json({
                  success: false,
                  message: "username already registered"
                })
              } else if(user.phoneNumber === carData.phoneNumber) {
                res.json({
                  success: false,
                  message: "phone number already registered"
                })
              }
              else if(user.email === carData.email) {
                res.json({
                  success: false,
                  message: "email already registered"
                })
              }
            }
          })
    
  }
  else{
    return res.json(middlewares.checkAllFieldsExist)
  }
      }
    }
  })

  

}

exports.getCarsForIC = function(req, res){
  if(req.user){
    Customer.find({insuranceCompanyId: req.user._doc._id}, function(err, users){
      if(err || !users){
        return res.json({
          success: false,
          message: err || "no users for this insurance company"
        })
      }
      else{
        car.find({parentId: req.user._doc._id}, (er, cars) => {
          if(er || !cars){
            return res.json({
              success: false,
              message: er || "no cars"
            })
          }
          else{
            var result = [];
            var usersJSON = JSON.parse(JSON.stringify(users))

            for (var i = 0; i < users.length; i++) {
              //userIds.push(users[i]._) 
              var car = cars.filter( car => car.driverId==users[i]._id)
              for (const prop in car[0]._doc) {
                if (car[0]._doc.hasOwnProperty(prop)) {
                  console.log(`-- ${prop}: ${car[0]._doc[prop]}`)
                  //console.log(i, usersJSON[i].email)
                  if(prop !== "_id"){
                    usersJSON[i][prop] = car[0]._doc[prop];
                  }

                }
              }
              var element = {
                user: users[i],
                car: car[0]
              }
              result.push(element)
            }
          }
          res.json({
            success: true,
            users: usersJSON
          })
        })
      }
    })
  }
  else{
    return res.json({
      success: false,
      message: "Authorization header is missing"
    })
  }
}

exports.editCarForIC = function(req, res){
  var reqBody = req.body;

  if(reqBody.userId){
    Customer.findById(reqBody.userId, function(err, user){
      if(err || !user){
        return res.json({
          success: false,
          message: err || "user not found"
        })
      }
      else{
        car.findOne({driverId: user._id, parentId:req.user._doc._id}, function(er, carFound){
          if(er || !carFound){
            return res.json({
              success: false,
              message: er || "car data is not found"
            })
          }
          else{
            if(reqBody.carNumber){
              carFound.carNumber = reqBody.carNumber
            }
            if(reqBody.carColor){
              carFound.carColor = reqBody.carColor
            }
            if(reqBody.carModel){
              carFound.carModel = reqBody.carModel
            }
            if(reqBody.carManufactory){
              carFound.carManufactory = reqBody.carManufactory
            }
            if(reqBody.name){
              user.name = reqBody.name
            }
            if(reqBody.username){
              user.username = reqBody.username
            }
            if(reqBody.email){
              user.email = reqBody.email
            }
            if(reqBody.phoneNumber){
              user.phoneNumber = reqBody.phoneNumber
            }
            if(reqBody.password){
              var hashedPass = bcryptjs.hash(reqBody.password, bcryptjs.genSalt(10))
              user.password = hashedPass
            }
            carFound.save((errr, result) => {
              if(errr || !result){
                res.json({
                  success: false,
                  message: errr || "failed to update car"
                })
              }
              else{
                user.save( (e, rslt) => {
                  if(e || !rslt){
                    res.json({
                      success: false,
                      message: e || "failed to update user"
                    })
                  }
                  else{
                    res.json({
                      success: true,
                      message: "Info updated successfully"
                    })
                  }
                })
              }
            })
          }
        })
      }
    })
  }
  else{
    res.json(middlewares.checkAllFieldsExist)
  }
}

exports.deleteCarForIC = function(req, res){
  var reqBody = req.body;

  if(reqBody.userId){
    Customer.findById(reqBody.userId, (err, user) => {
      if(err || !user){
        res.json({
          success: false,
          message: err || "user not found"
        })
      }
      else{
        car.findOne({driverId: user._id}, (errr, carFound) => {
          if(errr || !carFound){
            res.json({
              success: false,
              message: errr || "car not found"
            })
          }
          else{
            vendor.findById(user.insuranceCompanyId, (ee, vendorFound) => {
              if(ee || !vendorFound){
                res.json({
                  success: false,
                  message: ee || "vendor not found"
                })
              }
              else{
                var carIndex = vendorFound.cars.indexOf(carFound._id)
                vendorFound.cars.splice(carIndex,1)
                vendorFound.save( (eee, rs) => {
                  if(eee || !rs){
                    res.json({
                      success: false,
                      message: eee || "failed to"
                    })
                  }
                  else{
                    Customer.deleteOne({_id: reqBody.userId}, (er, result) => {
                      if(er || !result){
                        res.json({
                          success: false,
                          message: er || "Failed to delete user"
                        })
                      }
                      else{
                        car.deleteOne({_id: carFound._id}, (e, rslt) => {
                          if(e || !rslt){
                            res.json({
                              success: false,
                              message: e || "Failed to delete car"
                            })
                          }
                          else{
                            res.json({
                              success: true,
                              message: "User deleted successfully"
                            })
                          }
                        })
                        
                      }
                    })
                  }
                })
                
              }
            })
            
            
          }
        })
        //if(req.user && user.insuranceCompanyId == req.user._doc._id)
        
      }
    })
  }
  else{
    res.json(middlewares.checkAllFieldsExist)
  }
}
