const mongoose = require('mongoose')
const bcryptjs = require('bcryptjs')
const carSchema = mongoose.Schema({
    carNumber: {
        type: String
    },
    carColor: {
        type: String
    },
    isAvailable: {
        type: Boolean,
        default: true
    },
    driverId: {
        type: String,
    },
    parentId: {
        type:String,
    },
    carModel: {
        type: String
    },
    carManufactory: {
        type: String
    },
})

const car = module.exports = mongoose.model('car', carSchema)