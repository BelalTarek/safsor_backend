var valid = require('card-validator');
let invoice = require('./model');
const decodeJWT = require('decoding_jwt');
let middlewares = require('../middlewares');
const vendor = require('../auth/vendor/vendorModel')
var schedule = require('node-schedule');


module.exports.getAllInvoices = function(req, res, next) {
	invoice.find({}, (err, invoices) => {
    if(err || !invoices){
      res.json({
          success: false,
          message: err || "No invoices"
        })
    }else{
      res.json({
          success: true,
          message: invoices
        })
    }
  })
}

module.exports.getAllVendorInvoices = function(req, res, next) {
  vendor.findOne({
    _id: req.user._doc._id
  }, (err, foundVendor) => {
    if (err) {
      res.json({
        success: false,
        message: err
      })
    } else {
      invoice.find({
       driverId: {
        $in: foundVendor.drivers
      }
      }, (err, invoices) => {
    if(err || !invoices){
      res.json({
          success: false,
          message: err || "No invoices"
        })
    }else{
      res.json({
          success: true,
          message: invoices
        })
    }
  })
    }
  });
 
}

var rule = new schedule.RecurrenceRule();
rule.dayOfWeek = 0
rule.hour = 24;
rule.minute = 1;

var weekly_setOff = schedule.scheduleJob(rule, function(){
   var i;
   vendor.find({} , (err , vendors) => {
    if(err){
      return err ;
    }else{
        for(i = 0 ; i < vendors.length ; i++){
        if(vendors[i].isSetOf === false){
         vendors[i].cashRequest = false
         vendors[i].save((err, done) => {
          if (done) {
            
            return "true";
           
          }else{
          vendors[i].isSetOf = false
          return "false";
          }
        })
        }
        }

    }
   })

});

module.exports.setOff = function(req, res, next) {
  var day = new Date()
let userData = req.user;
var cash = 0;
var credit = 0;
var yourCredit ;
  vendor.findOne({
          _id:userData._doc._id
        }, (err, vendor) => {
           if (err || !vendor) {
        res.json({
          success: false,
          message: err
        })
      } else {
        invoice.find({
      driverId: userData._doc._id,
    }, (err, invoices) => {
      if (err || !invoices) {
        res.json({
          success: false,
          message: err
        })
      }else{
       //console.log(vendor)
        invoices.forEach(function(element) {
        if(element.date.getDay() < 6 && Math.abs((day.getDate() - element.date.getDate())) <= 7 && element.date.getMonth() === day.getMonth() && element.date.getYear() === day.getYear())
        if(element.paymentType === "Cash" || element.paymentType === "cash"){
          cash += Number(element.finalCost) - element.enqazPercentage
        }else{
          credit += element.enqazPercentage - Number(element.finalCost)
        }
       });
        
        yourCredit = credit - cash  ;

        res.json({
          success: true,
          yourCredit:yourCredit
        })
      }
        })
        
      }
    });
}


module.exports.adminSetOff = function(req, res, next) {
var day = new Date()
let userData = req.user;

vendor.findOne({
  _id: req.body.vendorId
}, (err , vendor) => {
  if(err || !vendor){
    res.json({
      success: false,
      message: err || "no vendor with this Id"
    })
  }else{
    if(vendor.isSetOf === true){

      vendor.isSetOf = false
      vendor.cashRequest = false
      res.json({
      success: true,
      message: vendor.isSetOf
    })

    }else if(vendor.isSetOf === false && day.getDay() === 0){

             vendor.isSetOf = true
              res.json({
      success: true,
      message: vendor.isSetOf
    })
    }else{

            vendor.isSetOf = true
            vendor.cashRequest = true
                 res.json({
      success: true,
      message: vendor.isSetOf
    })
    }
  }
})
}