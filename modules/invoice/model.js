const mongoose = require('mongoose')
const config = require('config')

const invoiceSchema = mongoose.Schema({
    requestId: {
        type: String,
        required: true
    },
    customerId: {
        type: String,
        required: true
    },
    customerName: {
        type: String,
        required: true
    },
    driverId: {
        type: String,
        required: true
    },
    driverName: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    finalCost: {
        type: Number,
        required: true
    },
    finalCostBeforeDiscount: {
        type: Number
    },
    promoCodeDiscount: {
        type: Number
    },
    dist: {
        type: String,
        required: true
    },
    paymentType: {
        type: String,
        required: true
    },
    kmPrice: {
        type: Number,
        // required: true
    },
     carModel: {
        type: String,
        required: true
    },
     carManufactory: {
        type: String,
        required: true
    },
    serviceBaseFare: {
        type: String,
        required: true
    },
    serviceName:[{
        type: String,
        required: true
    }],
    waitingTime: {
        type: Number,
    },
    waitingTimePrice:{
        type: Number,
    },
    enqazPercentage: {
        type: String,
        required: true
    },
});

module.exports = mongoose.model('invoice', invoiceSchema)