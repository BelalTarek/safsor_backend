var express = require('express');
var router = express.Router();
//require controllers
var invoiceController = require('./controllers');
var middlewares = require('../middlewares');
var jwt = require('express-jwt');

// "/invoice/get-invoices"


router.get('/get-invoices',jwt({
    secret: process.env.secret
}), invoiceController.getAllInvoices);

// "/invoice/get-vendor-invoices"

router.get('/get-vendor-invoices',jwt({
    secret: process.env.secret
}), invoiceController.getAllVendorInvoices);

router.get('/setof',jwt({
    secret: process.env.secret
}), invoiceController.setOff);

//"invoice/admin-setoff"
router.post('/admin-setoff',jwt({
    secret: process.env.secret
}), invoiceController.adminSetOff);


module.exports = router;


