var valid = require('card-validator');
let refCodeModel = require('./model');
const decodeJWT = require('decoding_jwt');
let middlewares = require('../middlewares');
var rn = require('random-number');

module.exports.getMyRefCode = function (req, res, next) {
  let reqBody = req.body;
  refCodeModel.findOne({
    userId: req.user._doc._id
  }, (err, codeFound) => {
    console.log("err, codeFound",err, codeFound);

    if (codeFound) {
      res.json({
        success: true,
        data: codeFound
      })
    } else {
      findRefCode() // Returns a Promise!
        .then(codeToSave => {
          refCodeModel.create({
              userId: req.user._doc._id,
              refCode: codeToSave,
            },
            (err, data) => {
              if (!data) {
                res.json({
                  success: false,
                  message: err
                })
              } else {
                res.json({
                  success: true,
                  data: data
                });
              }
            });
        })
        .catch(err => {
          res.json({
            success: false,
            message: err
          })
        })
    }
  });
};
let findRefCode = function (callback) {
  return new Promise((resolve, reject) => {
    function generateFunction() {
      var gen = rn.generator({
        min: -900000,
        max: 900000,
        integer: true
      });
      return generatedNum = gen(100000);
    }
    let findQuery = {
      refCode: generateFunction(),
    };
    refCodeModel.findOne(findQuery, (err, result) => {
      if (err) {
        return reject(err)
      } else if (result) {
        findRefCode(callback);
      } else {
        return resolve(generatedNum)
      }
    })
  })
}