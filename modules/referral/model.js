const mongoose = require('mongoose')
const config = require('config')

const ReferralSchema = mongoose.Schema({
    userId: {
        type: String,
        require: false
    },
    refCode: {
        type: String,
        require: false
    },
    expiresAt:{
        type:Date
    }

})

const Referral = module.exports = mongoose.model('Referral', ReferralSchema)