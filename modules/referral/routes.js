var express = require('express');
var router = express.Router();
//require controllers
var jwt = require('express-jwt');

var controllers = require('./controllers');
var middlewares = require('../middlewares');
router.use(jwt({
    secret: process.env.secret
}), function (req, res, next) {
    if (req.user) {
        console.log(req.user._doc.role);
        if (req.user._doc.role != 5) return res.json({
            sucess: false,
            message: "you are not a customer"
        });
        next();
    } else {
        next();
    }
});

//routers
/**
 * @api {get} /referral/get-my-ref-code create new referral code or get it 
 * @apiName create new ref code
 * @apiGroup refCode
 * 
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *     data: {}
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *        message: errMsg
 *   }
 */

router.get('/get-my-ref-code', controllers.getMyRefCode);
module.exports = router;