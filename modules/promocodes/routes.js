var express = require('express');
var router = express.Router();
//require controllers
var promoCodesController = require('./controllers');
var middlewares = require('../middlewares');
var jwt = require('express-jwt');

//Routers
/**
 * @api {post} /promo-codes/add-promo add new promocode
 * @apiName addPromoCode
 * @apiGroup promocodes
 * @apiParam {Date} startDate start date of promocode
 * @apiParam {Date} expiresAt expiry date of promocode
 * @apiParam  {number} discountRate rate of the discount
 * @apiParam  {string} code code user will write
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *   }
 */
router.post('/add-promo', jwt({
    secret: process.env.secret
}), promoCodesController.addCode);
/**
 * @api {get} /promo-codes/list-my-promos get all user cards
 * @apiName listMyPromoCodes
 * @apiGroup promocodes
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *     list: list of promos
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *   }
 */
router.get('/list-my-promos', jwt({
    secret: process.env.secret
}), promoCodesController.getMyPromosList);

//promo-codes/activate-promo-code

router.post('/activate-promo-code', jwt({
    secret: process.env.secret
}), promoCodesController.activatePromoCode);

module.exports = router;