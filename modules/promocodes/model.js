const mongoose = require('mongoose')
const config = require('config')

const promoCodesSchema = mongoose.Schema({
    userIds: [{
        type: String,
    }],
    code: {
        type: String
    },
    startDate: {
        type: Date
    },
    expiresAt: {
        type: Date
    },
    discountRate: {
        type: Number
    }
});
module.exports = mongoose.model('promoCodes', promoCodesSchema)