var valid = require('card-validator');
let promoCodes = require('./model');
const decodeJWT = require('decoding_jwt');
let middlewares = require('../middlewares')
var Customer = require('../auth/customer/customerModel')
const ReferralModel = require('../referral/model');
const RequestService = require('../requests/model');

var AWS = require('aws-sdk');

AWS.config.region = 'us-east-1';
AWS.config.update({
      accessKeyId: process.env.AWS_Access_Key_ID,
      secretAccessKey: process.env.AWS_Secret_Access_Key,
});
var sns = new AWS.SNS();

module.exports.addCode = function (req, res, next) {
    let reqBody = req.body;
    if (reqBody.code && reqBody.expiresAt && reqBody.discountRate && reqBody.userIds && reqBody.startDate) {
        let findQuery = {
            code: reqBody.code,
        };
        promoCodes.findOne(findQuery, (err, code) => {
            if (code) {
                res.json({
                    success: false,
                    message: "promo-code already added"
                })
            } else {
                promoCodes.create({
                        //userId: reqBody.userId,
                        code: reqBody.code,
                        expiresAt: reqBody.expiresAt,
                        startDate: reqBody.startDate,
                        discountRate: reqBody.discountRate
                    },
                    (err, code) => {
                        if (!code) {
                            res.json({
                                success: false,
                                message: err
                            })
                        } else {

                            Customer.find({
                                _id: {
                                    $in: reqBody.userIds
                                }
                            }, function(errr,users){
                                if(errr || users.length <= 0){
                                    return res.json({
                                        success: false,
                                        message: errr || "users not found"
                                    })
                                }
                                else{
                                    for (var i = 0; i < users.length; i++) {

//                                         var phonenumPromise = new AWS.SNS({apiVersion: '2010-03-31'}).checkIfPhoneNumberIsOptedOut({phoneNumber: '2'+users[i].phoneNumber}).promise();

// // Handle promise's fulfilled/rejected states
//                                         phonenumPromise.then(
//                                           function(data) {
//                                             console.log("Phone Opt Out is " + data.isOptedOut);
//                                           }).catch(
//                                             function(err) {
//                                             console.error(err, err.stack);
//                                         });
                                        //users[i]
                                        var params = {
                                            Message: "Enqaz: this promo code '"+code.code+"'\ngives you "+code.discountRate+"% discount\nvalid till "+code.expiresAt,
                                            MessageStructure: 'string',
                                            PhoneNumber: '2'+users[i].phoneNumber,
                                            Subject: 'ENQAZ'
                                        };

                                        sns.publish(params, function(err, data) {
                                            if (err) console.log(err, err.stack); // an error occurred
                                            else     console.log(data);           // successful response
                                        });
                                    }
                                }
                            })

                            
                            
                            res.json({
                                success: true,
                                code: code
                            });
                        }
                    });
            }
        });
    } else {
        res.json(middlewares.checkAllFieldsExist)
    }
};
module.exports.getMyPromosList = function (req, res, next) {
    promoCodes.findOne({
        userIds: req.user._doc._id,
        expiresAt: {
            $gte: new Date(Date.now())
        }
    })
    .select({"userIds": 0, "startDate": 0})
    .exec( (err, promoCode) => {
        if (err || !promoCode) {
            res.json({
                success: false,
                message: err || "user has no valid promo codes"
            })
        }
        else {
            res.json({
                success: true,
                promoCode: promoCode
            })
        }
    });
}
module.exports.activatePromoCode = function(req, res){
    var reqBody = req.body;

    ReferralModel.findOne({
        refCode: reqBody.promoCode
    }, (err , foundReferral) => {
        if(err){

             return res.json({
                success: false,
                message: err 
            })
        }
        else if(foundReferral){
          
          RequestService.findOne(
            {
                userId: req.user._doc._id
            }, (err , foundRequest) => {
         if(err || foundRequest){

             return res.json({
                success: false,
                message: err || "Sorry it just valid for first time using ENQAZ"
            })
        }else{
              promoCodes.create({
                        //userId: reqBody.userId,
                        code: reqBody.promoCode,
                        //expiresAt: reqBody.expiresAt,
                        //startDate: reqBody.startDate,
                        discountRate: 50
                    },(err , code) => {
                        if(err || !code) {
                            res.json({
                                success: false,
                                message: err
                            })
                        }else{
                            code.userIds = code.userIds.concat([req.user._doc._id])
                            code.userIds = code.userIds.concat([foundReferral.userId])
                            code.save((er, result) => {
                        if(er || !result){
                            return res.json({
                                success: false,
                                message: er || "failed"
                            })
                        }
                        else{
                            res.json({
                                success: true,
                                message: "Referral code added successfully",
                                promoCode: {
                                    code: result.code,
                                    discountRate: result.discountRate
                                }
                            })
                        }
                    })
                        }
                    })
        }
            })
        
        }
        else{

    promoCodes.findOne({
        userIds: req.user._doc._id,
        expiresAt: {
            $gte: new Date(Date.now())
        }
    }, function(er, foundCode){
        if(er){
            return res.json({
                success: false,
                message: er
            })
        }
        else if(foundCode /*&& new Date(Date.now()) <= code.expiresAt*/){
            return res.json({
                success: false,
                message: "your already have a valid promo code"
            })
        }
        else{
            promoCodes.findOne({code: reqBody.promoCode}, function(err, code){
                if(err || !code){
                    return res.json({
                        success: false,
                        message: err || "promo code not found"
                    })
                }
                else{
                    //code.userIds.push(req.user._doc._id);
                    if(new Date(Date.now()) > code.expiresAt){
                        res.json({
                            success: false,
                            message: "this promo code is expired"
                        })
                    }
                    else{
                        code.userIds = code.userIds.concat([req.user._doc._id])
                    console.log(req.user._doc._id, code.userIds)
                    code.save((er, result) => {
                        if(er || !result){
                            return res.json({
                                success: false,
                                message: er || "failed"
                            })
                        }
                        else{
                            res.json({
                                success: true,
                                message: "promo code added successfully",
                                promoCode: {
                                    code: result.code,
                                    expiresAt: result.expiresAt,
                                    discountRate: result.discountRate
                                }
                            })
                        }
                    })
                    }
                    
                }
            })
        }
    })
        }
    }) 
    
}
// function to pay either cash or by the selected fav card.