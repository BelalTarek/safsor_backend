var valid = require('card-validator');
let settingsModel = require('./settingsModel');
let requestsModel = require('../requests/model');
let invoiceModel = require('../invoice/model');
let middlewares = require('../middlewares');
var unirest = require("unirest");
const RequestService = require('../requests/model');
const pushModel = require('../pushNot/model');
const serviceModel = require('../services/model');
const vendor = require('../auth/vendor/vendorModel');
var FCM = require('fcm-push');
var fcm = new FCM('AIzaSyCogA6raV7doIYReJiuy9HYUCyplslbCII');
var geodist = require('geodist')
var Report = require('../report/model')
var PromoCode = require('../promocodes/model')

module.exports.addSettings = function (req, res, next) {
  var endTripCost;
  let settings = req.body;
  //if (settings.firstRadius && settings.secondRadius && settings.firstKmPrice && settings.secondKmPrice && settings.highLimit && settings.lowLimit && settings.waitingTimePrice && settings.cancellationFee) {
    settingsModel.find({}, (err, docs) => {
      if (err) {
        res.json({
          success: false,
        });
      } else if (docs.length > 0) {
        docs[0].firstRadius = settings.firstRadius || docs[0].firstRadius;
        docs[0].secondRadius = settings.secondRadius || docs[0].secondRadius;
        docs[0].firstKmPrice = settings.firstKmPrice || docs[0].firstKmPrice;
        docs[0].secondKmPrice = settings.secondKmPrice || docs[0].secondKmPrice;
        docs[0].highLimit = settings.highLimit || docs[0].highLimit;
        docs[0].lowLimit = settings.lowLimit || docs[0].lowLimit;
        docs[0].waitingTimePrice = settings.waitingTimePrice || docs[0].waitingTimePrice;
        docs[0].cancellationFee = settings.cancellationFee || docs[0].cancellationFee
        docs[0].save((err, done) => {
          if (done) {
            res.json({
              success: true
            })
          }
        })
      } else {
        let model = new settingsModel(settings);
        model.save((err, obj) => {
          console.log(err, obj);
          if (err) {
            res.json({
              success: false,
            });
          } else {
            res.json({
              success: true
            })
          }
        })

      }
    });
  // } else {
  //   res.json(middlewares.checkAllFieldsExist)
  // }
};

module.exports.getTripSettings = function (req, res, next) {  
    settingsModel.find({}, (err, docs) => {
      if (err) {
        res.json({
          success: false,
        });
      } else 
        {
          console.log(docs[0])
            res.json({
              success: true,
              message: docs[0]
            })
          }
        
      
    });
  
};

module.exports.endTrip = function (req, res, next) {
  let reqBody = req.body;
  var endTripCost;
  var baseFare;
  if (reqBody.requestId && reqBody.driverLat && reqBody.driverLong) {
    requestsModel.findOne({
      _id: reqBody.requestId
    }, (err, foundReq) => {
      if (err || !foundReq) {
        res.json({
          success: false,
          message: err || "not found"
        });
      } else if (foundReq) {
        // console.log(foundReq)
        if (foundReq.status === "running") {
          serviceModel.find({
            _id:{
            $in: foundReq.serviceId
            }
          }, (err, serviceFound) => {
            // console.log("err, serviceFound", err, serviceFound)
            if (err || !serviceFound) {
              res.json({
                success: false,
                message: err || "not found"
              });
            } else {
              settingsModel.find({}, (err, settings) => {
                //console.log("err, settings", err, settings)
                if (err) {
                  res.json({
                    success: false,
                  });
                }else if (settings) {
                foundReq.status = 'finished';
                let enqazPercentage;      
            if(serviceFound[0].name === 'Winch'){
              baseFare = serviceFound[0].baseFare
              foundReq.endTripLocation.lat = reqBody.driverLat;
              foundReq.endTripLocation.long = reqBody.driverLong;
        var dist = geodist({
                lat: foundReq.startTripLocation.lat,
                lon: foundReq.startTripLocation.long
              },{
                lat: reqBody.driverLat,
                lon: reqBody.driverLong
              });
        //console.log(foundReq.endTripLocation.lat)
        if(dist < 40) {
          //console.log(service.baseFare)
          enqazP = (serviceFound[0].baseFare + (dist * settings[0].secondKmPrice)) * 0.20  + ((reqBody.waitingTime / 60) * 2);
          if(enqazP < 100) {
            enqazPercentage = settings[0].lowLimit;
          endTripCost = serviceFound[0].baseFare + (dist * settings[0].secondKmPrice) + settings[0].lowLimit + ((reqBody.waitingTime / 60) * 2);
          }else{
            enqazPercentage = (serviceFound[0].baseFare + (dist * settings[0].secondKmPrice)) * 0.20;
          endTripCost = serviceFound[0].baseFare + (dist * settings[0].secondKmPrice) + enqazP + ((reqBody.waitingTime / 60) * 2);
          }

        }else{
          enqazPercentage = (serviceFound[0].baseFare + (dist * settings[0].firstKmPrice)) * 0.20;
          endTripCost= serviceFound[0].baseFare + (dist * settings[0].firstKmPrice) + ((reqBody.waitingTime / 60) * 2) + (serviceFound[0].baseFare + (dist * settings[0].firstKmPrice)) * 0.20;
        }
        
        }
     // }
     else {
              foundReq.endTripLocation.lat = reqBody.driverLat;
              foundReq.endTripLocation.long = reqBody.driverLong;
              var dist = geodist({
                lat: foundReq.acceptReqLocation.lat,
                lon: foundReq.acceptReqLocation.long
              }, {
                lat: foundReq.endTripLocation.lat,
                lon: foundReq.endTripLocation.long
              });
              
              //console.log(dist);
               if(foundReq.serviceId.length == 1){
                baseFare = serviceFound[0].baseFare
                  // console.log("settings", dist, Number(settings.firstRadius))
                  if (dist > settings[0].firstRadius) {
                    enqazPercentage = 50;
                    //console.log(serviceFound.baseFare, dist, Number(settings.firstRadius), Number(settings.kmPrice));
                    endTripCost = serviceFound[0].baseFare + (dist - settings[0].firstRadius) * 2.5 + ((reqBody.waitingTime / 60) * 2) + enqazPercentage;
                    
                  } else {
                    enqazPercentage = 50;
                    endTripCost = serviceFound[0].baseFare + enqazPercentage;
                  }
                }else{
                  baseFare = serviceFound[0].baseFare + serviceFound[1].baseFare
                  if (dist > settings[0].firstRadius) {
                    enqazPercentage = settings[0].lowLimit;
                    //console.log(serviceFound.baseFare, dist, Number(settings.firstRadius), Number(settings.kmPrice));
                    endTripCost = serviceFound[0].baseFare + serviceFound[1].baseFare + (dist - settings[0].firstRadius) * 2.5 + ((reqBody.waitingTime / 60) * 2);
                    
                  } else {
                    enqazPercentage = settings[0].lowLimit;
                    endTripCost = serviceFound[0].baseFare + serviceFound[1].baseFare + settings[0].lowLimit;
                  }
                }
                  }
                  PromoCode.findOne({
                    userIds: foundReq.userId,
                    expiresAt: {
                      $gt : new Date(Date.now())
                    }
                  }, (e, promo) => {
                    if(e){
                      res.json({
                        success: false,
                        message: e
                      })
                    }
                    else if(!promo){
                      foundReq.finalCost = Number.parseFloat(endTripCost).toFixed(2);
                      console.log(endTripCost);
                      foundReq.save((err, doc) => {
                        // console.log(err, doc)
                        if (err) {
                          res.json({
                            success: false,
                            message: 'failed'
                          });
                        } else {
                          if(req.body.connectionId){
                            Report.findOneAndUpdate({
                              driverId: foundReq.driverId,
                              _id: req.body.connectionId
                            },
                            {
                              $inc: {
                                'connectionHistory.numOfReqAccepted': 1,
                                'connectionHistory.numOfTrips': 1,
                                'connectionHistory.totalProfit': foundReq.finalCost,
                                'connectionHistory.enqazPercentage': enqazPercentage,
                                'connectionHistory.kilometers': dist  
                              },
                              $push: {
                                'connectionHistory.trips': foundReq._id
                              }
                            // $set: {'connectionHistory.rating': foundReq.driverRate}
                            },{new: true}, function(er, resultt){
                              if(er || !resultt){
                                return res.json({
                                  success: false,
                                  message: er || 'error updating'
                                })
                              }
                              else{
                                console.log('reporttt  '+resultt)
                              } 
                            })
                          }
                          else{
                            Report.findOneAndUpdate({
                            driverId: foundReq.driverId,
                            'connectionHistory.endTime': null
                            },
                            {
                              $inc: {
                                'connectionHistory.numOfReqAccepted': 1,
                                'connectionHistory.numOfTrips': 1,
                                'connectionHistory.totalProfit': foundReq.finalCost,
                                'connectionHistory.enqazPercentage': enqazPercentage,
                                'connectionHistory.kilometers': dist  
                              },
                              $push: {
                                'connectionHistory.trips': foundReq._id
                              }
                            // $set: {'connectionHistory.rating': foundReq.driverRate}
                            },{new: true}, function(er, resultt){
                              if(er || !resultt){
                                return res.json({
                                  success: false,
                                  message: er || 'error updating'
                                })
                              }
                              else{
                                console.log('reporttt  '+resultt)
                              } 
                            })
                          }
                          pushModel.findOne({
                            userId: foundReq.userId
                          }, (err, userFcmToken) => {
                            if (err || !userFcmToken) {
                              // console.log(err, userFcmToken)
                            } else if (userFcmToken) {
                              vendor.findOne({
                                _id: req.user._doc._id
                              }, (err, foundDriver) => {
                                // console.log("err,foundDriver", err, foundDriver)
                                if (err || !foundDriver) {
                                  res.json({
                                    success: false,
                                    message: err || "no driver"
                                  });
                                } else if (foundDriver) {
                                  var message = {
                                    to: userFcmToken.fcmToken, // required fill with device token or topics
                                    collapse_key: 'your_collapse_key',
                                    notification: {
                                      title: "Request finished",
                                      body: "Your request finished successfully",
                                    },
                                    data: {
                                      notificationType: "requestDone"
                                    }
                                  };
                                  //promise stylelet 
                                  requestIvoice = new invoiceModel({
                                    requestId: reqBody.requestId,
                                    customerId: foundReq.userId,
                                    customerName: foundReq.userName,
                                    driverId: foundDriver._id,
                                    driverName: foundDriver.name,
                                    date: foundReq.createdAt,
                                    finalCost: foundReq.finalCost,
                                    enqazPercentage: enqazPercentage,
                                    dist: dist,
                                    paymentType: foundReq.paymentType,
                                    kmPrice: settings[0].firstKmPrice || settings[0].secondKmPrice,
                                    carModel: foundReq.carModel,
                                    carManufactory: foundReq.carManufactory,
                                    serviceName: foundReq.serviceName,
                                    serviceBaseFare: baseFare,
                                    waitingTime:parseInt((reqBody.waitingTime / 60)),
                                    waitingTimePrice:parseInt((reqBody.waitingTime / 60) * 2)
                                  })
                                  requestIvoice.save((err, invoice) => {
                                    if (!invoice) {
                                      res.json({
                                        success: false,
                                        message: err
                                      });
                                    } else {

                                      fcm.send(message)
                                        .then(function (response) {
                                          res.json({
                                            success: true,
                                            data: invoice
                                          });
                                        })
                                        .catch(function (err) {
                                          res.json({
                                            success: false,
                                            message: err
                                          });
                                          // console.log("Something has gone wrong!", err);
                                        });
                                    }
                                  })
                                }
                              });
                            }
                          })
                        }
                      });
                    }
                    else{
                      var finalCostAfterDis = Number.parseFloat(endTripCost).toFixed(2) - Number.parseFloat((endTripCost*promo.discountRate/100)).toFixed(2)
                      foundReq.finalCost = finalCostAfterDis
                      console.log(endTripCost);
                      foundReq.save((err, doc) => {
                        // console.log(err, doc)
                        if (err) {
                          res.json({
                            success: false,
                            message: 'failed'
                          });
                        } else {
                          if(req.body.connectionId){
                            Report.findOneAndUpdate({
                              driverId: foundReq.driverId,
                              _id: req.body.connectionId
                            },
                            {
                              $inc: {
                                'connectionHistory.numOfReqAccepted': 1,
                                'connectionHistory.numOfTrips': 1,
                                'connectionHistory.totalProfit': foundReq.finalCost,
                                'connectionHistory.enqazPercentage': enqazPercentage,
                                'connectionHistory.kilometers': dist  
                              },
                              $push: {
                                'connectionHistory.trips': foundReq._id
                              }
                            // $set: {'connectionHistory.rating': foundReq.driverRate}
                            },{new: true}, function(er, resultt){
                              if(er || !resultt){
                                return res.json({
                                  success: false,
                                  message: er || 'error updating'
                                })
                              }
                              else{
                                console.log('reporttt  '+resultt)
                              } 
                            })
                          }
                          else{
                            Report.findOneAndUpdate({
                            driverId: foundReq.driverId,
                            'connectionHistory.endTime': null
                            },
                            {
                              $inc: {
                                'connectionHistory.numOfReqAccepted': 1,
                                'connectionHistory.numOfTrips': 1,
                                'connectionHistory.totalProfit': foundReq.finalCost,
                                'connectionHistory.enqazPercentage': enqazPercentage,
                                'connectionHistory.kilometers': dist  
                              },
                              $push: {
                                'connectionHistory.trips': foundReq._id
                              }
                            // $set: {'connectionHistory.rating': foundReq.driverRate}
                            },{new: true}, function(er, resultt){
                              if(er || !resultt){
                                return res.json({
                                  success: false,
                                  message: er || 'error updating'
                                })
                              }
                              else{
                                console.log('reporttt  '+resultt)
                              } 
                            })
                          }
                          pushModel.findOne({
                            userId: foundReq.userId
                          }, (err, userFcmToken) => {
                            if (err || !userFcmToken) {
                              // console.log(err, userFcmToken)
                            } else if (userFcmToken) {
                              vendor.findOne({
                                _id: req.user._doc._id
                              }, (err, foundDriver) => {
                                // console.log("err,foundDriver", err, foundDriver)
                                if (err || !foundDriver) {
                                  res.json({
                                    success: false,
                                    message: err || "no driver"
                                  });
                                } else if (foundDriver) {
                                  var message = {
                                    to: userFcmToken.fcmToken, // required fill with device token or topics
                                    collapse_key: 'your_collapse_key',
                                    notification: {
                                      title: "Request finished",
                                      body: "Your request finished successfully",
                                    },
                                    data: {
                                      notificationType: "requestDone"
                                    }
                                  };
                                  //promise stylelet 
                                  requestIvoice = new invoiceModel({
                                    requestId: reqBody.requestId,
                                    customerId: foundReq.userId,
                                    customerName: foundReq.userName,
                                    driverId: foundDriver._id,
                                    driverName: foundDriver.name,
                                    date: foundReq.createdAt,
                                    finalCostBeforeDiscount: Number.parseFloat(endTripCost).toFixed(2),
                                    finalCost: foundReq.finalCost,
                                    promoCodeDiscount: Number.parseFloat((endTripCost*promo.discountRate/100)).toFixed(2),
                                    enqazPercentage: enqazPercentage,
                                    dist: dist,
                                    paymentType: foundReq.paymentType,
                                    kmPrice: settings[0].firstKmPrice || settings[0].secondKmPrice,
                                    carModel: foundReq.carModel,
                                    carManufactory: foundReq.carManufactory,
                                    serviceName: foundReq.serviceName,
                                    serviceBaseFare: baseFare,
                                    waitingTime:parseInt((reqBody.waitingTime / 60)),
                                    waitingTimePrice:parseInt((reqBody.waitingTime / 60) * 2)
                                  })
                                  requestIvoice.save((err, invoice) => {
                                    if (!invoice) {
                                      res.json({
                                        success: false,
                                        message: err
                                      });
                                    } else {

                                      fcm.send(message)
                                        .then(function (response) {
                                          res.json({
                                            success: true,
                                            data: invoice
                                          });
                                        })
                                        .catch(function (err) {
                                          res.json({
                                            success: false,
                                            message: err
                                          });
                                          // console.log("Something has gone wrong!", err);
                                        });
                                    }
                                  })
                                }
                              });
                            }
                          })
                        }
                      });
                    }
                  })
                  //foundReq.finalCost = parseInt(endTripCost);
                  
                }

              });
            }
          })
        }else{
         res.json({
          success: false,
          message: "trip not running"
          });
        }
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
}


exports.driverArrived = function (req, res, next) {
  let reqBody = req.body;
  let userData = req.user;
  if (reqBody.driverLat && reqBody.driverLong && reqBody.requestId) {
    //console.log("h1")
    RequestService.findOne({
      _id: reqBody.requestId,
    }, (err, foundReq) => {
      //console.log("h1", err, foundReq)

      if (err || !foundReq) {
        res.json({
          success: false,
          message: err || "not found"
        });
      } else if (foundReq) {
        //console.log(foundReq)
        if (foundReq.status === "accepted") {
          foundReq.status = 'running';
          foundReq.startTripLocation.lat = reqBody.driverLat;
          foundReq.startTripLocation.long = reqBody.driverLong;
          foundReq.save((err, doc) => {
            if (err) {
              res.json({
                success: false,
                message: err || 'failed'
              })
            } else {
              console.log(doc)
              pushModel.findOne({
                userId: foundReq.userId
              }, (err, userFcmToken) => {
                if (err || !userFcmToken) {
                  res.json({
                    success: false,
                    message: err || 'failed'
                  })
                }
                if (userFcmToken) {
                  vendor.findOne({
                    _id: userData._doc._id
                  }, (err, foundDriver) => {
                    if (err || !foundDriver) {
                      res.json({
                        success: false,
                        message: err || "no driver"
                      });
                    } else if (foundDriver) {
                      var message = {
                        to: userFcmToken.fcmToken, // required fill with device token or topics
                        collapse_key: 'your_collapse_key',
                        notification: {
                          title: "Your Help arrived!",
                          body: "Your help have just arrived",
                        },
                        data: {
                          notificationType: "driverArrived"
                        }
                      };
                      //promise style
                      fcm.send(message)
                        .then(function (response) {
                          res.json({
                            success: true
                          });
                        })
                        .catch(function (err) {
                          res.json({
                            success: false,
                            message: err
                          });
                          console.log("Something has gone wrong!", err);
                        });
                    }
                  });

                }
              })
            }
          });
        }
      }
    })
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
};
// trip details api
exports.getTripDetails = function(req, res){
  var reqBody = req.body;

  if(reqBody.requestId){
    invoiceModel.findOne({requestId: reqBody.requestId}, function(err, invoice){
    if(err || !invoice){
        return res.json({
          success: false,
          message: err || "trip details not found"
        })
    }
    else{
      requestsModel.findById(reqBody.requestId, function(err, request){
        if(err || !request){
          return res.json({
            success: false,
            message: err || "couldn't get trip start & end locations",
            trip_details: invoice
          })
        }
        else{
          
          // tripDetails.startTripLocation = request.startTripLocation;
          // tripDetails.endTripLocation = request.endTripLocation;
          // tripDetails = {
          //   startTripLocation: request.startTripLocation,
          //   endTripLocation: request.endTripLocation
          // }
         // tripDetails += invoice.toJSON();
          //console.log(JSON.parse(tripDetails))
          if(req.user._doc.role === 5){
            res.json({
              success: true,
              trip_details: invoice,
              startTripLocation: request.startTripLocation,
              endTripLocation: request.endTripLocation,
              //driverRate: request.driverRate,
              userRate: request.userRate //Object.assign(invoice.toJSON(), tripDetails)
            })
          }
          else if( req.user._doc.role === 4){
            res.json({
              success: true,
              trip_details: invoice,
              startTripLocation: request.startTripLocation,
              endTripLocation: request.endTripLocation,
              driverRate: request.driverRate,
              //userRate: request.userRate //Object.assign(invoice.toJSON(), tripDetails)
            })
          }
        }
      })
    }
  })
  }
  else{
    return res.json(middlewares.checkAllFieldsExist)
  }
  
}