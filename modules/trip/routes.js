var express = require('express');
var router = express.Router();
//require controllers
var tripController = require('./controllers');
var middlewares = require('../middlewares');
var jwt = require('express-jwt');

//Routers
/**
 * @api {post} /trip/edit-settings  edit trip settings
 * @apiName editSettings
 * @apiGroup tripModule
 * @apiParam {Number} firstRadius
 * @apiParam {Number} secondRadius
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *   }
 */
router.post('/edit-settings', tripController.addSettings);
/**
 * @api {post} /trip/end-trip  end trip 
 * @apiName endTrip
 * @apiGroup tripModule
 * @apiParam {Number} firstRadius
 * @apiParam {Number} secondRadius
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *   }
 */
router.post('/end-trip', jwt({
    secret: process.env.secret
}), tripController.endTrip);
/**
 * @api {post} /trip/driver-arrived  driver arrived 
 * @apiName driverArrived
 * @apiGroup tripModule
 * @apiParam {string} driverLat driverLat.
 * @apiParam {string} driverLong driverLong.
 * @apiParam {string} requestId requestId.
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *   }
 */
router.post('/driver-arrived', jwt({
    secret: process.env.secret
}), tripController.driverArrived);
/**
 * @api {post} /trip/show-trip-details  show trip details for specific request 
 * @apiName ShowTripDetails
 * @apiGroup tripModule
 * @apiParam {string} requestId request Id.
 *
 * @apiSuccessExample Success-Response:
 *{
 *      success: true,
 *      trip_details: invoice,
 *      startTripLocation: request.startTripLocation,
 *      endTripLocation: request.endTripLocation
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *   }
 */
router.post('/show-trip-details', jwt({
    secret: process.env.secret
}), tripController.getTripDetails)

//"/trip/get-trip-settings"
router.get('/get-trip-settings', tripController.getTripSettings);
module.exports = router;