const mongoose = require('mongoose')
const config = require('config')

const tripSettingsSchema = mongoose.Schema({
    firstRadius: {
        type: Number,
        //default: 5
    },
    secondRadius: {
        type: Number,
         //default: 10
    },
    firstKmPrice: {
        type: Number,
        //default: 7
    },
    secondKmPrice: {
        type: Number,
        //default: 7
    },
    highLimit:{
        type: Number,
        //default: 250
    },
    lowLimit:{
        type: Number,
        //default: 100
    },
    waitingTimePrice:{
        type: Number,
       // default: 2
    },
    cancellationFee: {
        type: Number,
        //default: 30
    }
});

const tripSettings = module.exports = mongoose.model('tripSettings', tripSettingsSchema)