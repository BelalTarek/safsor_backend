const express = require('express');
const router = express.Router();
const pushModel = require('./model');
let middlewares = require('../middlewares')
var FCM = require('fcm-push');
require('dotenv').config();

var serverKey = process.env.serverKey;
var fcm = new FCM('AAAA31tLaBA:APA91bGcVhg9k4hURRXmIFgoJjvvD3TE6olUdZof0Wiy4XxtmE5pkbubXghr0irA8zoYulGlOsKWiT6N6YseD61mHI2F0A4k674Jc_Yn8Eb8lQrulyf_WR8VYpaK7QAjfPLSd-cWu5LU');


//send push not.
exports.sendPushToOne = function (req, res, next) {
  let reqBody = req.body;
  if (reqBody.userId && reqBody.notifBody && reqBody.notifTitle) {
    pushModel.findOne({
      userId: reqBody.userId
    }, (err, userFcmToken) => {
      if (userFcmToken) {
        var message = {
          to: userFcmToken.fcmToken, // required fill with device token or topics
          collapse_key: 'your_collapse_key',
          data: {
            body: reqBody.notifBody
          },
          notification: {
            title: reqBody.notifTitle,
            body: reqBody.notifBody
          }
        };
        //promise style
        fcm.send(message)
          .then(function (response) {
            console.log("Successfully sent with response: ", response);
            res.json({
              success: true,
            });
          })
          .catch(function (err) {
            console.log("Something has gone wrong!");
            res.json({
              success: false,
            });
          })
      } else {
        res.json({
          success: false,
          message: 'no fcm token provided'
      });

      }
    })
  }
}

exports.sendPushToTopic = function (req, res, next) {
  let reqBody = req.body;
  if (reqBody.userId && reqBody.notifBody && reqBody.notifTitle && reqBody.topicName) {
    var message = {
      to: topicName, // required fill with device token or topics
      collapse_key: 'your_collapse_key',
      data: {
        body: reqBody.notifBody
      },
      notification: {
        title: reqBody.notifTitle,
        body: reqBody.notifBody
      }
    };
    //promise style
    fcm.send(message)
      .then(function (response) {
        console.log("Successfully sent with response: ", response);
      })
      .catch(function (err) {
        console.log("Something has gone wrong!");
        console.error(err);
      })
  }
}