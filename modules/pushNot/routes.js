var express = require('express');
var router = express.Router();
//require controllers
var controllers = require('./controllers');
var middlewares = require('../middlewares');

//routers
/**
 * @api {post} /push/send-to-one send push notification to one device
 * @apiName send Push Notification
 * @apiGroup pushNot
 *
 * @apiParam {string} userId user id of person to send notification to
 * @apiParam {string} notifBody body of notification 
 * @apiParam {string} notifTitle title of notification 
 * 
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *      newServiceName: 'newServiceName'
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *        message: 'failed to add'
 *   }
 */

router.post('/send-to-one', controllers.sendPushToOne);
/**
 * @api {post} /push/send-to-topic send push notification to topic(group of devices)
 * @apiName send Push Notification
 * @apiGroup pushNot
 *
 * @apiParam {string} userId user id of person to send notification to
 * @apiParam {string} notifBody body of notification 
 * @apiParam {string} notifTitle title of notification 
 * @apiParam {string} topicName topic of notification 
 * 
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *      newServiceName: 'newServiceName'
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *        message: 'failed to add'
 *   }
 */

router.post('/send-to-topic', controllers.sendPushToTopic);

module.exports = router;