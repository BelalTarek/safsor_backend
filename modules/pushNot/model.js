const mongoose = require('mongoose')
const config = require('config')

const PushNotSchema = mongoose.Schema({
    userId: {
        type: String,
        require: false
    },
    fcmToken: {
        type: String,
        require: false
    }
})

const pushNot = module.exports = mongoose.model('pushNot', PushNotSchema)