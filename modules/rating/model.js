const mongoose = require('mongoose')

const ratingSchema = mongoose.Schema({
    //user can be a customer or a driver
    userId: {
        type: String
    },
    rating: {
        type: Number
    }
})

const Rating = module.exports = mongoose.model('Rating', ratingSchema);