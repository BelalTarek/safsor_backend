var express = require('express');
var router = express.Router();
var controller = require('./controller')
var jwt = require('express-jwt');

router.post('/rate-user', controller.rateUser)

router.get('/show-my-rating', jwt({
    secret: process.env.secret
}), controller.showMyRating)

module.exports = router;

