var middlewares = require('../middlewares')
var Rating = require('./model')

exports.rateUser = function(req, res){
    var ratingInfo = req.body;
    if(ratingInfo.userId && ratingInfo.rating){
        Rating.findOne({userId: ratingInfo.userId}, function(err, rate){
            if(err){
                return res.json({
                    success: false,
                    message: err 
                })
            }
            else if(!rate){
                var newRating = new Rating({
                    userId: ratingInfo.userId,
                    rating: ratingInfo.rating
                })
                newRating.save(function(er, result){
                    if(er || !result){
                        return res.json({
                            success: false,
                            message: er || "Failed to rate user"
                        })
                    }
                    else{
                        res.json({
                            success: true,
                            message: "user rated successfully",
                            rating: result.rating
                        })
                    }
                })
            }
            else{
                var oldRating = rate.rating;
                rate.rating = (oldRating + Number(ratingInfo.rating)) / 2;
                rate.save(function(er, result){
                    if(er || !result){
                        return res.json({
                            success: false,
                            message: er || "Failed to rate user"
                        })
                    }
                    else{
                        res.json({
                            success: true,
                            message: "user rated successfully",
                            rating: result.rating
                        })
                    }
                })
            }
        })
    }
    else{
        return res.json(middlewares.checkAllFieldsExist)
    }
}

exports.showMyRating = function(req, res){
    if(req.user){
        Rating.findOne({
            userId: req.user._doc._id
        }, function(err, rate){
            if(err){
                return res.json({
                    success: false,
                    message: err
                })
            }
            else{
                res.json({
                    success: true,
                    rating: rate ? rate.rating : 0.0 
                })
            }
        })
    }
    else{
        return res.json({
            success: false,
            message: "Authorization token is missing ><"
        })
    }
}