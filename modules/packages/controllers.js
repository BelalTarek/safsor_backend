var valid = require('card-validator');
let packages = require('./model');
const decodeJWT = require('decoding_jwt');
let middlewares = require('../middlewares');


module.exports.addPackage = function(req, res, next) {
	let reqBody = req.body;
    if (reqBody.duration && reqBody.serviceName && reqBody.numberOfCars) {
        let findQuery = {
            duration: reqBody.duration,
            serviceName: reqBody.serviceName,
            numberOfCars: reqBody.numberOfCars,
        };
        packages.findOne(findQuery, (err, package) => {
            if (package) {
                res.json({
                    success: false,
                    message: "package already added"
                })
            } else {
                packages.create({
                        name: reqBody.packageName,
                        duration: reqBody.duration,
                        serviceName: reqBody.serviceName,
                        numberOfCars: reqBody.numberOfCars,
                    },
                    (err, package) => {
                        if (!package) {
                            res.json({
                                success: false,
                                message: err
                            })
                        } else {
                            res.json({
                                success: true,
                                package: package
                            });
                        }
                    });
            }
        });
    } else {
        res.json(middlewares.checkAllFieldsExist)
    }
}
exports.listPackages = function (req, res, next) {
  packages.find({
    },
    function (err, packages) {
      if (!packages) {
        res.json({
          success: false,
          message: "No packages"
        })
      } else {
        res.json({
          success: true,
          message: packages
        })
      }
    })
}