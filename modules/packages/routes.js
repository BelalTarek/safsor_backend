var express = require('express');
var router = express.Router();
//require controllers
var packagesController = require('./controllers');
var middlewares = require('../middlewares');


/**
 * @api {post} /packages/add-package add packages
 * @apiName addPackages
 * @apiGroup packages
 *
 * @apiParam {string} duration duration for this package.
 * @apiParam {string} serviceName service that package include it.
 * @apiParam {Number} numberOfCars .
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
       package: package
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
         message: "package already added"
 *   }
 */



router.post('/add-package', packagesController.addPackage);

/**
 * @api {get} /packages/list-package list of packages
 * @apiName listOfPackages
 * @apiGroup packages
 *
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
       package: package
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
         message: "No packages"
 *   }
 */

router.get('/list-package', packagesController.listPackages);

module.exports = router;