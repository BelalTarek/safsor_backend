const mongoose = require('mongoose')
const config = require('config')

const packagesSchema = mongoose.Schema({
    name: {
        type: String
    },duration: {
        type: String,
        required: true
    },
    serviceName: {
        type: String,
        required: true
    },
    numberOfCars: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('packages', packagesSchema)