var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
//require controllers
var controller = require('./controllers');
const middlewares = require('../../middlewares');

var multer = require('multer');
var upload = multer({
    storage: multer.diskStorage({
        filename: function (req, file, cb) {
            cb(null, file.fieldname )
        }
    }),
    // rename: function (fieldname, filename) {
    //   return filename + Date.now();
    // },
    // onFileUploadStart: function (file) {
    //   console.log(file.originalname + ' is starting ...');
    // },
    // onFileUploadComplete: function (file) {
    //   console.log(file.fieldname + ' uploaded to  ' + file.path);
    // }
}).fields([
    {name: "imageURL", maxCount: 1}, 
    {name: "idURL", maxCount: 1}, 
    {name: "licenseURL", maxCount: 1}
]);

const baseRoute = '/user/admin/shared/';
// middleware to protect all except mentioned apis
router.use(jwt({
    secret: process.env.secret
}).unless({
    path: [baseRoute + 'login', baseRoute + 'forget-pass-code', baseRoute + 'verify-forget-pass-code']
}), function (req, res, next) {
    if (req.user) {
        console.log(req.user._doc.role);
        if (req.user._doc.role != 1 && req.user._doc.role != 2) return res.json({
            sucess: false,
            message: "you are not an admin or moderator"
        });
        next();
    } else {
        next();
    }
});
/**
 * @api {get} /user/admin/super/show-moderators/:page show list of customers
 * @apiName showModerators
 * @apiGroup admin
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *      list: list
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *      message: 'admin faild to register'
 *   }
 */
router.get('/show-moderators', controller.showModerators);
//Routers
/**
 * @api {get} /user/admin/shared/show-vendor show list of vendors
 * @apiName showVendors
 * @apiGroup admin
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *      list: list
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *      message: 'admin faild to register'
 *   }
 */
router.get('/show-vendor', controller.showVendor);
/**
 * @api {get} /user/admin/shared/show-customer show list of customers
 * @apiName showCustomers
 * @apiGroup admin
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *      list: list
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *      message: 'admin faild to register'
 *   }
 */
router.get('/show-customer', controller.showCustomer);
/**
 * @api {post} /user/admin/shared/add-vendor add new vendor
 * @apiName addVendor
 * @apiGroup admin
 * @apiParam {string} name user name
 * @apiParam  {string} email user email address.
 * @apiParam  {number} phoneNumber user phone number.
 * @apiParam  {string} password user password.
 * @apiParam  {string} username username.
 * @apiParam  {Boolean} isVendor is business account.
 * @apiParam  {string} [packages] array of packages for Business account.
 * @apiParam  {string} services array of services for each vendor.
 * @apiParam  {number} enqazPercentage percentage for each trip.
 * @apiParam  {Date} setOfDate Date to set of with enqaz exp 23-jul-2018.
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *      message: 'vendor registered successfully'
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *      message: 'admin faild to register'
 *   }
 */
router.post('/add-vendor', upload, controller.addVendor);
/**
 * @api {post} /user/admin/shared/add-customer add new customer
 * @apiName addModerator
 * @apiGroup admin
 * @apiParam {string} name user name
 * @apiParam  {string} email user email address.
 * @apiParam  {number} phoneNumber user phone number.
 * @apiParam  {string} password user password.
 * @apiParam  {string} username username.
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *      message: 'admin registered successfully'
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *      message: 'admin faild to register'
 *   }
 */
router.post('/add-customer', controller.addCustomer);
/**
 * @api {post} /user/admin/shared/update-vendor update vendor info
 * @apiName update user info
 * @apiGroup admin
 * @apiParam  {string} _id id for vendor to update his data.
 * @apiParam  {string} [name]
 * @apiParam  {string} [username]
 * @apiParam  {string} [email] 
 * @apiParam  {string} [addresses] 
 * @apiParam  {number} [phoneNumbers] 
 * @apiParam  {string} [password] user password.
 * @apiParam  {string} [packages] array of packages for Business account.
 * @apiParam  {string} [services] array of services for each vendor.
 * @apiParam  {Date} [setOfDate] Date to set of with enqaz exp 23-jul-2018.
 * @apiSuccessExample Success-Response:
 *{
 *"success": true,
 * "message" :"Info Updated Successfuly"
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    "success": 
 *    "message":"User Not Found"
 * }
 */
router.post('/update-vendor', upload, controller.updateVendor);
/**
 * @api {post} /user/admin/shared/update-customer update customer info
 * @apiName update customer info
 * @apiGroup admin
 * @apiParam  {string} _id id for customer to update his data.
 * @apiParam  {string} [name]
 * @apiParam  {string} [username]
 * @apiParam  {string} [email] 
 * @apiParam  {string} [addresses] 
 * @apiParam  {number} [phoneNumbers] 
 * @apiSuccessExample Success-Response:
 *{
 *"success": true,
 * "message" :"Info Updated Successfuly"
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    "success": 
 *    "message":"User Not Found"
 * }
 */
router.post('/update-customer', controller.updateCustomer);

/**
 * @api {post} /user/admin/shared/login login 
 * @apiName Loginadmin
 * @apiGroup admin
 * @apiParam  {string} [email] One type of login methods.
 * @apiParam  {number} [phoneNumber] One type of login methods.
 * @apiParam  {string} password One type of login methods.
 * @apiSuccessExample Success-Response:
 *{
 *"success": true,
 *"token": "",
 *"user": {
 *"id": "",
 *"name": "",
 *"username": "",
 *"email": ""
 *   }
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": false,
 *    "message": "Wrong password!"
 * }
 */
router.post('/login', controller.login);
/**
 * @api {post} /user/admin/shared/forget-pass-code forget password
 * @apiName forgetPassword
 * @apiGroup admin
 * @apiParam  {string} [email] User's Email address.
 * @apiParam  {number} [phone] User's phone number.
 * @apiSuccessExample Success-Response:
 *{
 *"success": true
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": false
 * }
 */
router.post('/forget-pass-code', controller.sendForgetPasswordCode);
/**
 * @api {post} /user/admin/shared/verify-forget-pass-code admin verifing code for changing his password 
 * @apiName verifing password verification code
 * @apiGroup admin
 * @apiParam  {string} [email] One type of login methods.
 * @apiParam  {number} [phoneNumber] One type of login methods.
 * @apiParam  {number} passVerificationCode code sent to user on mail
 * @apiSuccessExample Success-Response:
 *{
 *"success": true
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": false
 * }
 */

router.post('/verify-forget-pass-code', controller.verifyForgetPasswordCode);
/**
 * @api {get} /user/admin/shared/reset-pass
 * @apiName user reset his password
 * @apiGroup admin
 * @apiParam  {string} password user old password.
 * @apiParam  {string} newPassword user new password.
 * @apiSuccessExample Success-Response:
 *{
 *        success: true,
 *        message: 'Your password reset successfuly'
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    success: false,
 *     message: 'wrong old password!'
 * }
 */
router.post('/reset-pass', controller.resetPassword);

/**
 * @api {post} /user/admin/shared/deactivate-vendor admin deactivate vendor
 * @apiName admindeactivatevendor
 * @apiGroup admin
 * @apiParam  {string} Id vendor Id that you want to deactivat his account .
 * @apiSuccessExample Success-Response:
 *{
 *        success: true,
 *        message: 'driver deacctivated successfuly'
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    success: false,
 *     message: 'can not save update'
 * }
 */
router.post('/deactivate-vendor', jwt({
    secret: process.env.secret
}), controller.deactivateVendor);

/**
 * @api {post} /user/admin/shared/activate-vendor admin activate vendor
 * @apiName adminactivatevendor
 * @apiGroup admin
 * @apiParam  {string} Id vendor Id that you want to activat his account .
 * @apiSuccessExample Success-Response:
 *{
 *        success: true,
 *        message: 'driver acctivated successfuly'
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    success: false,
 *     message: 'can not save update'
 * }
 */
router.post('/activate-vendor', jwt({
    secret: process.env.secret
}), controller.reactivateVendor);

/**
 * @api {post} /user/admin/shared/deactivate-customer admin deactivate customer
 * @apiName admin deactivate customer
 * @apiGroup admin
 * @apiParam  {string} customerId customer Id that you want to deactivat his account .
 * @apiSuccessExample Success-Response:
 *{
 *        success: true,
 *        message: 'driver deacctivated successfuly'
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    success: false,
 *     message: 'can not save update'
 * }
 */
router.post('/deactivate-customer', controller.deactivateCustomer);

/**
 * @api {post} /user/admin/shared/activate-customer admin activate customer
 * @apiName admin activate customer
 * @apiGroup admin
 * @apiParam  {string} customerId customer Id that you want to activat his account .
 * @apiSuccessExample Success-Response:
 *{
 *        success: true,
 *        message: 'driver acctivated successfuly'
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    success: false,
 *     message: 'can not save update'
 * }
 */
router.delete('/activate-customer', controller.reactivateCustomer);

/**
 * @api {delete} /user/admin/shared/remove-vendor admin remove vendor
 * @apiName admin remove vendor
 * @apiGroup admin
 * @apiParam  {string} vendorId customer Id that you want to remove his account .
 * @apiSuccessExample Success-Response:
 *{
 *        success: true,
 *        message: 'vendor account deleted successfully'
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    success: false,
 *     message: 'No vendor account for this ID'
 * }
 */
router.delete('/remove-vendor/:vendorId', controller.removeVendor);

/**
 * @api {delete} /user/admin/shared/remove-customer admin remove customer
 * @apiName admin remove customer
 * @apiGroup admin
 * @apiParam  {string} customerId customer Id that you want to remove his account .
 * @apiSuccessExample Success-Response:
 *{
 *        success: true,
 *        message: 'customer account deleted successfully'
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    success: false,
 *     message: 'No customer account for this ID'
 * }
 */
router.delete('/remove-customer/:customerId', controller.removeCustomer);

router.post('/user-history', controller.trackUserHistory);

router.post('/service-history', controller.trackServiceHistory);
module.exports = router;