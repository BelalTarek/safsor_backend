var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
//require controllers
var controller = require('../controllers');
const baseRoute = '/user/admin/super/';
// middleware to protect all except mentioned apis
router.use(jwt({
    secret: process.env.secret
}).unless({
    path: [baseRoute + 'register', baseRoute + 'login', baseRoute + 'forget-pass-code', baseRoute + 'verify-forget-pass-code']
}), function (req, res, next) {
    if (req.user) {
        console.log(req.user._doc.role);
        if (req.user._doc.role != 1) return res.json({
            sucess: false,
            message: "you are not an admin"
        });
        next();
    } else {
        next();
    }
});
//Routers
/**
 * @api {post} /user/admin/super/add-mod add new moderator
 * @apiName addModerator
 * @apiGroup SuperAdmin
 * @apiParam {string} name user name
 * @apiParam  {string} email user email address.
 * @apiParam  {number} phoneNumber user phone number.
 * @apiParam  {string} password user password.
 * @apiParam  {string} username username.
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *      message: 'admin registered successfully'
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *      message: 'admin faild to register'
 *   }
 */
router.post('/add-mod', controller.addModerator);
/**
 * @api {post} /user/admin/super/deactivate-mod deactivate moderator
 * @apiName deactivateModerator
 * @apiGroup SuperAdmin
 *  
 *  @apiParam  {string} _id modertator Id.
 * @apiSuccessExample Success-Response:
 *{
 *"success": account de-activate successfuly,
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": falied To de-activate,
 * }
 */
router.post('/deactivate-mod', controller.deactivateModerator);

/**
 * @api {post} /user/admin/super/activate-mod activate moderator
 * @apiName activateModerator
 * @apiGroup SuperAdmin
 *  
 *  @apiParam  {string} _id modertator Id.
 * @apiSuccessExample Success-Response:
 *{
 *"success": account de-activate successfuly,
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": falied To de-activate,
 * }
 */
router.post('/activate-mod', controller.activateModerator);

/**
 * @api {delete} /user/admin/super/remove-moderator/:moderatorId remove moderator
 * @apiName admin remove moderator
 * @apiGroup SuperAdmin
 * @apiParam  {string} moderatorId moderator Id that you want to remove his account .
 * @apiSuccessExample Success-Response:
 *{
 *        success: true,
 *        message: 'moderator account deleted successfully'
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    success: false,
 *     message: 'No moderator account for this ID'
 * }
 */
router.delete('/remove-moderator/:moderatorId', controller.removeModerator);

/**
 * @api {post} /user/admin/super/update-moderator update moderator info
 * @apiName update user info
 * @apiGroup SuperAdmin
 * @apiParam  {string} [name]
 * @apiParam  {string} [username]
 * @apiParam  {string} [email] 
 * @apiParam  {string} [addresses] 
 * @apiParam  {number} [phoneNumbers] 
 * @apiSuccessExample Success-Response:
 *{
 *"success": true,
 * "message" :"Info Updated Successfuly"
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    "success": 
 *    "message":"User Not Found"
 * }
 */
router.post('/update-moderator', controller.updateModerator);

module.exports = router;