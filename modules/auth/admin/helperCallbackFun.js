const admin = require('./adminModel');

module.exports.findOneByEmailOrPhone = function (userData, callback) {
    if (userData.phoneNumber) {
        admin.findOne({
            phoneNumber: userData.phoneNumber
        }, callback);
    } else if (userData.email) {
        admin.findOne({
            email: userData.email
        }, callback);
    }

};
module.exports.findOneById = function (userData, callback) {
    admin.findOne({
        _id: userData._id
    }, callback);
}
module.exports.reActivate = function (user, callback) {
    admin.findOneAndUpdate({
        _id: user._id,
    }, {
        $set: {
            active: true,
        }
    }, {
        new: true
    }, callback);
}