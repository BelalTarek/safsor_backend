const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
var request = require('request');
var rn = require('random-number');
const admin = require('./adminModel');
const vendor = require('../vendor/vendorModel');
const customer = require('../customer/customerModel');
const adminHelper = require('./helperCallbackFun');
const shared = require('../shared');
const bcryptjs = require('bcryptjs')
const requests = require('../../requests/model')
const service = require('../../services/model')
const push = require('../../pushNot/model');
var Rating = require('../../rating/model')
var Report = require('../../report/model') 
var PromoCode = require('../../promocodes/model')
var middlewares = require('../../middlewares')
var Invoice = require('../../invoice/model')
var AWS = require('aws-sdk');

AWS.config.region = 'us-east-2';
AWS.config.update({
      accessKeyId: process.env.AWS_Access_Key_ID,
      secretAccessKey: process.env.AWS_Secret_Access_Key,
}); 
var s3 = new AWS.S3();

var fs = require('fs');

var cloudinary = require('cloudinary');

cloudinary.config({
  cloud_name: process.env.cloudinary_cloud_name, 
  api_key: process.env.cloudinary_api_key, 
  api_secret: process.env.cloudinary_api_secret
});

exports.showModerators = function (req, res, next) {
  if (req.body.id) {
    admin.findOne({
      _id: req.body.id
    }, (err, moderator) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        res.json({
          success: true,
          list: moderator
        })
      }
    });
  } else {
    admin.find({
      role: 2
    }, (err, moderators) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        res.json({
          success: true,
          list: moderators
        })
      }
    });
  }
}
exports.showVendor = function (req, res, next) {
  if (req.body.id) {
    vendor.findOne({
      _id: req.body.id
    }, (err, vendor) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        res.json({
          success: true,
          list: vendor
        })
      }
    });
  } else {
    vendor.find({
       parentId: { $exists: false } 
    }, (err, vendors) => {
      if (err || !vendors) {
        res.json({
          success: false,
          message: err
        })
      } else {
        var vendorsJSON = JSON.parse(JSON.stringify(vendors))
        var vendorIds = [];
        for (var i = 0; i < vendors.length; i++) {
          vendorIds.push(vendors[i]._id)
        }
        Rating.find({
          userId: {
            $in : vendorIds
          }
        }, function(er, rates){
          if(er){
            return res.json({
              success: false,
              message: er
            })
          }
          // else if(rates.length <= 0){
          //   res.json({
          //     success: true,
          //     list: vendors
          //   })
          // }
          else{
            Report.find({
              driverId: {
                $in: vendorIds
              }
            }, function(e, reports){
              if(e){
                  return res.json({
                    success: false,
                    message: e
                  })
              }
              else{
                if(rates.length <= 0 && reports.length <= 0){
                    res.json({
                      success: true,
                      list: vendors
                    })
                }
                else{
                  // var result = [];
                  //console.log(reports.filter( report => report.driverId==vendors[0]._id))
                  var now = new Date();

                  var lastSunday = now.getDate() - now.getDay();

                  var lastSaturday;
                  var month;
                  if(lastSunday > 0){
                      lastSaturday = lastSunday - 1;
                      month = now.getMonth();

                  }
                  else{
                      var lastDayOfMonth = new Date(now.getFullYear(), now.getMonth(), 0);
                      lastSaturday = lastDayOfMonth.getDate() + lastSunday - 1;
                      month = now.getMonth() - 1;
                  }
                    //console.log(new Date(now.getFullYear(), now.getMonth(), 1))
                  var dateOfLastSaturday = new Date(now.getFullYear(), month, lastSaturday);
                  console.log(now, dateOfLastSaturday)
                  Invoice.find({
                      // driverId: vendors[i]._id,
                      date: { $gte: dateOfLastSaturday , $lte: now}
                  }, (ee, invoices) => {
                    if(ee){
                        return res.json({
                          success: false,
                          message: ee
                        })
                    }
                    else{
                       console.log("inv", invoices)
                        for (var i = 0; i < vendors.length; i++) {

                          var vendorReports = reports.filter( report => report.driverId==vendors[i]._id);
                          var vendorInvoices = invoices.filter( invoice => invoice.driverId==vendors[i]._id)
                          var totalEnqazPercentage = 0;
                          var cash = 0;
                          var credit = 0;
                          for (var j = 0; j < vendorInvoices.length; j++) {
                            totalEnqazPercentage += vendorInvoices[j].enqazPercentage
                            if(vendorInvoices[j].paymentType == "Cash" || vendorInvoices[j].paymentType == "cash"){
                              cash += vendorInvoices[j].finalCost
                            }
                            else{
                              credit += vendorInvoices[j].finalCost
                            }
                          }
                          var setOff = totalEnqazPercentage - credit
                          console.log(setOff)
                          vendorsJSON[i]["setOffAmount"] = setOff > 0 ? setOff : 0.0
                         // console.log(i)
                         // console.log(vendors[i])
                          if(vendorReports.length <= 0){
                            var rate = rates.filter( rate => rate.userId==vendors[i]._id)
                            vendorsJSON[i]["rating"] = rate.length > 0 ? Number.parseFloat(rate[0].rating).toFixed(2) : 5.0;
                            vendorsJSON[i].rejectionRate = 0.0;
                            // var vendorElement = {
                            //   vendor: vendors[i],
                            //   rate: rate[0]//,
                            //   //rejectionRate: (rejection / totalRecievedReq) * 100
                            // }
                            // result.push(vendorElement)
                          }
                          else{
                            var rate = rates.filter( rate => rate.userId==vendors[i]._id)
                            //console.log(vendors[i])
                            vendorsJSON[i]["rating"] = rate.length > 0 ? Number.parseFloat(rate[0].rating).toFixed(2) : 5.0;
                            var totalRecievedReq = 0;
                            var totalAcceptedReq = 0;
                            for (var j = 0; j < vendorReports.length; j++) {
                              //reports[i]
                              totalRecievedReq += vendorReports[j].connectionHistory.numOfReqRecieved;
                              totalAcceptedReq += vendorReports[j].connectionHistory.numOfReqAccepted;
                              //console.log('rec', vendorReports[j].connectionHistory.numOfReqRecieved, 'acc',vendorReports[j].connectionHistory.numOfReqAccepted)
                            }
                            var rejection = totalRecievedReq - totalAcceptedReq;
                            //console.log('rec', totalRecievedReq)
                            if(totalRecievedReq == 0){
                              vendorsJSON[i]["rejectionRate"] = 0.0
                            }
                            else{
                              vendorsJSON[i]["rejectionRate"] = parseInt((rejection / totalRecievedReq) * 100)
                            }
                            

                            // var vendorElement = {
                            //   vendor: vendors[i],
                            //   rate: rate[0],
                            //   rejectionRate: (rejection / totalRecievedReq) * 100
                            // }
                            // result.push(vendorElement)
                          }

                   //  console.log(i)
                   // console.log(vendors[i].rejectionRate)
                    //console.log(vendorReports)
                    
                    //vendors[i]
                    //console.log(result)
                    
                  }
                  // console.log("h ",vendorsJSON[0])
                  res.json({
                     success: true,
                    //list: result,
                     list: vendorsJSON
                  })
                      }
                      // var totalEnqazPercentage = 0;
                      // var cash = 0;
                      // var credit = 0;
                      // for (var j = 0; j < invoices.length; j++) {
                      //   totalEnqazPercentage += invoices[j].enqazPercentage
                      //   if(invoices[j].paymentType == "Cash" || invoices[j].paymentType == "cash"){
                      //     cash += invoices[j].finalCost
                      //   }
                      //   else{
                      //     credit += invoices[j].finalCost
                      //   }
                      // }
                      // var setOff = totalEnqazPercentage - credit
                      // // vendorsJSON[i]["setOffAmount"] = setOff > 0 ? setOff : 0.0 

                      // console.log(vendors[i], vendorsJSON[i], i, vendors.length)

                      
                    })
                  
                  //vendors.forEach(element => console.log(element.rating))
                  // for (const prop in vendors) {
                  //   if (vendors.hasOwnProperty(prop)) {
                  //     console.log(`${prop}: ${vendors[prop]}`)
                  //   }
                  // }

                  // convertedJSON[0].rating = 4.05
                  
                   
                }
                
              }
            })
            // var result = [];
            // for (var i = 0; i < vendors.length; i++) {
              // Report.find({driverId: vendors[i]._id}, function(e, reports){
              //   if(e){
              //     return res.json({
              //       success: false,
              //       message: e
              //     })
              //   }
              //   else if(reports.length > 0){
              //     var totalRecievedReq = 0;
              //     var totalAcceptedReq = 0;
              //     for (var i = 0; i < reports.length; i++) {
              //       //reports[i]
              //       totalRecievedReq += reports[i].connectionHistory.numOfReqRecieved;
              //       totalAcceptedReq += reports[i].connectionHistory.numOfReqAccepted;
              //     }
              //     var rejection = totalRecievedReq - totalAcceptedReq;
              //     var vendorElement = {
              //       vendor: vendors[i],
              //       rate: rates.filter( rate => rate.userId==vendors[i]._id),
              //       rejectionRate: (rejection / totalRecievedReq) * 100
              //     }
              //     result.push(vendorElement)
              //   }
              // })
            //   var vendorElement = {
            //     vendor: vendors[i],
            //     rate: rates.filter( rate => rate.userId==vendors[i]._id)
            //   }
            //   result.push(vendorElement)
            //   //vendors[i]
            // }
            // console.log(result)
            // res.json({
            //   success: true,
            //   list: result
            // })
          }
        })
        // var vendorsList = [];
        // vendors.forEach(element => {
        //   Rating.findOne({userId: element._id}, function(er, rate){
        //     if(er){
        //       return res.json({
        //         success: false,
        //         message: er
        //       })
        //     }
        //     else{
        //       var vendorElement = {
        //         vendor: element,
        //         rating: rate ? rate.rating : 0.0
        //       }
        //       vendorsList.push(vendorElement)
        //     }
        //   })
        //   console.log(vendorsList)
        // });
        
        // res.json({
        //   success: true,
        //   list: vendors
        // })
      }
    });
  }
}
exports.showCustomer = function (req, res, next) {
  if (req.body.id) {
    customer.find({
      _id: req.body.id
    }, (err, customer) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        res.json({
          success: true,
          list: customer
        })
      }
    });
  } else {
    customer.find({}, (err, customers) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        res.json({
          success: true,
          list: customers
        })
      }
    });
  }
}
//super admin 
// add moderator 
exports.addModerator = function (req, res, next) {
  let userData = req.body;
  // query users collection to check if phonenumbers already registered
  let findRegisteredUsers = (usersPhoneNumb) => {
    admin.find({
      phoneNumber: {
        $in: usersPhoneNumb
      }
    }, (err, users) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        saveBulk(users, usersPhoneNumb);
      }
    });
  }
  // return phone numbers if found any registered or save bulk of users 
  let saveBulk = (usersFound, usersPhoneNumb) => {
    //array to save phoneNumber of all users in the array
    let phoneNum = [];
    for (var i = 0; i < userData.length; i++) {
      // if there is no users found already registered, hash passwords
      if (usersFound.length < 1) {
        let each = userData[i];
        if (i === (userData.length)) {
          break;
        } else {
          // hashing password in every user object in the array
          var salt = bcryptjs.genSaltSync(10);
          var hash = bcryptjs.hashSync(userData[i].password, salt);
          each.password = hash;
          userData[i] = each;
        }
      }
    }
    if (usersFound.length < 1) {
      admin.insertMany(userData, function (err, users) {
        if (err) {
          res.json({
            success: false,
            message: err
          })
        } else {
          res.json({
            success: true,
            users: users
          })
        }
      });
    } else {
      res.json({
        success: false,
        message: "following phone numbers found already registered " + usersPhoneNumb,
      });
    }
  }
  // array has objects or empty?
  if (userData.length > 0) {
    var usersPhoneNumb = [];
    // loop on the bulk of users to be added
    for (var i = 0; i < userData.length; i++) {
      // check all attr. are there 
      if (userData[i].name && userData[i].email && userData[i].username && userData[i].phoneNumber && userData[i].password) {
        usersPhoneNumb.push(userData[i].phoneNumber)
        if (i === (userData.length)) {
          break;
        }
      } else {
        res.json({
          success: false,
          message: 'all attributes are needed'
        })
      };
    }
    // let's check if this phone numbers associated already to registered accounts
    findRegisteredUsers(usersPhoneNumb);
  } else {
    res.json({
      success: true,
      message: 'it must be an array with at least one object'
    })
  }
};

// add vendors 
exports.addVendor = function (req, res, next) {
  let userData = req.body;
  // query users collection to check if phonenumbers already registered
  let findRegisteredUsers = (usersPhoneNumb) => {
    // console.log("h6")
    vendor.find({
      phoneNumber: {
        $in: usersPhoneNumb
      }
    }, (err, users) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {

        saveBulk(users, usersPhoneNumb);
      }
    });
  }
  // return phone numbers if found any registered or save bulk of users 
  let saveBulk = (usersFound, usersPhoneNumb) => {
    // console.log("h7")
    //array to save phoneNumber of all users in the array
    let phoneNum = [];
    let answer = function (usersFound) {
       //console.log("h8.5")
      if (usersFound.length < 1) {
         console.log("h9")
        if(req.files){

          console.log('file-- ' + req.files)

          // fs.readFile(req.files[0].path, (err, data) => {
            
          //   if(err) console.log(err)
          //   console.log(data)
          //   var params = {
          //     Bucket: 'enqazbackend',
          //     Key: req.files[0].originalname, 
          //     Body: data,
          //     ACL:'public-read'
          //   };
          //   s3.upload(params, function(error,result){
          //   //cloudinary.v2.uploader.upload(req.files[0].path, function(error, result) {
          //       console.log(result)
          //       //console.log(error)
          //       if(error || !result){
          //         return res.json({
          //           success: false,
          //           message: error || "Failed to upload image"
          //         });
          //       }
          //       else{
          //         userData.imageURL = result.Location;
          //         // if(userArr[0].isVendor == false){
          //         //   userArr[0].role = 4;
          //         // }
                  
          //         //console.log(userArr[0].role)

          //         vendor.insertMany(userData, function (err, users) {
          //           // console.log("h10", err, users);
          //           if (err) {
          //             res.json({
          //               success: false,
          //               message: err
          //             })
          //           } else {
          //             res.json({
          //               success: true,
          //               users: users
          //             })
          //           }
          //         });
          //       }
          //   })
          // })

          //console.log('file-- ' + req.files)
          console.log("files", req.files, "body", req.body)
          // res.json({
          //   success: true,
          //   files: req.files
          // })
          var files = req.files;
          if(files.imageURL && files.idURL && files.licenseURL){
            shared.uploadToS3(files.imageURL[0].path, files.imageURL[0].originalname, (error, data) => {
              if(error || data == null){
                res.json({
                  success: false,
                  message: "Failed to upload the image"
                })
              }
              else{
                shared.uploadToS3(files.idURL[0].path, files.idURL[0].originalname, (err, data2) => {
                  if(err || data2 == null){
                    res.json({
                      success: false,
                      message: "Failed to upload the ID"
                    })
                  }
                  else{
                    shared.uploadToS3(files.licenseURL[0].path, files.licenseURL[0].originalname, (er, data3) => {
                      if(er || data3 == null){
                        res.json({
                          success: false,
                          message: "Failed to upload the license"
                        })
                      }
                      else{
                        userData.imageURL = data.Location;
                        userData.idURL = data2.Location
                        userData.licenseURL = data3.Location
  
                        vendor.insertMany(userData, function (err, users) {
                                    // console.log("h10", err, users);
                          if (err) {
                            res.json({
                              success: false,
                              message: err
                            })
                          } else {
                              res.json({
                                success: true,
                                users: users,
                                files: req.files
                              })
                          }
                        });
                      }
                    })
                  }
                })
              }
            })
          }
          else{
            res.json(middlewares.checkAllFieldsExist)
          }
          
          

        }
        else{
        

              vendor.insertMany(userData, function (err, users) {
                // console.log("h10", err, users);
                if (err) {
                  res.json({
                    success: false,
                    message: err
                  })
                } else {
                  res.json({
                    success: true,
                    users: users
                  })
                }
              });
      //       }
      //   })
      // })
        }
        
      } else {
        res.json({
          success: false,
          message: "following phone numbers found already registered " + usersPhoneNumb,
        });
      }
    }

    if (usersFound.length < 1) {
      if(!Array.isArray(userData)){
        console.log('m4 array save bulk')
        let each = userData
        var salt = bcryptjs.genSaltSync(10);
        var hash = bcryptjs.hashSync(userData.password, salt);
        each.password = hash;
        console.log("hasheeeed..", each)
        userData = each;
        if(userData.isVendor == false || userData.isVendor == 'false'){
          userData.role = 4;
 
 
          console.log('role-- '+userData.role)
 
        }
        // if(userData[i].isInsuranceCompany == true){
        //   userData[i].role = 6;
        //   if(userData[i].promoCode && userData[i].expiresAt && userData[i].startDate 
        //      && userData[i].discountRate){
        //       console.log(userData[i])
        //          var promo = new PromoCode({
        //                code: userData[i].promoCode,
        //                expiresAt: userData[i].expiresAt,
        //                startDate: userData[i].startDate,
        //                discountRate: userData[i].discountRate
        //          })
        //          promo.save((e, result) => {
        //            if(e || !result){
        //              return res.json({
        //                success: false,
        //                message: e || "Failed to add promo code"
        //              })
        //            }
        //          })
        //      }
        //      else{
        //        return res.json(middlewares.checkAllFieldsExist)
        //      }
        //   }
 
        answer(usersFound);
      }
      else{
        for (var i = 0; i < userData.length; i++) {
          //console.log("h8")
         // if there is no users found already registered, hash passwords
         let each = userData[i];
         // console.log("h12", userData.length)
 
         if (i === userData.length) {
           // console.log("h11")
           answer(usersFound);
         } else if (userData.length === 1) {
           var salt = bcryptjs.genSaltSync(10);
           var hash = bcryptjs.hashSync(userData[i].password, salt);
           each.password = hash;
           console.log("hasheeeed..", each)
           userData[i] = each;
           if(userData[i].isVendor == false ){
             userData[i].role = 4;
 
 
             console.log('role-- '+userData[i].role)
 
           }
           if(userData[i].isInsuranceCompany == true){
             userData[i].role = 6;
             if(userData[i].promoCode && userData[i].expiresAt && userData[i].startDate 
               && userData[i].discountRate && userData[i].numOfUsers){
               console.log(userData[i])
                 var promo = new PromoCode({
                       code: userData[i].promoCode,
                       expiresAt: userData[i].expiresAt,
                       startDate: userData[i].startDate,
                       discountRate: userData[i].discountRate
                 })
                 promo.save((e, result) => {
                   if(e || !result){
                     return res.json({
                       success: false,
                       message: e || "Failed to add promo code"
                     })
                   }
                 })
             }
             else{
               return res.json(middlewares.checkAllFieldsExist)
             }
           }
 
           answer(usersFound);
 
         } else {
           // hashing password in every user object in the array
           var salt = bcryptjs.genSaltSync(10);
           var hash = bcryptjs.hashSync(userData[i].password, salt);
           each.password = hash;
           console.log("hasheeeed", each)
           userData[i] = each;
           // userData[i].isVendor = true;
         }
       }
      }
      
    } else {
      res.json({
        success: false,
        msg: usersFound[0].phoneNumber + " is already associated to another account"
      })
    }

  }
  var userArr = [];
  userArr.push(userData)
  
  console.log(userData, !Array.isArray(userData))
  //var form = new IncomingForm(userData[0])
  // array has objects or empty?
  if(!Array.isArray(userData)){
    console.log("m4 array")
    var usersPhoneNumb = [];
    if (userData.name && userData.email && userData.username && userData.phoneNumber && userData.password /*&& userData[i].services && userData[i].enqazPercentage && userData[i].setOfDate*/) {
      usersPhoneNumb.push(userData.phoneNumber);
      console.log('1', usersPhoneNumb)
      findRegisteredUsers(usersPhoneNumb);
    }
    else {
      res.json({
        success: false,
        message: 'all attributes are needed',
        user: userData,
        data: userData[0].toString()
      })
    }
  }
  else if (userData.length > 0) {
    // console.log("h1")
    var usersPhoneNumb = [];
    // loop on the bulk of users to be added
    for (var i = 0; i < userData.length; i++) {
      // check all attr. are there 
      if (userData[i].name && userData[i].email && userData[i].username && userData[i].phoneNumber && userData[i].password /*&& userData[i].services && userData[i].enqazPercentage && userData[i].setOfDate*/) {
        // console.log("h2")

        if (i === (userData.length)) {
          // console.log("h3")
          // let's check if this phone numbers associated already to registered accounts
          findRegisteredUsers(usersPhoneNumb);
        } else if (userData.length === 1) {

          //console.log("h4")
          usersPhoneNumb.push(userData[i].phoneNumber);
          findRegisteredUsers(usersPhoneNumb);
        } else {
          // console.log("5")
          usersPhoneNumb.push(userData[i].phoneNumber);
        }
      } else {
        res.json({
          success: false,
          message: 'all attributes are needed',
          user: userData,
          data: userData[0].toString()
        })
      };
    }
  } else {
    res.json({
      success: true,
      message: 'it must be an array with at least one object',
     
    })
  }
};
// add customer
exports.addCustomer = function (req, res, next) {
  let userData = req.body;
  // query users collection to check if phonenumbers already registered
  let findRegisteredUsers = (usersPhoneNumb) => {
    customer.find({
      phoneNumber: {
        $in: usersPhoneNumb
      }
    }, (err, users) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        saveBulk(users, usersPhoneNumb);
      }
    });
  }
  // return phone numbers if found any registered or save bulk of users 
  let saveBulk = (usersFound, usersPhoneNumb) => {
    //array to save phoneNumber of all users in the array
    let phoneNum = [];
    for (var i = 0; i < userData.length; i++) {
      // if there is no users found already registered, hash passwords
      if (usersFound.length < 1) {
        let each = userData[i];
        if (i === (userData.length)) {
          break;
        } else {
          // hashing password in every user object in the array
          var salt = bcryptjs.genSaltSync(10);
          var hash = bcryptjs.hashSync(userData[i].password, salt);
          each.password = hash;
          userData[i] = each;
        }
      }
    }
    if (usersFound.length < 1) {
      customer.insertMany(userData, function (err, users) {
        if (err) {
          res.json({
            success: false,
            message: err
          })
        } else {
          res.json({
            success: true,
            users: users
          })
        }
      });
    } else {
      res.json({
        success: false,
        message: "following phone numbers found already registered " + usersPhoneNumb,
      });
    }
  }
  // array has objects or empty?
  if (userData.length > 0) {
    var usersPhoneNumb = [];
    // loop on the bulk of users to be added
    for (var i = 0; i < userData.length; i++) {
      // check all attr. are there 
      if (userData[i].name && userData[i].email && userData[i].username && userData[i].phoneNumber && userData[i].password) {
        usersPhoneNumb.push(userData[i].phoneNumber)
        if (i === (userData.length)) {
          break;
        }
      } else {
        res.json({
          success: false,
          message: 'all attributes are needed'
        })
      };
    }
    // let's check if this phone numbers associated already to registered accounts
    findRegisteredUsers(usersPhoneNumb);
  } else {
    res.json({
      success: true,
      message: 'it must be an array with at least one object'
    })
  }
};

// Login api for admin
exports.login = function (req, res, next) {
  let userData = req.body;
  if (userData.email && userData.password || userData.phoneNumber && userData.password) {
    // add attr. "findWith" to let the callback fun. know the type of credentials
    adminHelper.findOneByEmailOrPhone(userData, (err, user) => {
      if (user) {
        shared.checkPassword(userData.password, user, (error, token) => {
          // error should be true if password is wrong
          if (error) {
            res.json({
              success: false,
              message: "incorrect password"
            });
          } else { 
            push.findOneAndUpdate({
                userId: user._id
              }, {
                $set: {
                  fcmToken: req.body.fcmToken
                }
              }, {
                new: true
              }, function (err, doc) {
                if (!doc) {
                  let newPushObj = {
                    userId: user._id,
                    fcmToken: req.body.fcmToken
                  }
                  push.create(newPushObj, (err, done) => {
                    if (done) {
                      userLoggedIn(token, user);
                    }
                  })
                }else{
                  userLoggedIn(token, user);
                }
          });
            }
        });
      } else if (!user) {
        res.json({
          success: false,
          message: "user not found"
        });
      } else if (err) {
        res.json({
          success: false,
          message: err
        });
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
  // if user can login successfully we will call this function 
  let userLoggedIn = (token, user) => {
    return res.json({
      success: true,
      token: 'Bearer ' + token,
      user: {
        id: user._id,
        name: user.name,
        username: user.username,
        email: user.email,
        role: user.role
      }
    });
  }
};
// api to send a mail to your email address with a code 
// which you will enter in the mobile app so you can change password
exports.sendForgetPasswordCode = function (req, res, next) {
  let userData = req.body;
  if (userData.email || userData.phone) {
    adminHelper.findOneByEmailOrPhone(userData, (err, userFound) => {
      if (!userFound) {
        res.json({
          success: false,
          message: "user email address doesn't exist"
        });
      } else {
        let smtpTrans = nodemailer.createTransport({
          service: 'SendGrid',
          auth: {
            user: 'bishoymelek',
            pass: 'admin123Admin'
          }
        });
        var gen = rn.generator({
          min: -900000,
          max: 900000,
          integer: true
        });
        generatedNum = gen(100000);
        userFound.passVerificationCode = generatedNum;
        userFound.save((err) => {
          if (err) {
            res.json({
              success: false,
              message: err
            });
          } else {
            var mailOptions = {
              to: req.body.email,
              from: 'noreply@enqaz.com',
              subject: 'Password Reset - enqaz',
              text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                'Please use the following code: ' + generatedNum + '\n\n' +
                ' If you did not request this, please ignore this email and your password will remain unchanged.\n'
            };
            smtpTrans.sendMail(mailOptions, function (err, sent) {
              if (err) res.json({
                success: false,
                message: err
              });
              res.json({
                success: true,
                message: generatedNum
              });
            });
          }
        });
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
};
// enter old and new password to renew it
exports.resetPassword = function (req, res, next) {
  let userData = req.body;
  if (userData.password && userData.newPassword) {
    userData.findWith = "email"
    adminHelper.findOneByEmailOrPhone({
      email: req.user._doc.email
    }, (err, userFound) => {
      if (userFound) {
        shared.checkPassword(userData.password, userFound.password, function (err, isMatch) {
          if (err) {
            res.json({
              success: false,
              message: 'wrong password!'
            });
          } else {
            shared.hashPassword(userData.newPassword, (err, hashedPassword) => {
              userFound.password = hashedPassword;
              userFound.save();
            });
            res.json({
              success: true,
              message: 'Your password reset successfuly'
            });
          }
        });
      } else {
        res.json({
          success: false,
          message: err
        })
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
};
// check that code entered by user is correct
exports.verifyForgetPasswordCode = function (req, res, next) {
  let reqPassVerCode = Number(req.body.passVerificationCode);
  if (reqPassVerCode) {
    admin.findOne({
      passVerificationCode: reqPassVerCode
    }, (err, userFound) => {
      if (!userFound) {
        res.json({
          success: false,
          message: "user details doesn't exist"
        });
      } else if (userFound && userFound.passVerificationCode) {
        if (reqPassVerCode === userFound.passVerificationCode) {
          shared.generateToken(userFound, (error, token) => {
            userFound.token = token;
            userFound.save((err, done) => {
              if (err) {
                res.json({
                  success: false,
                });
              } else {
                res.json({
                  success: true,
                  token: token
                });
              }

            })
          });
        } else {
          userFound.passVerificationCouserFound.passVerificationCodede
          res.json({
            success: false,
            message: "don't match"
          });
        }
      } else if (err) {
        res.json({
          success: false,
          message: err
        });
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
};

exports.deactivateModerator = function (req, res, next) {
  adminHelper.findOneById({
    _id: req.body._id,
  }, (err, userFound) => {
    if (!userFound) {
      res.json({
        success: false,
        message: "user details doesn't exist"
      });
    } else if (userFound) {
      userFound.active = false;
      userFound.save((err, user) => {
        if (!user) {
          res.json({
            success: false,
            message: err
          });
        } else {
          res.json({
            success: true,
            message: "user deactiavted successfully"
          });
        }
      });
    };
  });
}
exports.activateModerator = function (req, res, next) {
  adminHelper.findOneById({
    _id: req.body._id,
  }, (err, userFound) => {
    if (!userFound) {
      res.json({
        success: false,
        message: "user details doesn't exist"
      });
    } else if (userFound) {
      userFound.active = true;
      userFound.save((err, user) => {
        if (!user) {
          res.json({
            success: false,
            message: err
          });
        } else {
          res.json({
            success: true,
            message: "user deactiavted successfully"
          });
        }
      });
    };
  });
}

exports.deactivateVendor = function (req, res, next) {
  let reqBody = req.body;
  //let userInfo = req.user;
  vendor.findOne({
    _id: reqBody.Id,
  }, function (err, doc) {
    if (!doc) {
      res.json({
        success: false,
        message: 'No Deiver with this Id'
      })
    } else {
      if (doc.adminActivation == false) {
        res.json({
          success: false,
          message: 'driver is already not active'
        })
      }else{
      doc.adminActivation = false;
      doc.save((err, vendor) => {
        if (!vendor) {
          res.json({
            success: false,
            message: 'can not save update'
          })
        } else {
          res.json({
            success: true,
            message: 'driver deactivated successfuly'
          })
        }
      })
    }
    }
  })
}

exports.reactivateVendor = function (req, res, next) {
  let reqBody = req.body;
  //let userInfo = req.user;
  vendor.findOne({
    _id: reqBody.Id
    //parentId: null
  }, function (err, doc) {
    if (!doc) {
      res.json({
        success: false,
        message: 'falied To active'
      })
    } else {
      if (doc.adminActivation == true) {
        res.json({
          success: false,
          message: 'driver is already active'
        })
      }else{
      doc.adminActivation = true;
      doc.save((err, vendor) => {
        if (!vendor) {
          res.json({
            success: false,
            message: 'can not save update'
          })
        } else {
          res.json({
            success: true,
            message: 'driver Activated successfuly'
          })
        }
      })
    }
    }
  })
}

exports.deactivateCustomer = function (req, res, next) {
  let reqBody = req.body;
  let userInfo = req.user;
  customer.findOne({
    _id: reqBody.customerId,
  }, function (err, doc) {
    if (!doc) {
      res.json({
        success: false,
        message: 'No customer with this Id'
      })
    } else {
      if (doc.isActive == false) {
        res.json({
          success: false,
          message: 'customer is already not active'
        })
      }
      doc.isActive = false;
      doc.save((err, vendor) => {
        if (!vendor) {
          res.json({
            success: false,
            message: 'can not save update'
          })
        } else {
          res.json({
            success: true,
            message: 'customer deactivated successfuly'
          })
        }
      })
    }
  })
}

exports.reactivateCustomer = function (req, res, next) {
  let reqBody = req.body;
  let userInfo = req.user;
  customer.findOne({
    _id: reqBody.customerId,

  }, function (err, doc) {
    if (!doc) {
      res.json({
        success: false,
        message: 'falied To cancel'
      })
    } else {
      if (doc.isActive == true) {
        res.json({
          success: false,
          message: 'customer is already active'
        })
      }
      doc.isActive = true;
      doc.save((err, vendor) => {
        if (!vendor) {
          res.json({
            success: false,
            message: 'can not save update'
          })
        } else {
          res.json({
            success: true,
            message: 'customer Activated successfuly'
          })
        }
      })
    }
  })
}

exports.updateModerator = function (req, res, next) {
  let reqBody = req.body;
  if (!reqBody) {
    res.json(middlewares.checkAllFieldsExist)
  }
  admin.findOne({
    _id: reqBody._id
  }, (err, userFound) => {
    if (!userFound) {
      res.json({
        success: false,
        message: "user details doesn't exist"
      });
    } else if (userFound) {
      if (reqBody.name) {
        userFound.name = reqBody.name
      }
      if (reqBody.username) {
        userFound.username = reqBody.username
      }
      if (reqBody.email) {
        userFound.email = reqBody.email
      }
      if (reqBody.addresses) {
        userFound.addresses = reqBody.addresses
      }
      if (reqBody.phoneNumber) {
        userFound.phoneNumber = reqBody.phoneNumber
      }
      userFound.save((err, user) => {
        if (!user) {
          res.json({
            success: false,
            message: err
          });
        } else {
          res.json({
            success: true,
            message: "Info Updated Successfuly"
          });
        }
      })
    }
  });
};

exports.updateVendor = function (req, res, next) {
  let reqBody = req.body;
  let files = req.files;
  if (!reqBody) {
    res.json(middlewares.checkAllFieldsExist)
  }
  console.log(reqBody)
  vendor.findOne({
    _id: reqBody._id
  }, (err, userFound) => {
    if (!userFound) {
      res.json({
        success: false,
        message: "user details doesn't exist"
      });
    } else if (userFound) {
      if (reqBody.name) {
        userFound.name = reqBody.name
      }
      if (reqBody.username) {
        userFound.username = reqBody.username
      }
      if (reqBody.email) {
        userFound.email = reqBody.email
      }
      if (reqBody.addresses) {
        userFound.addresses = reqBody.addresses
      }
      if (reqBody.phoneNumber) {
        userFound.phoneNumber = reqBody.phoneNumber
      }
      if (reqBody.services) {
        userFound.services = reqBody.services
      }
      if (reqBody.packages) {
        userFound.packages = reqBody.packages
      }
      if (reqBody.setOfDate) {
        userFound.setOfDate = reqBody.setOfDate
      }
      if (reqBody.password) {
        shared.hashPassword(reqBody.password, (err, hashedPassword) => {
          userFound.password = hashedPassword;
          userFound.save();
        });
      }
      if(files){
        console.log("files", req.files)
        if(files.imageURL){
          //userFound.imageURL = reqBody.imageURL
          shared.uploadToS3(files.imageURL[0].path, files.imageURL[0].originalname, (err, data) => {
            if(err || data == null){
              res.json({
                success: false,
                message: "Failed to upload the image"
              })
            }
            else{
              userFound.imageURL = data.Location
              userFound.save()
            }
          })
        }
        if(files.idURL){
          //userFound.idURL = reqBody.idURL
          shared.uploadToS3(files.idURL[0].path, files.idURL[0].originalname, (err, data) => {
            if(err || data == null){
              res.json({
                success: false,
                message: "Failed to upload the ID"
              })
            }
            else{
              userFound.idURL = data.Location
              userFound.save()
            }
          })
        }
        if(files.licenseURL){
          //userFound.licenseURL = reqBody.licenseURL
          shared.uploadToS3(files.licenseURL[0].path, files.licenseURL[0].originalname, (err, data) => {
            if(err || data == null){
              res.json({
                success: false,
                message: "Failed to upload the license"
              })
            }
            else{
              userFound.licenseURL = data.Location
              userFound.save()
            }
          })
        }
      }
      userFound.save((err, user) => {
        if (!user) {
          res.json({
            success: false,
            message: err
          });
        } else {
          res.json({
            success: true,
            message: "Info Updated Successfuly"
          });
        }
      })
    }
  });
};

exports.updateCustomer = function (req, res, next) {
  let reqBody = req.body;
  if (!reqBody) {
    res.json(middlewares.checkAllFieldsExist)
  }
  customer.findOne({
    _id: reqBody._id
  }, (err, userFound) => {
    if (!userFound) {
      res.json({
        success: false,
        message: "user details doesn't exist"
      });
    } else if (userFound) {
      if (reqBody.name) {
        userFound.name = reqBody.name
      }
      if (reqBody.username) {
        userFound.username = reqBody.username
      }
      if (reqBody.email) {
        userFound.email = reqBody.email
      }
      if (reqBody.addresses) {
        userFound.addresses = reqBody.addresses
      }
      if (reqBody.phoneNumber) {
        userFound.phoneNumber = reqBody.phoneNumber
      }
      userFound.save((err, user) => {
        if (!user) {
          res.json({
            success: false,
            message: err
          });
        } else {
          res.json({
            success: true,
            message: "Info Updated Successfuly"
          });
        }
      })
    }
  });
};
exports.removeVendor = function (req, res, next) {
  let reqBody = req.params;
  if (reqBody.vendorId) {
    vendor.remove({
      _id: reqBody.vendorId
    }, (err) => {
      if (err) {
        res.json({
          success: false,
          message: 'No bussnies account for this ID'
        })
      } else {
        res.json({
          success: true,
          message: 'bussines account deleted successfully'
        })
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
}
exports.removeCustomer = function (req, res, next) {
  let reqBody = req.params;
  if (reqBody.customerId) {
    customer.remove({
      _id: reqBody.customerId
    }, (err) => {
      if (err) {
        res.json({
          success: false,
          message: 'No customer account for this ID'
        })
      } else {
        res.json({
          success: true,
          message: 'customer account deleted successfully'
        })
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
}

exports.removeModerator = function (req, res, next) {
  let reqBody = req.params;
  if (reqBody.moderatorId) {
    admin.remove({
      _id: reqBody.moderatorId
    }, (err) => {
      if (err) {
        res.json({
          success: false,
          message: 'No moderator account for this ID'
        })
      } else {
        res.json({
          success: true,
          message: 'moderator account deleted successfully'
        })
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
}

exports.trackUserHistory = function (req, res, next) {
  requests.find({
    userId: req.body.id
  }, (err, requests) => {
    if (err) {
      res.json({
        success: false,
        message: 'no requests for this user'
      })
    } else {
      res.json({
        success: true,
        list: requests
      })
    }
  });
}

exports.trackServiceHistory = function (req, res, next) {
  requests.find({
    serviceId: req.body.serviceId
  }, (err, requests) => {
    if (err) {
      res.json({
        success: false,
        message: 'no request for this service'
      })
    } else {
      res.json({
        success: true,
        list: requests
      })
    }
  });
}