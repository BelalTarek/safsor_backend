const mongoose = require('mongoose')
const bcryptjs = require('bcryptjs')
const adminSchema = mongoose.Schema({
    name: {
        type: String,
        required: false
    },
    photo: {
        type: String,
        required: false
    },
    email: {
        type: String,
        unique: true,
        required: true,
    },
    phoneNumber: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true
    },
    passVerificationCode: {
        type: Number,
    },
    resetPasswordToken: {
        type: String,
    },

    addresses: {
        type: [String]
    },
    fbAccountId: {
        type: String,
        required: false,
        unique: false
        // unique: true,
    },
    // superAdmin : 1
    // moderator : 2  
    role: {
        type: Number,
        default: 2,
        required: true
    },
    active: {
        type: Boolean,
        default: true
    }
});
const admin = module.exports = mongoose.model('admin', adminSchema)
// Save a new user
module.exports.addUser = function (newUser, callback) {
    bcryptjs.genSalt(10, (err, salt) => {
        bcryptjs.hash(newUser.password, salt, (err, hash) => {
            if (err) callback(err);
            newUser.password = hash
            newUser.save(callback)
        });
    });
}