const bcryptjs = require('bcryptjs')
const jwt = require('jsonwebtoken');
var AWS = require('aws-sdk');

AWS.config.region = 'us-east-2';
AWS.config.update({
      accessKeyId: process.env.AWS_Access_Key_ID,
      secretAccessKey: process.env.AWS_Secret_Access_Key,
}); 
var s3 = new AWS.S3();

var fs = require('fs');

// Check password
module.exports.checkPassword = function (input, userObj, callback) {
    bcryptjs.compare(input, userObj.password, (err, isMatch) => {
        if (!isMatch) callback(true, null);
        else {
            const token = jwt.sign(userObj, "b1OFyhEJyZfdsfWxh4WKoLzb", {
                expiresIn: 29030400 // 1 week
            })
            callback(null, token);
        }
    });
}

module.exports.hashPassword = function (password, callback) {
    bcryptjs.genSalt(10, (err, salt) => {
        bcryptjs.hash(password, salt, (err, hash) => {
            if (err) callback(err);
            password = hash
            callback(null, password);
        })
    })
}

module.exports.generateToken = function (userObj,callback) {
    const token = jwt.sign(userObj, "b1OFyhEJyZfdsfWxh4WKoLzb", {
        expiresIn: 29030400 // 1 week
    });
    callback(null, token);
}
module.exports.uploadToS3 = (path, originalname, callback) => {
    fs.readFile(path, (err, data) => {
            
            if(err) console.log(err)
            console.log(data)
            var params = {
              Bucket: 'enqazbackend',
              Key: originalname, 
              Body: data,
              ACL:'public-read'
            };
            s3.upload(params, function(error,result){
            //cloudinary.v2.uploader.upload(req.files[0].path, function(error, result) {
                console.log(result)
                //console.log(error)
                if(error || !result){
                    callback(true, null)
                //   return res.json({
                //     success: false,
                //     message: error || "Failed to upload image"
                //   });
                }
                else{
                    callback(null, result)
                //   userData.imageURL = result.Location;
                //   // if(userArr[0].isVendor == false){
                //   //   userArr[0].role = 4;
                //   // }
                  
                //   //console.log(userArr[0].role)

                //   vendor.insertMany(userData, function (err, users) {
                //     // console.log("h10", err, users);
                //     if (err) {
                //       res.json({
                //         success: false,
                //         message: err
                //       })
                //     } else {
                //       res.json({
                //         success: true,
                //         users: users
                //       })
                //     }
                //   });
                }
            })
    })
}