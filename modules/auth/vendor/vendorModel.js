const mongoose = require('mongoose')
const bcryptjs = require('bcryptjs')


const vendorSchema = mongoose.Schema({
    imageURL: {
        type: String
    },
    idURL: {
        type: String
    },
    licenseURL: {
        type: String
    },
    name: {
        type: String,
        required: false
    },
    email: {
        type: String,
        unique: true,
        required: true,
    },
    phoneNumber: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true
    },
    passVerificationCode: {
        type: Number,
    },
    resetPasswordToken: {
        type: String,
    },
    // superAdmin : 1
    // moderator : 2  
    role: {
        type: Number,
        default: 3,
        required: true
    },
    isActive: {
        type: Boolean,
        default: false
    },
    isAvailable: {
        type: Boolean,
        default: true
    },
    isVendor: {
        type: Boolean,
        default: false
    },
    isSetof: {
        type: Boolean,
        default: false
    },
    cashRequest: {
        type: Boolean,
        default: true
    },
    drivers: [{
        type: String,
    }],
    cars: [{
        type: String,
    }],
    parentId: {
        type: String,
    },
    packages: [{
        type: String,
    }],
    services: [{
        type: String,
    }],
    enqazPercentage: {
        type: Number
    },
    setOfDate: {
        type: Date
    },
    photo: {
        type: String,
        required: false
    },
    adminActivation: {
      type: Boolean,
      required: false  
    },
    connectionId: {
        type: String
    },
    promoCode: {
        type: String
    }, //number of users insurance company is allowed to add
    numOfUsers: {
        type: Number
    }
})

const Vendor = module.exports = mongoose.model('Vendor', vendorSchema)

// Save a new admin
module.exports.addUser = function (newUser, callback) {
    bcryptjs.genSalt(10, (err, salt) => {
        bcryptjs.hash(newUser.password, salt, (err, hash) => {
            if (err) callback(err);
            newUser.password = hash
            newUser.save(callback)
        });
    });
}