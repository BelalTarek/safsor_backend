const vendor = require('./vendorModel');

module.exports.findOneByEmailOrPhone = function (userData, callback) {
    if (userData.phoneNumber) {
        vendor.findOne({
            phoneNumber: userData.phoneNumber
        }, callback);
    } else if (userData.email) {
        vendor.findOne({
            email: userData.email
        }, callback);
    }
};
module.exports.findOneById = function (userData, callback) {
    vendor.findOne({
        _id: userData._id
    }, callback);
}
module.exports.reActivate = function (user, callback) {
    vendor.findOneAndUpdate({
        _id: user._id,
    }, {
        $set: {
            active: true,
        }
    }, {
        new: true
    }, callback);
}