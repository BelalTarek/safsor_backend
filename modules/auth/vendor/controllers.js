const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
var request = require('request');
var rn = require('random-number');
const vendor = require('./vendorModel');
const vendorHelper = require('./helperCallbackFun');
const shared = require('../shared');
const middlewares = require('../../middlewares');
const bcryptjs = require('bcryptjs')
const requests = require('../../requests/model')
const push = require('../../pushNot/model')
let promoCodes = require('../../promocodes/model');
const admin = require("firebase-admin");
const serviceAccount = require("../../../config/firebaseKey.json");
var db = admin.database();
var ref = db.ref("/drivers");
var Report = require('../../report/model')
var Rating = require('../../rating/model')
var Invoice = require('../../invoice/model')
var AWS = require('aws-sdk');

AWS.config.region = 'us-east-1';
AWS.config.update({
      accessKeyId: process.env.AWS_Access_Key_ID,
      secretAccessKey: process.env.AWS_Secret_Access_Key,
}); 
var s3 = new AWS.S3();
var sns = new AWS.SNS();

var fs = require('fs');

var multer = require('multer');
var cloudinary = require('cloudinary');

cloudinary.config({
  cloud_name: process.env.cloudinary_cloud_name, 
  api_key: process.env.cloudinary_api_key, 
  api_secret: process.env.cloudinary_api_secret
});
var upload = multer().single('file');

exports.IsActive = function (req, res, next) {
  vendor.findOne({
    _id: req.user._doc._id
  }, (err, driver) => {
    if (err || !driver) {
      res.json({
        success: false,
        message: err
      })
    } else {
      // Report.findOne({
      //   'connectionHistory.endTime': null,
      //   driverId: req.user._doc._id
      // }, function(er, conn){
      //   if(er || !conn){
      //     return res.json({
      //       success: false,
      //       message: er || "no connection"
      //     })
      //   }
      //   else{
      //     res.json({
      //       success: true,
      //       active: driver.isActive,
      //       connectionId: conn._id
      //     })
      //   }
      // })
      res.json({
        success: true,
        active: driver.isActive,
        connectionId: driver.connectionId
      })
    }
  });
}

// exports.getCars = function (req, res, next) {
//   car.find({
//    parentId: req.user._doc._id
//   }, (err, cars) => {
//     if (err || !cars) {
//       res.json({
//         success: false,
//         message: err
//       })
//     } else {
//       // console.log(driver)
//       res.json({
//         success: true,
//         cars: cars
//       })

//     }
//   });
// }

exports.showWorkerList = function (req, res, next) {
  console.log(req.user._doc._id)
  vendor.find({
    parentId: req.user._doc._id || req.body.vendorId
    // {
    //   $in: [req.user._doc._id, req.body.vendorId]
    // }
    // $or:
    // [{parentId: req.user._doc._id},
    // {parentId: req.body.vendorId}]
  }, (err, workers) => {
    if (err) {
      res.json({
        success: false,
        message: err
      })
    } else {
      console.log(workers.length)
      var vendorIds = [];
      var workersJSON = JSON.parse(JSON.stringify(workers))
        for (var i = 0; i < workers.length; i++) {
          vendorIds.push(workers[i]._id)
        }
        Rating.find({
          userId: {
            $in : vendorIds
          }
        }, function(er, rates){
          if(er){
            return res.json({
              success: false,
              message: er
            })
          }
          // else if(rates.length <= 0){
          //   res.json({
          //     success: true,
          //     list: vendors
          //   })
          // }
          else{
            Report.find({
              driverId: {
                $in: vendorIds
              }
            }, function(e, reports){
              if(e){
                  return res.json({
                    success: false,
                    message: e
                  })
              }
              else{
                if(rates.length <= 0 && reports.length <= 0){
                    res.json({
                      success: true,
                      list: workers
                    })
                }
                else{
                  var result = [];
                  //console.log(reports.filter( report => report.driverId==workers[0]._id))
                  var now = new Date();

                  var lastSunday = now.getDate() - now.getDay();

                  var lastSaturday;
                  var month;
                  if(lastSunday > 0){
                      lastSaturday = lastSunday - 1;
                      month = now.getMonth();

                  }
                  else{
                      var lastDayOfMonth = new Date(now.getFullYear(), now.getMonth(), 0);
                      lastSaturday = lastDayOfMonth.getDate() + lastSunday - 1;
                      month = now.getMonth() - 1;
                  }
                    //console.log(new Date(now.getFullYear(), now.getMonth(), 1))
                  var dateOfLastSaturday = new Date(now.getFullYear(), month, lastSaturday);
                  console.log(now, dateOfLastSaturday)
                  Invoice.find({
                      // driverId: vendors[i]._id,
                      date: { $gte: dateOfLastSaturday , $lte: now}
                  }, (ee, invoices) => {
                    if(ee){
                        return res.json({
                          success: false,
                          message: ee
                        })
                    }
                    else{
                       console.log("inv", invoices)
                       for (var i = 0; i < workers.length; i++) {
                        var vendorReports = reports.filter( report => report.driverId==workers[i]._id);
                        //console.log(i)
                        var vendorInvoices = invoices.filter( invoice => invoice.driverId==workers[i]._id)
                          var totalEnqazPercentage = 0;
                          var cash = 0;
                          var credit = 0;
                          for (var j = 0; j < vendorInvoices.length; j++) {
                            totalEnqazPercentage += vendorInvoices[j].enqazPercentage
                            if(vendorInvoices[j].paymentType == "Cash" || vendorInvoices[j].paymentType == "cash"){
                              cash += vendorInvoices[j].finalCost
                            }
                            else{
                              credit += vendorInvoices[j].finalCost
                            }
                          }
                          var setOff = totalEnqazPercentage - credit
                          console.log(setOff)
                          workersJSON[i]["setOffAmount"] = setOff > 0 ? setOff : 0.0
                        if(vendorReports.length <= 0){
                          var rate = rates.filter( rate => rate.userId==workers[i]._id)
                          workersJSON[i]["rating"] = rate.length > 0 ? Number.parseFloat(rate[0].rating).toFixed(2) : 5.0;
                          workersJSON[i].rejectionRate = 0.0;
                          var vendorElement = {
                            vendor: workers[i],
                            rate: rates.filter( rate => rate.userId==workers[i]._id)//,
                            //rejectionRate: (rejection / totalRecievedReq) * 100
                          }
                          result.push(vendorElement)
                        }
                        else{
                          var rate = rates.filter( rate => rate.userId==workers[i]._id)
                          //console.log(vendors[i])
                          workersJSON[i]["rating"] = rate.length > 0 ? Number.parseFloat(rate[0].rating).toFixed(2) : 5.0;
                          var totalRecievedReq = 0;
                          var totalAcceptedReq = 0;
                          for (var j = 0; j < vendorReports.length; j++) {
                            //reports[i]
                            totalRecievedReq += vendorReports[j].connectionHistory.numOfReqRecieved;
                            totalAcceptedReq += vendorReports[j].connectionHistory.numOfReqAccepted;
                          }
                          var rejection = totalRecievedReq - totalAcceptedReq;
                          if(totalRecievedReq == 0){
                            workersJSON[i]["rejectionRate"] = 0.0;
                          }
                          else{
                            workersJSON[i]["rejectionRate"] = parseInt((rejection / totalRecievedReq) * 100)
                          }
                          //workersJSON[i]["rejectionRate"] = (rejection / totalRecievedReq) * 100
                          var vendorElement = {
                            vendor: workers[i],
                            rate: rates.filter( rate => rate.userId==workers[i]._id),
                            rejectionRate: (rejection / totalRecievedReq) * 100
                          }
                          result.push(vendorElement)
                        }
                        //console.log(vendorReports)
                        
                        //vendors[i]
                        //console.log(result)
                      }
                      //workers.forEach(element => console.log(element.rating))
                      res.json({
                        success: true,
                        //list: result,
                        list: workersJSON
                      })
                     }
                   })
                  
                }
                
              }
            })
            
          }
        })
      // res.json({
      //   success: true,
      //   list: workers
      // })
    }
  });
}
// Login api for admin
exports.login = function (req, res, next) {
  let userData = req.body;
  if (userData.email && userData.password && userData.fcmToken || userData.phoneNumber && userData.password && userData.fcmToken) {
    // add attr. "findWith" to let the callback fun. know the type of credentials
    vendorHelper.findOneByEmailOrPhone(userData, (err, user) => {
      if (user) {
        shared.checkPassword(userData.password, user, (error, token) => {
          // error should be true if password is wrong
          if (error) {
            res.json({
              success: false,
              message: "incorrect password"
            });
          } else {
            push.findOneAndUpdate({
              userId: user._id
            }, {
              $set: {
                fcmToken: req.body.fcmToken
              }
            }, {
              new: true
            }, function (err, doc) {
              if (!doc) {
                let newPushObj = {
                  userId: user._id,
                  fcmToken: req.body.fcmToken
                }
                push.create(newPushObj, (err, done) => {
                  if (done) {
                    userLoggedIn(token, user);
                  }
                })
              } else {
                userLoggedIn(token, user);
              }
            });
          }
        });
      } else if (!user) {
        res.json({
          success: false,
          message: "user not found"
        });
      } else if (err) {
        res.json({
          success: false,
          message: err
        });
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
  // if user can login successfully we will call this function 
  let userLoggedIn = (token, user) => {
    return res.json({
      success: true,
      token: 'Bearer ' + token,
      user: {
        id: user._id,
        name: user.name,
        phoneNumber: user.phoneNumber,
        username: user.username,
        email: user.email,
        role: user.role,
        imageURL: user.imageURL,
        promoCode: user.promoCode,
        services: user.services
      }
    });
  }
};
// api to send a mail to your email address with a code 
// which you will enter in the mobile app so you can change password
exports.sendForgetPasswordCode = function (req, res, next) {
  let userData = req.body;
  if (userData.email || userData.phone) {
    vendorHelper.findOneByEmailOrPhone(userData, (err, userFound) => {
      if (!userFound) {
        res.json({
          success: false,
          message: "user email address doesn't exist"
        });
      } else {
        console.log("hiiiiii24")
        let smtpTrans = nodemailer.createTransport({
          service: 'SendGrid',
          auth: {
            user: 'bishoywadie',
            pass: 'user123456'
          }
        });
        var gen = rn.generator({
          min: -900000,
          max: 900000,
          integer: true
        });
        generatedNum = gen(100000);
        userFound.passVerificationCode = generatedNum;
        userFound.save((err) => {
          if (err) {
            res.json({
              success: false,
              message: err
            });
          } else {
            var mailOptions = {
              to: req.body.email,
              from: 'noreply@enqaz.com',
              subject: 'Password Reset - enqaz',
              text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                'Please use the following code: ' + generatedNum + '\n\n' +
                ' If you did not request this, please ignore this email and your password will remain unchanged.\n'
            };
            smtpTrans.sendMail(mailOptions, function (err, sent) {
              console.log("err,sent", err, sent)
              if (err) res.json({
                success: false,
                message: err
              });
              res.json({
                success: true,
                message: req.body.email
              });
            });
          }
        });
      }
    });
  }
  else if(userData.phoneNumber){

   vendor.findOne({phoneNumber : userData.phoneNumber},function(err, user){
     if(err){
       return res.json({
         success: false,
         message: err
       })
     }
     if(!user){
       return res.json({
         success: false,
         message: "this phone number does not exist"
       })
     }
     else{
         var gen = rn.generator({
         min: -900000,
         max: 900000,
         integer: true
       });
       generatedNum = gen(100000);
       user.passVerificationCode = generatedNum;
       //var verifyRequestId ;
       user.save(function(errr, result){
        if(errr || !result){
          return res.json({
            success: false,
            message: errr || "error updating user"
          })
        }
        else{
          var params = {
            Message: 'Enqaz \n\nVerification code: '+ generatedNum +'.\n\n',
            MessageStructure: 'string',
            PhoneNumber: '2'+user.phoneNumber,
            Subject: 'ENQAZ'
          };

          sns.publish(params, function(eror, data) {
                if (eror){
                  return res.json({
                    success: false,
                    message: eror
                  })
                } // an error occurred
                else{
                  res.json({
                    success: true,
                    message: "SMS sent successfully",
                    //data: data
                  })
                }              // successful response
          });
        }
       });

       
     }
   })
  

    vendor.findOne({phoneNumber : userData.phoneNumber},function(err, user){
      if(err){
        return res.json({
          success: false,
          message: err
        })
      }
      if(!user){
        return res.json({
          success: false,
          message: "this phone number does not exist"
        })
      }
      else{
 
       
        var gen = rn.generator({
          min: -900000,
          max: 900000,
          integer: true
        });
        generatedNum = gen(100000);
        user.passVerificationCode = generatedNum;
        //var verifyRequestId ;
        user.save(function(errr, result){
         if(errr || !result){
           return res.json({
             success: false,
             message: errr || "error updating user"
           })
         }
         else{
           var params = {
             Message: 'Enqaz \n\nVerification code: '+ generatedNum +'.\n\n',
             MessageStructure: 'string',
             PhoneNumber: '2'+user.phoneNumber,
             Subject: 'ENQAZ'
           };
 
           sns.publish(params, function(eror, data) {
                 if (eror){
                   return res.json({
                     success: false,
                     message: eror
                   })
                 } // an error occurred
                 else{
                   res.json({
                     success: true,
                     message: "SMS sent successfully",
                     //data: data
                   })
                 }              // successful response
           });
         }
        });
 
        

      }
    })
  } 
  else {
    res.json(middlewares.checkAllFieldsExist)
  }
};



// add vendors 
exports.addWorkers = function (req, res, next) {
  let userData = req.body;
  let userInfo = req.user;
  var array = [];
  // query users collection to check if phonenumbers already registered
  let findRegisteredUsers = (usersPhoneNumb) => {
    vendor.find({
      phoneNumber: {
        $in: usersPhoneNumb
      }
    }, (err, users) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        saveBulk(users, usersPhoneNumb);
      }
    });
  }
  // return phone numbers if found any registered or save bulk of users 
  let saveBulk = (usersFound, usersPhoneNumb) => {
    //array to save phoneNumber of all users in the array
    let phoneNum = [];
    if(!Array.isArray(userData)){
      if (usersFound.length < 1) {
  
        //let each = userData;
        var salt = bcryptjs.genSaltSync(10);
        var hash = bcryptjs.hashSync(userData.password, salt);
        userData.password = hash;
        //userData[i] = each;
        userData.parentId = userInfo._doc._id;
        userData.role = 4;
      }
    }
    else{
      for (var i = 0; i < userData.length; i++) {
        //console.log('hereeee1')
        // if there is no users found already registered, hash passwords
        if (usersFound.length < 1) {
  
          let each = userData[i];
          if (i === (userData.length)) {
            //console.log('hereeee')
  
            break;
          } else {
            console.log('hereeee')
            // hashing password in every user object in the array
  
            
            //console.log('url-- '+userData[i].imageURL)
            var salt = bcryptjs.genSaltSync(10);
            var hash = bcryptjs.hashSync(userData[i].password, salt);
            each.password = hash;
            userData[i] = each;
            userData[i].parentId = userInfo._doc._id;
            userData[i].role = 4;
          }
        }
      }
    }
    
    if (usersFound.length < 1) {

      if(req.files){
        console.log("files", req.files, "body", req.body)
          // res.json({
          //   success: true,
          //   files: req.files
          // })
          var files = req.files;
          if(files.imageURL && files.idURL && files.licenseURL){
            shared.uploadToS3(files.imageURL[0].path, files.imageURL[0].originalname, (error, data) => {
              if(error || data == null){
                res.json({
                  success: false,
                  message: "Failed to upload the image"
                })
              }
              else{
                shared.uploadToS3(files.idURL[0].path, files.idURL[0].originalname, (err, data2) => {
                  if(err || data2 == null){
                    res.json({
                      success: false,
                      message: "Failed to upload the ID"
                    })
                  }
                  else{
                    shared.uploadToS3(files.licenseURL[0].path, files.licenseURL[0].originalname, (er, data3) => {
                      if(er || data3 == null){
                        res.json({
                          success: false,
                          message: "Failed to upload the license"
                        })
                      }
                      else{
                        userData.imageURL = data.Location;
                        userData.idURL = data2.Location
                        userData.licenseURL = data3.Location
  
                        vendor.insertMany(userData, function (err, users) {
                                    // console.log("h10", err, users);
                          if (err) {
                            res.json({
                              success: false,
                              message: err
                            })
                          } else {
                              res.json({
                                success: true,
                                users: users,
                                files: req.files
                              })
                          }
                        });
                      }
                    })
                  }
                })
              }
            })
          }
          else{
            res.json(middlewares.checkAllFieldsExist)
          }
          
      }
      else{
      

              vendor.insertMany(userData, function (err, users) {
          if (err) {
            res.json({
              success: false,
              message: err
            })
          } else {
            console.log(users)
            vendor.findOne({
                _id: userInfo._doc._id
              },
              (err, user) => {
                if (!user) {
                  res.json({
                    success: false,
                    message: "no parent for this driver"
                  })

                } else {
                  for (var i = 0; i < users.length; i++) {
                    array[i] = users[i]._id.toString();
                    // user.drivers.push(array[i])
                  }
                  //console.log(user)
                  vendor.findByIdAndUpdate(userInfo._doc._id, {
                      $push: {
                        drivers: array
                      }
                    }, {
                      safe: true,
                      upsert: true
                    },
                    function (err, doc) {
                      if (err) {
                        console.log(err);
                      } else {
                        res.json({
                          success: true,
                          users: users
                        })
                      }
                    });
                  // user.save((err, user2) => {
                  //   if (err) {
                  //     console.log(err)
                  //     res.json({
                  //       success: false,
                  //       message: "kfaya"
                  //     })
                  //   }else{
                  //     res.json({
                  //     success: true,
                  //     users: user
                  //    })
                  //   }

                  // })
                }
              })

          }
        });
              // userFound.save((err, user) => {
              //     if (!user || err) {
              //       return res.json({
              //         success: false,
              //         message: err
              //       });
              //     } /*else {
              //       res.json({
              //         success: true,
              //         message: "Info Updated Successfuly"
              //       });
              //     }*/
              // })
    //         }

    //       });
    // })       
      }
      
      
    } else {
      res.json({
        success: false,
        message: "following phone numbers found already registered " + usersPhoneNumb,
      });
    }
  }
  // array has objects or empty?
  //console.log(userData[0])
  var userDataArr = [];
  userDataArr.push(userData)
  if(!Array.isArray(userData)){
    console.log("m4 array")
    var usersPhoneNumb = [];
    if (userData.name && userData.email && userData.username && userData.phoneNumber && userData.password && userData.services /*&& userData[i].enqazPercentage && userData[i].setOfDate*/) {
      usersPhoneNumb.push(userData.phoneNumber);
      console.log('1', usersPhoneNumb)
      findRegisteredUsers(usersPhoneNumb);
    }
    else {
      res.json({
        success: false,
        message: 'all attributes are needed',
        user: userData,
        data: userData[0].toString()
      })
    }
  }
  else if (userData.length > 0) {
    var usersPhoneNumb = [];
    // loop on the bulk of users to be added
    for (var i = 0; i < userData.length; i++) {
      // check all attr. are there 
      if (userData[i].name && userData[i].email && userData[i].username && userData[i].phoneNumber && userData[i].password && userData[i].services /*&& userData[i].imageURL && userData[i].idURL && userData[i].licenseURL /*&& userData[i].setOfDate*/) {
        usersPhoneNumb.push(userData[i].phoneNumber)
        if (i === (userData.length)) {
          break;
        }
      } else {
        res.json({
          success: false,
          message: 'all attributes are needed'
        })
      };
    }
    // let's check if this phone numbers associated already to registered accounts
    findRegisteredUsers(usersPhoneNumb);
  } else {
    res.json({
      success: true,
      message: 'it must be an array with at least one object'
    })
  }
};

// enter old and new password to renew it
exports.resetPassword = function (req, res, next) {
  let userData = req.body;
  if (userData.password && userData.newPassword) {
    vendorHelper.findOneByEmailOrPhone({
      email: req.user._doc.email
    }, (err, userFound) => {
      if (userFound) {
        shared.checkPassword(userData.password, userFound, function (err, isMatch) {
          if (err) {
            res.json({
              success: false,
              message: 'wrong password!'
            });
          } else {
            shared.hashPassword(userData.newPassword, (err, hashedPassword) => {
              userFound.password = hashedPassword;
              userFound.save();
            });
            res.json({
              success: true,
              message: 'Your password reset successfuly'
            });
          }
        });
      } else {
        res.json({
          success: false,
          message: err
        })
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
};
// check that code entered by user is correct
exports.verifyForgetPasswordCode = function (req, res, next) {
  let reqPassVerCode = Number(req.body.passVerificationCode);
  if (reqPassVerCode) {
    vendor.findOne({
      passVerificationCode: reqPassVerCode
    }, (err, userFound) => {
      console.log(reqPassVerCode, userFound)
      if (!userFound) {
        res.json({
          success: false,
          message: "user details doesn't exist"
        });
      } else if (userFound && userFound.passVerificationCode) {
        if (reqPassVerCode === userFound.passVerificationCode) {
          shared.generateToken(userFound, (error, token) => {
            userFound.token = token;
            userFound.save((err, done) => {
              if (err) {
                res.json({
                  success: false,
                });
              } else {
                res.json({
                  success: true,
                  token: 'Bearer ' + token
                });
              }

            })
          });
        } else {
          userFound.passVerificationCouserFound.passVerificationCodede
          res.json({
            success: false,
            message: "don't match"
          });
        }
      } else if (err) {
        res.json({
          success: false,
          message: err
        });
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
};

exports.editWorker = function (req, res, next) {
  let reqBody = req.body;
  let files = req.files
  if (!reqBody) {
    res.json(middlewares.checkAllFieldsExist)
  }
  console.log(reqBody, files)
  vendorHelper.findOneById({
    _id: reqBody.userId
  }, (err, userFound) => {
    if (!userFound) {
      res.json({
        success: false,
        message: "user details doesn't exist"
      });
    } else if (userFound) {
      if (reqBody.name) {
        userFound.name = reqBody.name
      }
      if (reqBody.username) {
        userFound.username = reqBody.username
      }
      if (reqBody.email) {
        userFound.email = reqBody.email
      }
      if (reqBody.addresses) {
        userFound.addresses = reqBody.addresses
      }
      if (reqBody.phoneNumber) {
        userFound.phoneNumber = reqBody.phoneNumber
      }
      if (reqBody.services) {
        userFound.services = reqBody.services
      }
      if (reqBody.setOfDate) {
        userFound.setOfDate = reqBody.setOfDate
      }
      if (reqBody.password) {
        shared.hashPassword(reqBody.password, (err, hashedPassword) => {
          userFound.password = hashedPassword;
          userFound.save();
        });
      }
      if(files){
        console.log("files", req.files)
        if(files.imageURL){
          //userFound.imageURL = reqBody.imageURL
          shared.uploadToS3(files.imageURL[0].path, files.imageURL[0].originalname, (err, data) => {
            if(err || data == null){
              res.json({
                success: false,
                message: "Failed to upload the image"
              })
            }
            else{
              userFound.imageURL = data.Location
              userFound.save()
            }
          })
        }
        if(files.idURL){
          //userFound.idURL = reqBody.idURL
          shared.uploadToS3(files.idURL[0].path, files.idURL[0].originalname, (err, data) => {
            if(err || data == null){
              res.json({
                success: false,
                message: "Failed to upload the ID"
              })
            }
            else{
              userFound.idURL = data.Location
              userFound.save()
            }
          })
        }
        if(files.licenseURL){
          //userFound.licenseURL = reqBody.licenseURL
          shared.uploadToS3(files.licenseURL[0].path, files.licenseURL[0].originalname, (err, data) => {
            if(err || data == null){
              res.json({
                success: false,
                message: "Failed to upload the license"
              })
            }
            else{
              userFound.licenseURL = data.Location
              userFound.save()
            }
          })
        }
      }
      userFound.save((err, user) => {
        if (!user) {
          res.json({
            success: false,
            message: err
          });
        } else {
          res.json({
            success: true,
            message: "Info Updated Successfuly"
          });
        }
      })
    }
  });
};
exports.updateInfo = function (req, res, next) {
  let reqBody = req.body;

  // if (!reqBody) {
  //   res.json(middlewares.checkAllFieldsExist)
  // }
  vendorHelper.findOneById({
    _id: req.user._doc._id
  }, (err, userFound) => {
    if (!userFound || err) {
      res.json({
        success: false,
        message: "user details doesn't exist" || err
      });
     }//else{
    //   console.log(userFound)
    // }
    //console.log('hhhhhh')

     if (req.file) {

        //upload(req, res, function (err) {
        console.log('henaaa  '+ req.file.path)
        // if (err) {
        //   //console.log('henaaa  '+ req.file)
        //   return res.json({
        //     success: false,
        //     message: err
        //   });
        // }
        //else if(req.file){
          
          cloudinary.v2.uploader.upload(req.file.path, function(error, result) {
            console.log(result)
            if(error || !result){
              return res.json({
                success: false,
                message: err || "Failed to upload image to cloudinary"
              });
            }
            else{
              userFound.imageURL = result.url;

              //console.log('url-- '+result.url)
              userFound.save((err, user) => {
                  if (!user || err) {
                    return res.json({
                      success: false,
                      message: err
                    });
                  } /*else {
                    res.json({
                      success: true,
                      message: "Info Updated Successfuly"
                    });
                  }*/
              })
            }

          });
       // }
      //})
    }
      //console.log('url -- ',userFound.imageURL)

      if (reqBody.name) {
        userFound.name = reqBody.name
      }
      if (reqBody.username) {
        userFound.username = reqBody.username
      }
      if (reqBody.email) {
        userFound.email = reqBody.email
      }
      if (reqBody.addresses) {
        userFound.addresses = reqBody.addresses
      }
      if (reqBody.phoneNumber) {
        userFound.phoneNumber = reqBody.phoneNumber
      }
      if (reqBody.password) {
        shared.hashPassword(reqBody.password, (err, hashedPassword) => {
          userFound.password = hashedPassword;
          userFound.save();
        });
      }
      //console.log('img-- '+userFound.imageURL)
      userFound.save((err, user) => {
        if (!user || err) {
          res.json({
            success: false,
            message: err
          });
        } else {
          res.json({
            success: true,
            message: "Info Updated Successfuly",
            //user: user
          });
        }
      })
 
  });
};
exports.deactivateMyAccount = function (req, res, next) {
  vendorHelper.findOneById({
    _id: req.user._doc._id,
  }, (err, userFound) => {
    if (!userFound) {
      res.json({
        success: false,
        message: "user details doesn't exist"
      });
    } else if (userFound) {
      userFound.isActive = false;
      userFound.save((err, user) => {
        if (err || !user) {
          res.json({
            success: false,
            message: err
          });
        } else {
          if(req.body.connectionId){
            Report.findOneAndUpdate({
              driverId: userFound._id,
              _id: req.body.connectionId 
              // 'connectionHistory.endTime': null
            },
            {
              $set: {'connectionHistory.endTime': Date.now() } 
            },{new: true}, function(er, result){
              if(er || !result){
                return res.json({
                  success: false,
                  message: er || 'error updating'
                })
              }
              else{
                res.json({
                  success: true,
                  message: "user deactiavted successfully"
                });
              }
              
            })
          }
          else{
            Report.findOneAndUpdate({
              driverId: userFound._id,
              //_id: reqBody.connectionId 
               'connectionHistory.endTime': null
            },
            {
              $set: {'connectionHistory.endTime': Date.now() } 
            },{new: true}, function(er, result){
              if(er || !result){
                return res.json({
                  success: false,
                  message: er || 'error updating'
                })
              }
              else{
                res.json({
                  success: true,
                  message: "user deactiavted successfully"
                });
              }
              
            })
            // res.json({
            //   success: true,
            //   message: "user deactiavted successfully"
            // });
          }
          
          
        }
      });
    };
  });
  // ref.once('value', function (snapshot) {
  //   snapshot.forEach(function (childSnapshot) {
  //     var childKey = childSnapshot.key;
  //     var childData = childSnapshot.val();
  //     if (childData.DriverId == req.user._doc._id) {
  //       ref.child(childKey).remove();
  //     }
  //   });
  // });
};
exports.activateMyAccount = function (req, res, next) {
  vendorHelper.findOneById({
    _id: req.user._doc._id,
  }, (err, userFound) => {
    if (!userFound) {
      res.json({
        success: false,
        message: "user details doesn't exist"
      });
    } else if (userFound.adminActivation == false) {
      res.json({
        success: false,
        message: "you are deactivated by admin"
      });
    } else if (userFound.isActive == true) {
      res.json({
        success: false,
        message: "you are already active",
        id: userFound._id
      });
    } else {
      userFound.isActive = true;
      var newConnection = new Report({
        driverId: userFound._id,
        connectionHistory: {
          startTime : Date.now()
        }
      })
      newConnection.save(function(err, result){
        if(err || !result){
          return res.json({
            success: false,
            message: err || "failed to create report"
          })
        }
        else{
          userFound.connectionId = result._id
          userFound.save((er, user) => {
            if(er || !user){
              return res.json({
                success: false,
                message: er || "failed"
              })
            }
            else{
              res.json({
                success: true,
                message: "actiavted successfully",
                connectionId: user.connectionId
              });
            }
          })
          // res.json({
          //   success: true,
          //   message: "actiavted successfully",
          //   connectionId: result._id
          // });
        }
      })
    };
  });
};


exports.salesReport = function (req, res, next) {
  let userInfo = req.user;
  // console.log(userInfo._doc.drivers)
  requests.find({
    driverId: {
      $in: userInfo._doc.drivers
    }
  }, (err, requests) => {
    if (!requests) {
      res.json({
        success: false,
        message: err
      })
    } else {
      // console.log(requests)
      res.json({
        success: true,
        message: requests
      })
    }
  })
}

// exports.assignDriverToCar = function (req, res, next) {
//   let userData = req.body;
//   let userInfo = req.user;
//   if (userData.driverId && userData.carNumber) {
//     car.findOne({
//       carNumber: userData.carNumber,
//       parentId: userInfo._doc._id
//     }, (err, car) => {
//       if (err) {
//         res.json({
//           success: false,
//           message: err
//         })
//       } else {
//         if (car.isAvailable == false) {
//           res.json({
//             success: false,
//             message: "this car in not available now"
//           })
//         } else {
//           vendor.findOne({
//             _id: userData.driverId,
//             parentId: userInfo._doc._id
//           }, (err, driver) => {
//             if (err) {
//               res.json({
//                 success: false,
//                 message: err
//               })
//             } else {
//               if (driver.isAvailable == false) {
//                 res.json({
//                   success: false,
//                   message: "this car in not available now"
//                 })
//               } else {
//                 car.driverId = userData.driverId;
//                 car.isAvailable == false;
//                 car.save((err, car2) => {
//                   if (err) {
//                     res.json({
//                       success: false,
//                       message: err
//                     })
//                   } else {
//                     res.json({
//                       success: true,
//                       message: 'assigned successfuly'
//                     })
//                   }
//                 })
//               }
//             }
//           })
//         }
//       }
//     })
//   } else {
//     res.json(middlewares.checkAllFieldsExist)
//   }
// }


module.exports.addCode = function (req, res, next) {
  let reqBody = req.body;
  if (reqBody.code && reqBody.expiresAt && reqBody.discountRate) {
    let findQuery = {
      code: reqBody.code,
    };
    promoCodes.findOne(findQuery, (err, code) => {
      if (code) {
        res.json({
          success: false,
          message: "promo-code already added"
        })
      } else {
        promoCodes.create({
            userId: req.user._doc._id,
            code: reqBody.code,
            expiresAt: reqBody.expiresAt,
            discountRate: reqBody.discountRate
          },
          (err, code) => {
            if (!code) {
              res.json({
                success: false,
                message: err
              })
            } else {
              res.json({
                success: true,
                code: code
              });
            }
          });
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
};

exports.reactivateWorker = function (req, res, next) {
  let reqBody = req.body;
  let userInfo = req.user;
  vendor.findOne({
    _id: reqBody.id,
    parentId: req.user._doc._id
  }, function (err, doc) {
    if (!doc) {
      res.json({
        success: false,
        message: 'falied To cancel'
      })
    } else {
      if (doc.isActive == true) {
        res.json({
          success: false,
          message: 'driver is already active'
        })
      }
      doc.isActive = true;
      doc.save((err, vendor) => {
        if (!vendor) {
          res.json({
            success: false,
            message: 'can not save update'
          })
        } else {
          res.json({
            success: true,
            message: 'driver Activated successfuly'
          })
        }
      })
    }
  })
}

// exports.addBulkCars = function (req, res, next) {
//   let userData = req.body;
//   let userInfo = req.user;
//   var array = [];
//   // query users collection to check if phonenumbers already registered
//   let findExistCars = (carsId) => {
//     car.find({
//       carNumber: {
//         $in: carsId
//       }
//     }, (err, foundCars) => {
//       if (err) {
//         // console.log(err);
//         res.json({
//           success: false,
//           message: err
//         })
//       } else {
//         // console.log(cars)
//         saveBulk(foundCars, carsId);
//       }
//     });
//   }
//   // return phone numbers if found any registered or save bulk of users 
//   let saveBulk = (carsFound, carsId) => {
//     for (var i = 0; i < userData.length; i++) {
//       // if there is no users found already registered, hash passwords
//       if (carsFound.length < 1) {
//         if (i === (userData.length)) {
//           break;
//         } else {
//           userData[i].parentId = userInfo._doc._id;
//           //userData[i].driverId = userData[i].driverId
//         }
//       }
//     }
//     if (carsFound.length < 1) {
//       car.insertMany(userData, function (err, cars) {
//         if (err) {

//           res.json({
//             success: false,
//             message: err
//           })
//         } else {
//           vendor.findOne({
//               _id: userInfo._doc._id
//             },
//             (err, user) => {
//               if (err || !user) {
//                 res.json({
//                   success: false,
//                   message: err || "no parent for this car"
//                 })
//               } else {
//                 for (var i = 0; i < cars.length; i++) {
//                   array = cars[i]._id.toString();
//                 }
//                 user.cars.push(array)
//                 user.save((err, user2) => {
//                   if (err || !user2) {
//                     res.json({
//                       success: false,
//                       msg: err || "issue"
//                     })
//                   } else {
//                     res.json({
//                       success: true,
//                       cars: cars
//                     })
//                   }
//                 })
//               }
//             })
//         }
//       });
//     } else {
//       res.json({
//         success: false,
//         message: "following carNumbers is already exist ",
//       });
//     }
//   }
//   // array has objects or empty?
//   if (userData.length > 0) {
//     var carsId = [];
//     // loop on the bulk of users to be added
//     for (var i = 0; i < userData.length; i++) {
//       // check all attr. are there 
//       if (userData[i].carNumber && userData[i].carColor && userData[i].carModel && userData[i].carManufactory && userData[i].driverId) {
//         carsId.push(userData[i].carNumber)
//         // console.log(carsId)
//         if (i === (userData.length)) {
//           break;
//         }
//       } else {
//         res.json({
//           success: false,
//           message: 'all attributes are needed'
//         })
//       };
//     }
//     findExistCars(carsId);
//   } else {
//     res.json({
//       success: true,
//       message: 'it must be an array with at least one object'
//     })
//   }
// };

exports.deactivateWorker = function (req, res, next) {
  let reqBody = req.body;
  let userInfo = req.user;
  vendor.findOne({
    _id: reqBody.id,
    parentId: req.user._doc._id
  }, function (err, doc) {
    if (!doc) {
      res.json({
        success: false,
        message: 'No Deiver with this Id'
      })
    } else {
      if (doc.isActive == false) {
        res.json({
          success: false,
          message: 'driver is already not active'
        })
      }
      doc.isActive = false;
      doc.save((err, vendor) => {
        if (!vendor) {
          res.json({
            success: false,
            message: 'can not save update'
          })
        } else {
          res.json({
            success: true,
            message: 'driver deactivated successfuly'
          })
        }
      })
    }
  })
}

exports.removeWorker = function (req, res, next) {
  let reqBody = req.params;
  if (reqBody.vendorId) {
    vendor.remove({
      _id: reqBody.vendorId,
      parentId: req.user._doc._id
    }, (err) => {
      if (err) {
        res.json({
          success: false,
          message: 'No worker account for this ID'
        })
      } else {
        res.json({
          success: true,
          message: 'worker account deleted successfully'
        })
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
}