var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
//require controllers
var controller = require('../controllers');

var multer = require('multer');
var upload = multer({
    storage: multer.diskStorage({
        filename: function (req, file, cb) {
            cb(null, file.fieldname )
        }
    }),
    // rename: function (fieldname, filename) {
    //   return filename + Date.now();
    // },
    // onFileUploadStart: function (file) {
    //   console.log(file.originalname + ' is starting ...');
    // },
    // onFileUploadComplete: function (file) {
    //   console.log(file.fieldname + ' uploaded to  ' + file.path);
    // }
}).fields([
    {name: "imageURL", maxCount: 1}, 
    {name: "idURL", maxCount: 1}, 
    {name: "licenseURL", maxCount: 1}
]);

const baseRoute = '/user/admin/shared/';
// middleware to protect all except mentioned apis
router.use(jwt({
    secret: process.env.secret
}).unless({
    path: [baseRoute + 'login', baseRoute + 'forget-pass-code', baseRoute + 'verify-forget-pass-code']
}), function (req, res, next) {
    if (req.user) {
        console.log(req.user._doc.role);
        if (req.user._doc.role != 3 && req.user._doc.role != 1) return res.json({
            sucess: false,
            message: "you are not a manager"
        });
        next();
    } else {
        next();
    }
});
//Routers
/**
 * @api {get} /user/vendor/manager/show-worker-list show my list of workers
 * @apiName showListOfWorkers
 * @apiGroup ManagerSpecificApis
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *      list: list
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *      message: 'error'
 *   }
 */
router.post('/show-worker-list', controller.showWorkerList);
/**
 * @api {post} /user/vendor/manager/add-workers add array of Workers
 * @apiName addWorkers
 * @apiGroup ManagerSpecificApis
 * @apiParam {string} name user name
 * @apiParam  {string} email user email address.
 * @apiParam  {number} phoneNumber user phone number.
 * @apiParam  {string} password user password.
 * @apiParam  {string} username username.
 * @apiParam  {string} services array of services for each driver.
 * @apiParam  {Date} setOfDate Date to set of with enqaz exp 23-jul-2018.
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *      message: 'vendor registered successfully'
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *      message: 'admin faild to register'
 *   }
 */
router.post('/add-workers', upload, controller.addWorkers);
/**
 * @api {post} /user/vendor/manager/edit-worker edit worker info
 * @apiName editWorkers
 * @apiGroup ManagerSpecificApis
 * @apiParam  {string} userId id for driver to change his data.
 * @apiParam  {string} [name]
 * @apiParam  {string} [username]
 * @apiParam  {string} [email] 
 * @apiParam  {string} [addresses] 
 * @apiParam  {number} [phoneNumbers] 
 * @apiParam  {string} [password] user password.
 * @apiParam  {string} [services] array of services for each driver.
 * @apiParam  {Date} [setOfDate] Date to set of with enqaz exp 23-jul-2018.
 * @apiSuccessExample Success-Response:
 *{
 *"success": true,
 * "message" :"Info Updated Successfuly"
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    "success": false
 *    "message":"User Not Found"
 * }
 */
router.post('/edit-worker', upload, controller.editWorker);
/**
 * @api {get} /user/vendor/manager/de-activate
 * @apiName user deactivate any user by sending the following parameters or by just calling the api with token
 * @apiGroup Auth
* @apiParam  {string} [userId] user old password.
* @apiParam  {string} [userType] customer,vendor.
 * @apiSuccessExample Success-Response:
 *{
 *"success": account de-activate successfuly,
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    "success": falied To de-activate,
 * }
 */



router.post('/deactivate-user', controller.deactivateMyAccount);

router.get('/sales-reports', controller.salesReport);


// router.post('/assign-driver', controller.assignDriverToCar);

router.post('/add-promo-code', controller.addCode);

router.post('/re-activate', controller.reactivateWorker);


// router.post('/add-cars', controller.addBulkCars);

//  (id) id for driver to deactivate his account 
// (token) business account token in header

router.post('/deactivate-driver', controller.deactivateWorker);

/**
 * @api {delete} /user/admin/shared/remove-worker vendor remove worker
 * @apiName admin remove worker
 * @apiGroup vendor
 * @apiParam  {string} vendorId worker Id that you want to remove his account .
 * @apiSuccessExample Success-Response:
 *{
 *        success: true,
 *        message: 'vendor account deleted successfully'
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    success: false,
 *     message: 'No vendor account for this ID'
 * }
 */
router.delete('/remove-worker/:vendorId', controller.removeWorker);

module.exports = router;