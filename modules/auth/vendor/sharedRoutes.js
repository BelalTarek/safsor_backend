var express = require('express');
var router = express.Router();
//require controllers
var controllers = require('./controllers');
var middlewares = require('../../middlewares');

var multer = require('multer');
var upload = multer({
    storage: multer.diskStorage({
        filename: function (req, file, cb) {
            cb(null, file.fieldname )
        }
    }),
    // rename: function (fieldname, filename) {
    //   return filename + Date.now();
    // },
    // onFileUploadStart: function (file) {
    //   console.log(file.originalname + ' is starting ...');
    // },
    // onFileUploadComplete: function (file) {
    //   console.log(file.fieldname + ' uploaded to  ' + file.path);
    // }
}).single('file');

const baseRoute = '/user/vendor/shared/';

var jwt = require('express-jwt');
router.use(jwt({
    secret: process.env.secret
}).unless({
    path: [baseRoute + 'login', baseRoute + 'forget-pass-code', baseRoute + 'verify-forget-pass-code']
}), function (req, res, next) {
    if (req.user) {
        console.log(req.user._doc.role);
        if (req.user._doc.role != 3 && req.user._doc.role != 4) return res.json({
            sucess: false,
            message: "you are not a vendor"
        });
        next();
    } else {
        next();
    }
});
//Routers
/**
 * @api {post} /user/vendor/shared/login login 
 * @apiName LoginVendor
 * @apiGroup Vendor
 * @apiParam  {string} [email] One type of login methods.
 * @apiParam  {number} [phoneNumber] One type of login methods.
 * @apiParam  {string} password One type of login methods.
 *
 * @apiSuccessExample Success-Response:
 *{
 *"success": true,
 *"token": "",
 *"user": {
 *"id": "",
 *"name": "",
 *"username": "",
 *"email": ""
 *   }
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": false,
 *    "message": "Wrong password!"
 * }
 */
router.post('/login', controllers.login);
/**
 * @api {post} /user/vendor/shared/forget-pass-code vendor forget password
 * @apiName forgetPassword
 * @apiGroup Vendor
 * @apiParam  {string} email Vendor's Email address.
 * @apiSuccessExample Success-Response:
 *{
 *"success": true
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": false
 * }
 */
router.post('/forget-pass-code', controllers.sendForgetPasswordCode);
/**
 * @api {post} /user/vendor/shared/verify-forget-pass-code Vendor verifing code for changing his password 
 * @apiName verifing password verification code
 * @apiGroup Vendor
 * @apiParam  {number} passVerificationCode code sent to user on mail
 * @apiSuccessExample Success-Response:
 *{
 *"success": true
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": false
 * }
 */

router.post('/verify-forget-pass-code', controllers.verifyForgetPasswordCode);
/**
 * @api {get} /user/vendor/shared/reset-pass reset password
 * @apiName vendorResetPassword
 * @apiGroup Vendor
 * @apiParam  {string} password user old password.
 * @apiParam  {string} newPassword user new password.
 * @apiSuccessExample Success-Response:
 *{
 *        success: true,
 *        message: 'Your password reset successfuly'
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    success: false,
 *     message: 'wrong old password!'
 * }
 */
router.post('/reset-pass', controllers.resetPassword);

/**
 * @api {post} /user/vendor/shared/update-my-account update my account Info
 * @apiName updateMyAccount
 * @apiGroup Vendor
 * @apiParam  {string} [name]
 * @apiParam  {string} [username]
 * @apiParam  {string} [email] 
 * @apiParam  {string} [addresses] 
 * @apiParam  {number} [phoneNumbers] 

 * @apiSuccessExample Success-Response:
 *{
 *"success": true,
 * "message" :"Info Updated Successfuly"
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    "success": 
 *    "message":"User Not Found"
 * }
 */
router.post('/update-my-account', upload, controllers.updateInfo);

/**
 * @api {get} /user/vendor/shared/de-activate-my-account vendor deactivate his account
 * @apiName deactivate my account
 * @apiGroup Vendor
 * @apiParam  {string} id Vendor's token.
 * @apiSuccessExample Success-Response:
 *{
 *"success": true
 *"message": "user deactiavted successfully"
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": false
 *    "message": "user details doesn't exist"
 * }
 */

router.post('/de-activate-my-account', controllers.deactivateMyAccount);

/**
 * @api {post} /user/vendor/shared/activate-my-account vendor activate his account
 * @apiName activate my account
 * @apiGroup Vendor
 * @apiParam  {string} id Vendor's token.
 * @apiParam  {string} lat Vendor's location.
 * @apiParam  {string} long Vendor's location.
 * @apiSuccessExample Success-Response:
 *{
 *"success": true
 *"message": "user actiavted successfully"
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": false
 *    "message": "user details doesn't exist"
 * }
 */

router.get('/activate-my-account', controllers.activateMyAccount);

router.get('/is-active', controllers.IsActive);

// router.get('/cars', controllers.getCars);


module.exports = router;