const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
var request = require('request');
var rn = require('random-number');
const customer = require('./customerModel');
const customerHelper = require('./helperCallbackFun');
const shared = require('../shared');
const push = require('../../pushNot/model');
const middlewares = require('../../middlewares')
//var Nexmo = require('nexmo');
var AWS = require('aws-sdk');
var google = require('googleapis');
//var google = require('googleapis')
AWS.config.region = 'us-east-1';
AWS.config.update({
      accessKeyId: process.env.AWS_Access_Key_ID,
      secretAccessKey: process.env.AWS_Secret_Access_Key,
});
var sns = new AWS.SNS();

exports.isUserExist = function (req , res , next) {
console.log(req.body.email)
if(req.body.fbAccountId){
customer.findOne({
fbAccountId: req.body.fbAccountId
}, (err, user) => {
if (err){
return res.json({
success: false,
message: err
})
} //console.log(err);
else if (!user) {
return res.json({
success: false,
message: "user not found"
})
}else {
console.log(user)
return res.json({
success: true,
message: "user exist"
})
}
})
}
else if(req.body.email){
customer.findOne({
email: req.body.email
}, (err, user) => {
if (err){
return res.json({
success: false,
message: err
})
} //console.log(err);
else if (!user) {
return res.json({
success: false,
message: "user not found"
})
}else {
console.log(user)
return res.json({
success: true,
message: "user exist"
})
}
})
}
else{
res.json(middlewares.checkAllFieldsExist)
}

}

// Register new customer
exports.registerCustomer = function (req, res, next) {
  let userData = req.body;
  if (userData.name && userData.email && userData.username && userData.phoneNumber && userData.password) {
    customerHelper.findOneByPhone(userData, (err, user) => {
      if (!user) {
        let newUser = new customer({
          name: req.body.name,
          email: req.body.email,
          username: req.body.username,
          phoneNumber: req.body.phoneNumber,
          password: req.body.password,
          role: req.body.role || 5
        });
        // Save the new user
        customer.addUser(newUser, (err, user) => {
          console.log(err);
          if (err) {
            res.json({
              success: false,
              message: err
            })
          } else {
            res.json({
              success: true,
              message: 'User registered successfully'
            })
          }
        })
      } else {
        if (user.username === userData.username) {
          res.json({
            success: false,
            message: "username already registered"
          })
        } else if(user.phoneNumber === userData.phoneNumber) {
          res.json({
            success: false,
            message: "phone number already registered"
          })
        }
        else if(user.email === userData.email) {
          res.json({
            success: false,
            message: "email already registered"
          })
        }
      }
    })
  } else {
    res.json({
      success: false,
      message: 'all attributes are needed'
    })
  }
};
// Login api for customers
exports.login = function (req, res, next) {
  let userData = req.body;
  if ((userData.email && userData.password  && !userData.loginWith) || (userData.phoneNumber && userData.password && userData.fcmToken && !userData.loginWith)) {
    // add attr. "findWith" to let the callback fun. know the type of credentials
    customerHelper.findOneByEmailOrPhone(userData, (err, user) => {
      if (user) {
        shared.checkPassword(userData.password, user, (error, token) => {
          // error should be true if password is wrong
          if (error) {
            res.json({
              success: false,
              message: "incorrect password"
            });
          } else if(!user.verification) {
             res.json({
              success: false,
              message: "please verify your phoneNumber first",
              userData: user
            });
          }else{
            if (!user.active) {
              customerHelper.reActivate(user, (errTwo) => {
                if (errTwo) {
                  res.json({
                    success: false,
                    message: errTwo
                  });
                } 
              })
            } else {
              push.findOneAndUpdate({
                userId: user._id
              }, {
                $set: {
                  fcmToken: req.body.fcmToken
                }
              }, {
                new: true
              }, function (err, doc) {
                if (!doc) {
                  let newPushObj = {
                    userId: user._id,
                    fcmToken: req.body.fcmToken
                  }
                  push.create(newPushObj, (err, done) => {
                    if (done) {
                      userLoggedIn(token, user);
                    }
                  })
                }else{
                  userLoggedIn(token, user);
                }
              });
            }
          }
        });
      } else if (!user) {
        res.json({
          success: false,
          message: "user not found"
        });
      } else if (err) {
        res.json({
          success: false,
          message: err
        });
      }else if (!user.verification) {
        res.json({
          success: false,
          message: "please verify your phoneNumber first",
          userData: user
        });
      }
    });
  } else if (userData.loginWith === "facebook" && userData.fcmToken && userData.fbUserAccessToken /*&& userData.phoneNumber*/) {
    /*userData.fbAccountId*/
     /*push.findOneAndUpdate({
                userId: user._id
              }, {
                $set: {
                  fcmToken: req.body.fcmToken
                }
              }, {
                new: true
              }, function (err, doc) {
                if (!doc) {
                  let newPushObj = {
                    userId: user._id,
                    fcmToken: req.body.fcmToken
                  }
                  push.create(newPushObj, (err, done) => {
                    if (done) {
                      userLoggedIn(token, user);
                    }
                  })
                }else{
                  userLoggedIn(token, user);
                }
              });*/
    request('https://graph.facebook.com/v3.1/me?fields=id,name,first_name,email&access_token=' + userData.fbUserAccessToken, function (error, response, body) {
      if(error || response.statusCode !== 200){
        return res.json({
          success: false,
          message: error || response
        })
      }
      else{
      console.log('error:', error); // Print the error if one occurred
      console.log('body:', body); // Print the HTML for the Google homepage.
      let resJSON = JSON.parse(body);
      customer.findOne({
        $or:[
        {'fbAccountId': resJSON.id},
        {'email': resJSON.email}
        ]
         //userData.fbAccountId
      }, (err, user) => {
          if (err){
            return res.json({
              success: false,
              message: err
            })
          } //console.log(err);
          else if (!user) {
              let newUser = new customer({
                name: resJSON.name,
                email: resJSON.email,
                phoneNumber: userData.phoneNumber,
                password: userData.password,
                username: resJSON.first_name,
                fbAccountId: resJSON.id
              })
            // Save the new user
            //User.addUser(newUser, (err, user) => {
              
            customer.addUser(newUser, (err, result) => {
              console.log(err);
              if (err) {
                res.json({
                  success: false,
                  message: err
                })
              }else if(!result.verification){
                    
                res.json({
                  success: false,
                  message: "please verify your phoneNumber first",
                  userData: result
                })

              } else {
                let newPushObj = {
                  userId: result._id,
                  fcmToken: req.body.fcmToken
                }
                push.create(newPushObj, (errr, done) => {
                      if(errr){
                        console.log(errr)
                      }
                      if (done) {
                        //userLoggedIn(token, result);
                      }
                })
                res.json({
                  success: true,
                  message: 'User registered successfully',
                  token: 'Bearer '+socialTokenGeneration(result),
                  user: result
                    //userId: request._id
                })
              }
            })
          } else if(user.verification) {
            push.findOneAndUpdate({
                userId: user._id
              }, {
                $set: {
                  fcmToken: req.body.fcmToken
                }
              }, {
                new: true
              }, function (err, doc) {
                if (!doc) {
                  let newPushObj = {
                    userId: user._id,
                    fcmToken: req.body.fcmToken
                  }
                  push.create(newPushObj, (er, done) => {
                    if (done) {
                      if(er){
                      console.log('er--- '+er)
                      }
                      //userLoggedIn(token, user);
                    }
                  })
                }
                  //userLoggedIn(token, user);
              });
            // if(user.phoneNumber !== userData.phoneNumber){
            //   user.phoneNumber = userData.phoneNumber;
            //   user.save(function(err, result){
            //     if(err || !result){
            //       return res.json({
            //         success: false,
            //         message: err || "error updating user"
            //       })
            //     }
            //     else{
            //       res.json({
            //           success: true,
            //           message: 'user Already exists',
            //           token: 'Bearer '+socialTokenGeneration(result),
            //           user: result
            // //userId: user._id
            //       })
            //     }
            //   })
            // }
            user.fbAccountId = resJSON.id
            user.save((err) => {
                if(err){
                  res.json({
                  success: false,
                  message: 'user Already mo7n'
            //userId: user._id
              })
                }
            })
            
              res.json({
                  success: true,
                  message: 'user Already exists',
                  token: 'Bearer '+socialTokenGeneration(user),
                  user: user
            //userId: user._id
              })
            
          
        }else {
          res.json({
              success: false,
              message: "please verify your phoneNumber first",
              userData: user
            })
        }
      });
      }
      
    });
  }
  else if(userData.loginWith === "google" && userData.fcmToken && userData.gUserAccessToken){
    //var OAuth2 = google.auth.OAuth2;
    
    const {OAuth2Client} = require('google-auth-library');
    

    var OAuth2 = google.oauth2_v2.Oauth2;
    var oauth2Client = new OAuth2();
    oauth2Client.tokeninfo({id_token: userData.gUserAccessToken}, (ee, resp) => {
      if(ee){
        
        console.log('e',ee)
        res.json({
          success: false,
          message: ee
        })
      }
      else{
        //console.log('res',resp.data.audience)
        console.log(resp.data.audience, resp.data.audience.substring(0,resp.data.audience.indexOf(".")))
        var aud = resp.data.audience.substring(0,resp.data.audience.indexOf("."));
        const client = new OAuth2Client(aud);
        const ticket = client.verifyIdToken({
          idToken: userData.gUserAccessToken,
          audience: resp.data.audience,  // Specify the CLIENT_ID of the app that accesses the backend
          // Or, if multiple clients access the backend:
          //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
        }, (er, r) => {
          if(er){
            console.log("mo7n")
            res.json({
              success: false,
              message: er
            })
          }
          else{
            console.log('t',r.payload)
            // const payload = ticket.getPayload();
            // console.log(payload)
            var data = r.payload;
            customer.findOne({
              $or:[
              {'gAccountId': data.sub},
              {'email': data.email}
              ]
               //userData.fbAccountId
            }, (err, user) => {
                if (err){
                  return res.json({
                    success: false,
                    message: err
                  })
                } //console.log(err);
                else if (!user) {
                    let newUser = new customer({
                      name: data.name,
                      email: data.email,
                      phoneNumber: userData.phoneNumber,
                      password: userData.password,
                      username: data.given_name,
                      gAccountId: data.sub
                    })
                  // Save the new user
                  //User.addUser(newUser, (err, user) => {
                    
                  customer.addUser(newUser, (err, result) => {
                    console.log(err);
                    if (err) {
                      res.json({
                        success: false,
                        message: err
                      })
                    }else if(!result.verification){
                          
                      res.json({
                        success: false,
                        message: "please verify your phoneNumber first",
                        userData: result
                      })
      
                    } else {
                      let newPushObj = {
                        userId: result._id,
                        fcmToken: req.body.fcmToken
                      }
                      push.create(newPushObj, (errr, done) => {
                            if(errr){
                              console.log(errr)
                            }
                            if (done) {
                              //userLoggedIn(token, result);
                            }
                      })
                      res.json({
                        success: true,
                        message: 'User registered successfully',
                        token: 'Bearer '+socialTokenGeneration(result),
                        user: result
                          //userId: request._id
                      })
                    }
                  })
                } else if(user.verification) {
                  push.findOneAndUpdate({
                      userId: user._id
                    }, {
                      $set: {
                        fcmToken: req.body.fcmToken
                      }
                    }, {
                      new: true
                    }, function (err, doc) {
                      if (!doc) {
                        let newPushObj = {
                          userId: user._id,
                          fcmToken: req.body.fcmToken
                        }
                        push.create(newPushObj, (er, done) => {
                          if (done) {
                            if(er){
                            console.log('er--- '+er)
                            }
                            //userLoggedIn(token, user);
                          }
                        })
                      }
                        //userLoggedIn(token, user);
                    });
                  // if(user.phoneNumber !== userData.phoneNumber){
                  //   user.phoneNumber = userData.phoneNumber;
                  //   user.save(function(err, result){
                  //     if(err || !result){
                  //       return res.json({
                  //         success: false,
                  //         message: err || "error updating user"
                  //       })
                  //     }
                  //     else{
                  //       res.json({
                  //           success: true,
                  //           message: 'user Already exists',
                  //           token: 'Bearer '+socialTokenGeneration(result),
                  //           user: result
                  // //userId: user._id
                  //       })
                  //     }
                  //   })
                  // }
                  user.gAccountId = data.sub
                  user.save((err) => {
                      if(err){
                        res.json({
                        success: false,
                        message: 'user Already mo7n'
                  //userId: user._id
                    })
                      }
                  })
                  
                    res.json({
                        success: true,
                        message: 'user Already exists',
                        token: 'Bearer '+socialTokenGeneration(user),
                        user: user
                  //userId: user._id
                    })
                  
                
              }else {
                res.json({
                    success: false,
                    message: "please verify your phoneNumber first",
                    userData: user
                  })
              }
            });
            // res.json({
            //   success: true,
            //   payload: r.payload
            // })
          }
          
        });
      }
    })

    
    
  } 
  else {
    res.json(middlewares.checkAllFieldsExist)
  }
  let socialTokenGeneration = function (user) {
    return token = jwt.sign(user, "b1OFyhEJyZfdsfWxh4WKoLzb", {
      expiresIn: 29030400 // 1 week
    })
  }
  // if user can login successfully we will call this function 
  let userLoggedIn = (token, user) => {
    return res.json({
      success: true,
      token: 'Bearer ' + token,
      user: {
        _id: user._id,
        name: user.name,
        username: user.username,
        phoneNumber: user.phoneNumber,
        email: user.email,
        role: user.role
      }
    });
  }
};
// api to send a mail to your email address with a code 
// which you will enter in the mobile app so you can change password
exports.sendForgetPasswordCode = function (req, res, next) {
  let userData = req.body;
  if (userData.email) {
    customerHelper.findOneByEmailOrPhone(userData, (err, userFound) => {
      if (!userFound) {
        res.json({
          success: false,
          message: "user email address doesn't exist"
        });
      } else {
        console.log("hiiiiii24")
        let smtpTrans = nodemailer.createTransport({
          service: 'SendGrid',
          auth: {
            user: 'bishoywadie',
            pass: 'user123456'
          }
        });
        var gen = rn.generator({
          min: -900000,
          max: 900000,
          integer: true
        });
        generatedNum = gen(100000);
        userFound.passVerificationCode = generatedNum;
        userFound.save((err) => {
          if (err) {
            res.json({
              success: false,
              message: err
            });
          } else {
            var mailOptions = {
              to: req.body.email,
              from: 'noreply@enqaz.com',
              subject: 'Password Reset - enqaz',
              text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                'Please use the following code: ' + generatedNum + '\n\n' +
                ' If you did not request this, please ignore this email and your password will remain unchanged.\n'
            };
            smtpTrans.sendMail(mailOptions, function (err, sent) {
              console.log("err,sent", err, sent)
              if (err) res.json({
                success: false,
                message: err
              });
              res.json({
                success: true,
                message: req.body.email
              });
            });
          }
        });
      }
    });
  }
  else if(userData.phoneNumber){
   customer.findOne({phoneNumber : userData.phoneNumber},function(err, user){
     if(err){
       return res.json({
         success: false,
         message: err
       })
     }
     if(!user){
       return res.json({
         success: false,
         message: "this phone number does not exist"
       })
     }
     else{

      

      // var auth_Token = process.env.orange_auth_header;
      // var options = {
      //     method: 'POST',
      //     url: 'https://api.orange.com/oauth/v2/token',
      //     headers: {
      //       'Authorization': auth_Token
      //     },
      //     form: {
      //       "grant_type":"client_credentials"
      //     }//,
      //     // body: {
      //     //   "grant_type":"client_credentials"
      //     // }
      // }
      // request(options, function(error,response,body){
      //   if(error || response.statusCode !== 200){
      //     //console.log(response)
      //     return res.json({
      //       success: false,
      //       message: error || response.statusCode
      //     })
      //   }
      //   else{
      //     var bodyJSON = JSON.parse(body)
      //     var auth = bodyJSON.token_type + ' ' + bodyJSON.access_token;
      //     //console.log(JSON.parse(body))
      //     var opts = {
      //       method: 'POST',
      //       url: 'https://api.orange.com/smsmessaging/v1/outbound/'+encodeURIComponent('tel:+2'+userData.phoneNumber)+'/requests',
      //       headers: {
      //         'Authorization': auth,
      //         'Content-Type': 'application/json',
      //         //'Accept': 'q=0.8;application/json;q=0.9'
      //       },
      //       json: {
      //         "outboundSMSMessageRequest": {
      //           "address": 'tel:+2'+userData.phoneNumber,
      //           "outboundSMSTextMessage":{ "message":'Enqaz \n\nVerification code: '+ generatedNum +'.\n\n'},
      //           "senderAddress":"tel:+2"+userData.phoneNumber,
      //           "senderName": "Enqaz"
      //         }
      //       }
      //     }
      //     request(opts, function(er, rs, bdy){
      //       if(er || rs.statusCode !== 200){
      //        // console.log(JSON.parse(bdy))
      //         return res.json({
      //           success: false,
      //           message: error || rs
      //         })
      //       }
      //       else{
      //         return res.json({
      //         success: true,
      //         body: JSON.parse(bdy) 
      //         })
      //       }
      //     })
          
      //   }
      // })

       // const nexmo = new Nexmo({
       //   apiKey: "15c97409",
       //   apiSecret: "ev25UB30j0SkSM0K"
       // });
       var gen = rn.generator({
         min: -900000,
         max: 900000,
         integer: true
       });
       generatedNum = gen(100000);
       user.passVerificationCode = generatedNum;
       //var verifyRequestId ;
       user.save(function(errr, result){
        if(errr || !result){
          return res.json({
            success: false,
            message: errr || "error updating user"
          })
        }
        else{
          var params = {
            Message: 'Enqaz \n\nVerification code: '+ generatedNum +'.\n\n',
            MessageStructure: 'string',
            PhoneNumber: '2'+user.phoneNumber,
            Subject: 'ENQAZ'
          };

          sns.publish(params, function(eror, data) {
                if (eror){
                  return res.json({
                    success: false,
                    message: eror
                  })
                } // an error occurred
                else{
                  console.log("dataaaaaaa" , data)
                  res.json({
                    success: true,
                    message: "SMS sent successfully",
                    data: data
                  })
                }              // successful response
          });
        }
       });

       
       /* nexmo.verify.request({number: userData.phoneNumber, brand: 'Jawlaty'}, function(err, result) {
         if(err) {
           return res.json({
             success: false,
             message: err
           })
          }
         else {
           verifyRequestId = result.request_id;
           //console.log(verifyRequestId);
           res.json({
             success : true,
             message: 'SMS sent successfully'
           })
           nexmo.verify.control({request_id: verifyRequestId, cmd: 'cancel'}, function(err, result) {
             if(err) { console.error('--- ',err); }
             else {
               console.log(result);
             }
           });
           nexmo
         }
       }); */
       //console.log('req id -- ',verifyRequestId)

       // nexmo.message.sendSms(
       //   'Enqaz',"2"+userData.phoneNumber, 'Enqaz \n\nVerification code: '+ generatedNum +'.\n\n',
       //     (err, responseData) => {
       //       if (err) {
       //         return res.json({
       //           success: false,
       //           message : err
       //         })
       //       } else {
       //         console.log(responseData)
       //         res.json({
       //           success : true,
       //           message: 'SMS sent successfully'
       //         })
       //       }
       //     }
       //  );
     }
   })
 }
 else {
    res.json(middlewares.checkAllFieldsExist)
  }
};
// enter old and new password to renew it
exports.resetPassword = function (req, res, next) {
  let userData = req.body;
  if (userData.password && userData.newPassword) {
    customerHelper.findOneByEmail({
      email: req.user._doc.email
    }, (err, userFound) => {
      if (userFound) {
        shared.checkPassword(userData.password, userFound, function (err, isMatch) {
          if (err) {
            res.json({
              success: false,
              message: 'wrong password!'
            });
          } else {
            shared.hashPassword(userData.newPassword, (err, hashedPassword) => {
              userFound.password = hashedPassword;
              userFound.save();
            });
            res.json({
              success: true,
              message: 'Your password reset successfuly'
            });
          }
        });
      } else {
        res.json({
          success: false,
          message: err
        })
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
};
exports.verifyForgetPasswordCode = function (req, res, next) {
  let reqPassVerCode = Number(req.body.passVerificationCode);
  if (reqPassVerCode) {
    customer.findOne({
      passVerificationCode: reqPassVerCode
    }, (err, userFound) => {
      console.log(reqPassVerCode, userFound)
      if (!userFound) {
        res.json({
          success: false,
          message: "user details doesn't exist"
        });
      } else if (userFound && userFound.passVerificationCode) {
        if (reqPassVerCode === userFound.passVerificationCode) {
          userFound.verification = true
          shared.generateToken(userFound, (error, token) => {
            userFound.token = token;
            userFound.save((err, done) => {
              if (err) {
                res.json({
                  success: false,
                });
              } else {
                res.json({
                  success: true,
                  token: 'Bearer ' + token
                });
              }

            })
          });
        } else {
          userFound.passVerificationCouserFound.passVerificationCodede
          res.json({
            success: false,
            message: "don't match"
          });
        }
      } else if (err) {
        res.json({
          success: false,
          message: err
        });
      }
    });
  } else {
    res.json(middlewares.checkAllFieldsExist)
  }
};


exports.deActivateAccount = function (req, res, next) {
  customerHelper.findOneById({
    _id: req.user._doc._id,
  }, (err, userFound) => {
    if (!userFound) {
      res.json({
        success: false,
        message: "user details doesn't exist"
      });
    } else if (userFound) {
      userFound.active = false;
      userFound.save((err, user) => {
        if (!user) {
          res.json({
            success: false,
            message: err
          });
        } else {
          res.json({
            success: true,
            message: "user deactiavted successfully"
          });
        }
      });
    };
  });
}
exports.updateInfo = function (req, res, next) {
  let reqBody = req.body;
  if (!reqBody) {
    res.json(middlewares.checkAllFieldsExist)
  }
  customerHelper.findOneById({
    _id: req.user._doc._id
  }, (err, userFound) => {
    if (!userFound) {
      res.json({
        success: false,
        message: "user details doesn't exist"
      });
    } else if (userFound) {
      if (reqBody.name) {
        userFound.name = reqBody.name
      }
      if (reqBody.username) {
        userFound.username = reqBody.username
      }
      if (reqBody.email) {
        userFound.email = reqBody.email
      }
      if (reqBody.addresses) {
        userFound.addresses = reqBody.addresses
      }
      if (reqBody.phoneNumber) {
        userFound.phoneNumber = reqBody.phoneNumber
      }
      if (reqBody.password) {
        shared.hashPassword(reqBody.password, (err, hashedPassword) => {
          userFound.password = hashedPassword;
          userFound.save();
        });
      }
      userFound.save((err, user) => {
        if (!user) {
          res.json({
            success: false,
            message: err
          });
        } else {
          res.json({
            success: true,
            message: "Info Updated Successfuly"
          });
        }
      })
    }
  });
};