var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');

//require controllers
var userController = require('./controllers');
const baseRoute = '/user/customer/';
// middleware to protect all except mentioned apis
router.use(jwt({
    secret: process.env.secret
}).unless({
    path: [baseRoute + 'is-user-exist', baseRoute + 'register', baseRoute + 'login', baseRoute + 'forget-pass-code', baseRoute + 'verify-forget-pass-code']
}), function (req, res, next) {
    if (req.user) {
        console.log(req.user._doc.role);
        if (req.user._doc.role != 5) return res.json({
            sucess: false,
            message: "you are not a customer"
        });
        next();
    } else {
        next();
    }
});
///user/customer/is-user-exist
router.post('/is-user-exist', userController.isUserExist);
//Routers
/**
 * @api {post} /user/customer/register register new customer
 * @apiName registerCustomer
 * @apiGroup authCustomer
 * @apiParam {string} name user name
 * @apiParam  {string} email user email address.
 * @apiParam  {number} phoneNumber user phone number.
 * @apiParam  {string} password user password.
 * @apiParam  {string} username username.
 * @apiSuccessExample Success-Response:
 *{
 *     success: true,
 *      message: 'customer registered successfully'
 *}
 * @apiErrorExample Error-Response:
 * {
 *       success: false,
 *      message: 'customer faild to register'
 *   }
 */
router.post('/register', userController.registerCustomer);
/**
 * @api {post} /user/customer/login login 
 * @apiName LoginCustomer
 * @apiGroup authCustomer
 * @apiParam  {string} [email] One type of login methods.
 * @apiParam  {number} [phoneNumber] One type of login methods.
 * @apiParam  {string} password One type of login methods.
 * @apiSuccessExample Success-Response:
 *{
 *"success": true,
 *"token": "",
 *"user": {
 *"id": "",
 *"name": "",
 *"username": "",
 *"email": ""
 *   }
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": false,
 *    "message": "Wrong password!"
 * }
 */
router.post('/login', userController.login);
/**
 * @api {post} /user/customer/forget-pass-code User forgetting password
 * @apiName forgetPassword
 * @apiGroup authCustomer
 * @apiParam  {string} [email] User's Email address.
 * @apiParam  {number} [phone] User's phone number.
 * @apiSuccessExample Success-Response:
 *{
 *"success": true
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": false
 * }
 */
router.post('/forget-pass-code', userController.sendForgetPasswordCode);
/**
 * @api {post} /user/customer/verify-forget-pass-code User verifing code for changing his password 
 * @apiName verifing password verification code
 * @apiGroup authCustomer
 * @apiParam  {string} [email] One type of login methods.
 * @apiParam  {number} [phoneNumber] One type of login methods.
 * @apiParam  {number} passVerificationCode code sent to user on mail
 * @apiSuccessExample Success-Response:
 *{
 *"success": true
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": false
 * }
 */

router.post('/verify-forget-pass-code', userController.verifyForgetPasswordCode);
/**
 * @api {get} /user/customer/reset-pass
 * @apiName user reset his password
 * @apiGroup authCustomer
 * @apiParam  {string} password user old password.
 * @apiParam  {string} newPassword user new password.
 * @apiSuccessExample Success-Response:
 *{
 *        success: true,
 *        message: 'Your password reset successfuly'
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    success: false,
 *     message: 'wrong old password!'
 * }
 */
router.post('/reset-pass', userController.resetPassword);
/**
 * @api {get} /user/customer/deactivate
 * @apiName deactivateMyAccount
 * @apiGroup authCustomer
 * @apiSuccessExample Success-Response:
 *{
 *"success": account de-activate successfuly,
 *} 
 * @apiErrorExample Error-Response:
 * {
 *    "success": falied To de-activate,
 * }
 */
router.get('/deactivate', userController.deActivateAccount);
/**
 * @api {post} /user/customer/update-account
 * @apiName update user info
 * @apiGroup authCustomer
 * @apiParam  {string} [name]
 * @apiParam  {string} [username]
 * @apiParam  {string} [email] 
 * @apiParam  {string} [addresses] 
 * @apiParam  {number} [phoneNumbers] 
 * @apiSuccessExample Success-Response:
 *{
 *"success": true,
 * "message" :"Info Updated Successfuly"
 *} 

 * @apiErrorExample Error-Response:
 * {
 *    "success": 
 *    "message":"User Not Found"
 * }
 */
router.post('/update-account', userController.updateInfo);

// router.post('/deactivate-user',middlewares.isAdmin, userController.deactivateUser);
module.exports = router;