const mongoose = require('mongoose')
const bcryptjs = require('bcryptjs')
const config = require('config')

const customerSchema = mongoose.Schema({
    name: {
        type: String,
        required: false
    },
    email: {
        type: String,
        unique: true,
        required: true,
    },
    phoneNumber: {
        type: String,
        unique: true,
        required: true,
    },
    username: {
        type: String,
        required: true,
        unique: false
    },
    password: {
        type: String,
        //required: true
    },
    passVerificationCode: {
        type: Number,
    },
    verification: {
        type: Boolean,
        default: false
    },
    resetPasswordToken: {
        type: String,
    },
    addresses: {
        type: [String]
    },
    fbAccountId: {
        type: String,
        unique: true,
    },
    gAccountId: {
        type: String,
        unique: true
    },
    // customer : 5 
    role: {
        type: Number,
        default: 5,
        required: true
    },
    active: {
        type: Boolean,
        default: true
    },
    primaryPaymentMethod: {
        type: Number,
        default: 1
    },
    photo: {
        type: String,
        required: false
    },

})

const customer = module.exports = mongoose.model('customer', customerSchema)

// Save a new user
module.exports.addUser = function (newUser, callback) {
    bcryptjs.genSalt(10, (err, salt) => {
        bcryptjs.hash(newUser.password, salt, (err, hash) => {
            if (err) callback(err);
            newUser.password = hash
            newUser.save(callback)
        })
    })
}