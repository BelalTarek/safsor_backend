const customer = require('./customerModel');

module.exports.findOneByPhone = function (userData, callback) {
    customer.findOne({
        $or : [{
            phoneNumber: userData.phoneNumber},
            {username: userData.username},
            {email: userData.email
            }]
        
    }, callback);
};
module.exports.findOneByEmailOrPhone = function (userData, callback) {
    if (userData.phoneNumber) {
        customer.findOne({
            phoneNumber: userData.phoneNumber
        }, callback);
    } else if (userData.email) {
        customer.findOne({
            email: userData.email
        }, callback);
    }
};

module.exports.findOneByEmail = function (userData, callback) {
    customer.findOne({
        email: userData.email
    }, callback);
};
module.exports.findOneById = function (userData, callback) {
    customer.findOne({
        _id: userData._id
    }, callback);
}
module.exports.reActivate = function (user, callback) {
    customer.findOneAndUpdate({
        _id: user._id,
    }, {
        $set: {
            active: true,
        }
    }, {
        new: true
    }, callback);
}