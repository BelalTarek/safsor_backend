const express = require('express');
const log = require('simple-node-logger').createSimpleLogger('project.log');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config();
const mongoose = require('mongoose')
var binary = require('node-pre-gyp');
const passport = require('passport')
const passport_jwt = require('passport-jwt')
const flash = require('express-flash')
const cookieSession = require('cookie-session');
const methodOverride = require('method-override');
const admin = require("firebase-admin");
const serviceAccount = require('./config/firebaseKey.json')
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://safesor-96915.firebaseio.com"
});
var customer = require('./modules/auth/customer/routes');
//admin routes
var superAdmin = require('./modules/auth/admin/superAdmin/superAdminRoutes');
var adminShared = require('./modules/auth/admin/sharedRoutes');
var superVendor = require('./modules/auth/vendor/manager/managerRoutes');
var vendorShared = require('./modules/auth/vendor/sharedRoutes');
var customer = require('./modules/auth/customer/routes');
var requests = require('./modules/requests/routes');
var services = require('./modules/services/routes');
var paymentCards = require('./modules/payment/routes');
var promoCodes = require('./modules/promocodes/routes');
var pushNot = require('./modules/pushNot/routes');
var referral = require('./modules/referral/routes');
var trip = require('./modules/trip/routes');
var packages = require('./modules/packages/routes');
var invoice = require('./modules/invoice/routes');
var car = require('./modules/cars/routes');
var carSelection = require('./modules/carSelection/routes')
var report = require('./modules/report/routes')
var rating = require('./modules/rating/routes')
var notifications = require('./modules/notifications/routes')


var app = express();

mongoose.connect(process.env.mongoDbURL, {
  useMongoClient: true
})
mongoose.connection.on('connected', () => {
  console.log(` | Database Is Live and Connected |\n ================================== \n`)
})
mongoose.connection.on('error', (error) => {
  console.log('Something worng!!', error)
})


// ref.on("value", function(snapshot) {
//   var newPost = snapshot.val();
//   if(newPost)
//   console.log("DriverId: " + newPost);
// });
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  limit: '100mb', extended: true //extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors())
// app.use(function(req, res, next) {
//   res.setHeader('Access-Control-Allow-Origin', '*');
//   res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//   res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
//   next();
// });
// Session
app.use(cookieSession({
  name: 'session',
  keys: ['icEgv95GyU', 'r5oQr21nj5'],
  maxAge: 2 * 60 * 60 * 1000 // 2 hours
}))
app.use(function (req, res, next) {
  res.locals.session = req.session;
  next();
});

// Method Override
app.use(methodOverride('_method'));

// Flash
app.use(flash());

// Passport
app.use(passport.initialize())
app.use(passport.session())
require('./passport')(passport);
// admin
app.use('/user/admin/super', superAdmin);
app.use('/user/admin/shared', adminShared);
// vendor
app.use('/user/vendor/manager', superVendor);
app.use('/user/vendor/shared', vendorShared);
//customer
app.use('/user/customer', customer);

app.use('/requests', requests);
app.use('/services', services);
app.use('/payment', paymentCards);
app.use('/promo-codes', promoCodes);
app.use('/push', pushNot);
app.use('/referral', referral);
app.use('/trip', trip);
app.use('/packages', packages);
app.use('/invoice', invoice);
app.use('/cars', car);
app.use('/car-selection', carSelection)
app.use('/reports', report)
app.use('/rating', rating)
app.use('/notifications', notifications)


// // catch 404 and forward to error handler
// app.use(function (req, res, next) {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });


// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.json({
    success: false,
    message: err.message
  });
});

module.exports = app;